<?php
include_once('../rotas.php');
include_once('../foods/functions.php');
include_once('../variables.php');

$arrayPosition = trim(htmlspecialchars($_POST['arrayPosition'], ENT_QUOTES));

$url = $rotas['api'].'sauros/vendasfood';

$jsonData = [
    "tipoEstabelecimento"=> $globalData['tipoEstabelecimento'],
    "cnpjEstabelecimento"=> $globalData['cnpjUser'],
    "idToken"=> "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRjMGMzNWZlYjBjODIzYjQyNzdkZDBhYjIwNDQzMDY5ZGYzMGZkZWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbm9tZXJjYWRvYXBwIiwiYXVkIjoibm9tZXJjYWRvYXBwIiwiYXV0aF90aW1lIjoxNTg2NDU3NTAxLCJ1c2VyX2lkIjoieXNFQ1ZLZ1Mwb2EzUWpCTWdzWnpoTGRKZ1BqMSIsInN1YiI6InlzRUNWS2dTMG9hM1FqQk1nc1p6aExkSmdQajEiLCJpYXQiOjE1ODY0NTc1MDEsImV4cCI6MTU4NjQ2MTEwMSwiZW1haWwiOiJjYW1wdXNAbm9tZXJjYWRvLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJjYW1wdXNAbm9tZXJjYWRvLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.ume5EnxRYoyt31dkxPmgQHl5Sdf77yF-KJtaQMazrEohFBMZNFmZLrYJEYmG0HZRXuH6aba39VCY-dR2pkhD5AHqf10NMhIrZl6yOP2KlCCc9tYkpn7wsXYkHT0pz37lMnZl4AgWkYr1sjUF0uP2mJJiGcpcurUCqaDHOgP1Q7_4gjdCGF85dyeIvpIwXbeKbK7qMOpGx73ygT25BrY_qOLZldu-gH-g8nkV-GeIIM1Np4gs-iSuozj7-aX-zz69YGBRqsqyPFt4_kwschC7YZJqgCK3LZnJ7-T0MtO39EBTggQjnD-2ZX3WEmP7aRe4lILusM79Bs2DECADSWClSQ",
    "status"=> "all",
    "date" => "01/07/2020"
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$resultList = file_get_contents($url, true, $context);

$jsonDataList = json_decode($resultList, true);
$produtos = '';
$values = $jsonDataList[explode(";",$arrayPosition)[0]]['carrinhoList'][explode(";",$arrayPosition)[1]]['adicionais'];

$arrayCategorias = array();

foreach ($values as $val){
    if(!isset($arrayCategorias[$val['categoria']])){
        $arrayCategorias[$val['categoria']]="<div class='text-left' style='background: #e6e6e6; padding: 10px;'><h3>".ucfirst($val['categoria'])."</h3></div><div class='quebra-linha'></div><div class='text-left'><h4>".$val['nomeProduto']."</h4></div><div class='text-left'><h4>".$val['valorInicial']."</h4></div><div class='text-left'><h4>Total: ".$val['quantidade']*$val['valorInicial']."</h4></div><div class='text-left'><h4>Quantidade: ".$val['quantidade']."</h4></div><div class='quebra-linha'></div>";
    }else{
        $arrayCategorias[$val['categoria']].="<div class='text-left'><h4>".$val['nomeProduto']."</h4></div><div class='text-left'><h4>".$val['valorInicial']."</h4></div><div class='text-left'><h4>Total: ".$val['quantidade']*$val['valorInicial']."</h4></div><div class='text-left'><h4>Quantidade: ".$val['quantidade']."</h4></div><div class='quebra-linha'></div>";
    }
}

$values = '';
foreach ($arrayCategorias as $arrays){
    $values .= $arrays;
}

echo $values;
