<?php
include_once('rotas.php');
include_once('variables.php');

$key = trim(htmlspecialchars($_POST['key'], ENT_QUOTES));
$text = trim(htmlspecialchars($_POST['text'], ENT_QUOTES));
$url = trim(htmlspecialchars($_POST['url'], ENT_QUOTES));

$urlChat = $rotas['api'].'chat/cadastrar';

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst'],
    "key" => $key,
	"photoUrl" => $url,
	"text" => $text
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$resultChat = file_get_contents($urlChat, true, $context);

