$(document).ready(function () {
    stopLoading();

    var i = 1;
    $('#add').click(function () {
        i++;
        $('#dynamic_field').append('<div id="row' + i + '" style="display:inline-flex" class="col-12"><input type="text" name="name[]" placeholder="" class="form-control name_list col-8" /><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove col-2">X</button></div>');
    });

    $(document).on('click', '.btn_remove', function () {
        var button_id = $(this).attr("id");
        $('#row' + button_id + '').remove();
    });

    $('#submit').click(function () {
        $.ajax({
            url: "name.php",
            method: "POST",
            data: $('#add_name').serialize(),
            success: function (data) {
                alert(data);
                $('#add_name')[0].reset();
            }
        });
    });

    /*$(".campo-adicionar-listas").keyup(function() {
        alert('asd');
    });*/

    $('.campo-valor').mask('###.00', {
        reverse: true
    });

    //Ao marcar um ingrediente, deverá habilitar o campo de quantidade referente ao ingrediente clicado
    $('.table-ingredientes #check-list').change(function () {
        $(".check-in").each(function (i) {
            if (this.checked) {
                $(".number-in:eq( " + i + " )").prop("disabled", false);
            } else {
                $(".number-in:eq( " + i + " )").prop("disabled", true);
            }
        });
    });

    //Ao marcar um adicional, deverá habilitar o campo de quantidade referente ao adicional clicado
    $('.table-adicionais #check-list').change(function () {
        $(".check-ad").each(function (i) {
            if (this.checked) {
                $(".number-ad:eq( " + i + " )").prop("disabled", false);
            } else {
                $(".number-ad:eq( " + i + " )").prop("disabled", true);
            }
        });
    });

    $('.campo-promocao').keyup(function () {
        if ($(this).val() != "")
            $(".radio-calculo").prop("disabled", false);
        else
            $(".radio-calculo").prop("disabled", true);
    });

    var value_tmp = 0;
    $(".radio-calculo").on('change', function () {
        if ($('.campo-valor').val() != "") {
            if ($(this).val() == "sim") {
                var valor = $('.campo-valor').val();
                value_tmp = valor;
                $('.campo-valor').val((1 - ($('.campo-promocao').val() / 100)) * valor);
            } else if ($(this).val() == "nao") {
                $('.campo-valor').val(value_tmp);
            }
        }
    });

    //Função de upload de imagem
    var readURL = function (input, pos) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic-' + pos).attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '.file-upload', function () {
        readURL(this, $(this).attr('pos'));
    });

    $(document).on('click', '.produtos .upload-area', function () {
        var pos = $(this).attr('pos');
        $(".produtos .upload-" + pos).click();
    });

    $(document).on('click', '.resultado .upload-area', function () {
        var pos = $(this).attr('pos');
        $(".resultado .upload-" + pos).click();
    });

    $(document).on('click', '#area-lista .upload-area', function () {
        var pos = $(this).attr('pos');
        $("#area-lista .upload-" + pos).click();
    });

    /*//Ao aumentar a quantidade de uma categoria, deverá bloquear as quantidades individuais dos itens
    $('.table-adicionais .titulo-categoria input[type="number"]').change(function () {
        var ref = $(this).attr('ref');
        if ($(this).val() > 0) {
            $('.number-ad').each(function (i) {
                if ($(this).attr('ref') == ref) {
                    $(this).prop("disabled", true);
                }
            });
        }else{
            $('.number-ad').each(function (i) {
                if ($(this).attr('ref') == ref) {
                    $(this).prop("disabled", false);
                }
            });
        }
    });*/

    //Ao marcar a caixa de categoria obrigatoria, deverá desabilitar todos os campos de quantidade daquela categoria
    $('.obrig-ad-title').change(function () {
        var checked = $(this).attr('ref');
        if (this.checked) {
            $('.table-adicionais input[type="number"]').each(function () {
                var check = $(this).attr('ref');
                if (check == checked) {
                    $(this).prop("disabled", true);
                }
            });
        } else {
            $('.table-adicionais input[type="number"]').each(function () {
                var check = $(this).attr('ref');
                if (check == checked) {
                    $(this).prop("disabled", false);
                }
            });
        }
    });

    //Evitar que feche o dropdown ao clicar
    $('.dropdown-menu').on('click', function (event) {
        event.stopPropagation();
    });

    //Ao marcar um novo ingrediente, irá acrescentá-lo na lista de visualização no canto direito
    $('.table-ingredientes .check-in').on('change', function () {
        if (this.checked) {
            var i = $(this).attr('pos');
            var name = $(".text-in-" + i).text();
            if ($(".number-val-" + i).val() != null) {
                $(".area-ing tbody").append("<tr id='appin" + i + "' style='border-bottom: 2px solid #ffc3c3; border-top: 2px solid #ffc3c3;'><td><span class='name-in'>" + name + "</span></td><td><span class='quant-val'>1</span></td><td><span class='quant-in'>1</span></td></tr>");
            } else {
                $(".area-ing tbody").append("<tr id='appin" + i + "' style='border-bottom: 2px solid #ffc3c3; border-top: 2px solid #ffc3c3;'><td><span class='name-in'>" + name + "</span></td><td><span class='quant-in'>1</span></td></tr>");
            }
        } else if (!this.checked) {
            var i = $(this).attr('pos');
            $("#appin" + i).remove();
        }
    });

    //Ao aumentar ou diminuir o número de ingredientes, irá atualizar a lista de visualização no canto direito
    $('.number-in').on('change', function () {
        var i = $(this).attr('pos');
        $('#appin' + i + ' .quant-in').text($(this).val());
    });

    //Ao aumentar ou diminuir o número de ingredientes, irá atualizar a lista de visualização no canto direito
    $('.number-val').on('change', function () {
        var i = $(this).attr('pos');
        $('#appin' + i + ' .quant-val').text($(this).val());
    });

    //Ao marcar um novo adicional, irá acrescentá-lo na lista de visualização no canto direito
    $('.table-adicionais .check-ad').on('change', function () {
        if (this.checked) {
            var i = $(this).attr('pos');
            var name = $(".text-ad-" + i).text();
            $(".area-adc tbody").append("<tr id='appad" + i + "' style='border-bottom: 2px solid #ffc3c3; border-top: 2px solid #ffc3c3;'><td><span class='name-ad'>" + name + "</span></td><td><span class='quant-ad'>1</span></td></tr>");
        } else if (!this.checked) {
            var i = $(this).attr('pos');
            $("#appad" + i).remove();
        }
    });

    //Ao aumentar ou diminuir o número de adicionais, irá atualizar a lista de visualização no canto direito
    $('.number-ad').on('change', function () {
        var i = $(this).attr('pos');
        $('#appad' + i + ' .quant-ad').text($(this).val());
    });

    //Ao clicar em um adicional, deverá também habilitar o campo de quantidade de categoria
    //Se desmarcar todos da categoria, deverá bloquear o campo novamente
    $('.check-ad').change(function () {
        var parent = $(this).parent().parent().parent();
        var ref = $(this).attr('ref');

        var cont = 0;
        $('.check-ad[ref=' + ref + ']',parent).each(function (index) {
            if ($(this).is(':checked'))
                cont++;
        });

        if (cont > 0) {
            $('.value-ad-title[ref=' + ref + ']').prop("disabled", false);
            $('.number-ad-title[ref=' + ref + ']').prop("disabled", false);
            $('.obrig-ad-title[ref=' + ref + ']').prop("disabled", false);
        } else {
            $('.value-ad-title[ref=' + ref + ']').prop("disabled", true);
            $('.number-ad-title[ref=' + ref + ']').prop("disabled", true);
            $('.obrig-ad-title[ref=' + ref + ']').prop("disabled", true);
        }
    });

    //Ao marcar uma categoria na tela de adicionais, irá atualizar o dropbox com o nome da categoria
    $(".radio-categorias").on('change', function () {
        $('.text-categoria').text($(this).val());
        $('.value-categoria').val($(this).val());
    });

    //Ao marcar uma lista na tela de adicionais, irá atualizar o dropbox com o nome da categoria
    $(".radio-listas-adicional").on('change', function () {
        $('.text-listas-adicional').text($(this).val());
        $('.value-listas-adicional').val($(this).val());
    });

    $(".campo-categoria").keyup(function () {
        if ($(this).val() != '') {
            $('.text-categoria').text($(this).val());
            $('.value-categoria').val($(this).val());
        } else {
            $('.text-categoria').text('Selecionar categoria');
            $('.value-categoria').val('');
        }
    });

    $('.adicional-lista').on('click', function () {
        $('#modal-adicional .modal-title').text('');
        $('#modal-adicional .nomeAdicional').val('');
        $('#modal-adicional .valorAdicional').val('');
        $('#modal-adicional .quantidadeAdicional').val('');
        $('#modal-adicional #text-categoria').text('');
        $('#modal-adicional .tmp-categoria-adicional').val('');
        $('#modal-adicional .tmp-keypush-adicional').val('');
        $('#modal-adicional .tmp-codbarras-adicional').val('');

        var ref = $(this).attr('ref');
        $('#modal-adicional .modal-title').text($('.adicional-lista .nome-' + ref).val());
        $('#modal-adicional .nomeAdicional').val($('.adicional-lista .nome-' + ref).val());
        $('#modal-adicional .valorAdicional').val($('.adicional-lista .valor-' + ref).val());
        $('#modal-adicional .quantidadeAdicional').val($('.adicional-lista .quantidade-' + ref).val());
        $('#modal-adicional #text-categoria').text($('.adicional-lista .categoria-' + ref).val());
        $('#modal-adicional .tmp-categoria-adicional').val($('.adicional-lista .categoria-' + ref).val());
        $('#modal-adicional .tmp-keypush-adicional').val($('.adicional-lista .keypush-' + ref).val());
        $('#modal-adicional .tmp-codbarras-adicional').val($('.adicional-lista .codbarras-' + ref).val());
        $('#modal-adicional').modal('show');
    });

    $('#chat-button').on('click', function (event) {
        $('#chat-list').toggle();
    });

    $('.chat-template').on('click', function (event) {
        $('.chat-menus').hide();
        $('.chat-area').show();
        $('.arrow-back').show();
        $('.chat-key').text($(this).attr('ref') + ' - ' + $(this).text());
        $('.chat-key').show();
        $('#key-value').val($(this).attr('ref'));
        $.post("../chat.php", {
            id: $(this).attr('ref')
        },
            function (data, status) {
                $("#chat-list .chat-all").append(data);
                scrollChat();
            });
    });

    $('.arrow-back').on('click', function (event) {
        $(this).hide();
        $('.chat-area').hide();
        $('.chat-menus').show();
        $('#key-value').val('');
        $('.chat-key').text('');
        $('.chat-key').show();
        $('#chat-list .chat-all .chat').remove();
    });

    $('#chat-send').on('click', function (event) {
        $.post("../enviarMensagem.php", {
            key: $('#key-value').val(),
            text: $('.chat-text').val(),
            url: $('.url-imagem').val()
        },
            function (data, status) {
                $("#chat-list .chat-all").append("<div class='chat chat-in'><p>" + $('.chat-text').val() + "</p></div>");
                $('.chat-text').val('');
                scrollChat();
            });
    });

    $(".chat-text").keypress(function (event) {
        if (event.key == 'Enter') {
            $.post("../enviarMensagem.php", {
                key: $('#key-value').val(),
                text: $('.chat-text').val(),
                url: $('.url-imagem').val()
            },
                function (data, status) {
                    $("#chat-list .chat-all").append("<div class='chat chat-in'><p>" + $('.chat-text').val() + "</p></div>");
                    $('.chat-text').val('');
                    scrollChat();
                });
        }
    });

    $(document).mouseup(function (e) {
        var container = $('#chat-list');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.hide();
        }
    });

    $('.btn-contato').on('click', function (event) {
        $('#chat-list .chat-all .chat').remove();
        $('#chat-list').toggle();
        $('.chat-menus').hide();
        $('.chat-area').show();
        $('.arrow-back').show();
        $('.chat-key').text($(this).val());
        $('.chat-key').show();
        $('#key-value').val($(this).val());
        $.post("../chat.php", {
            id: $(this).val()
        },
            function (data, status) {
                $("#chat-list .chat-all").append(data);
                scrollChat();
            });
    });

    $('.btn-atualizar-status').on('click', function (event) {
        startLoading();
        var key = $(this).val();
        $.post("../atualizarStatus.php", {
            key: key
        },
            function (data, status) {
                stopLoading();
                alert("Status da venda " + key.split(";")[1] + " atualizado!");
                document.location.reload(true);
            });
    });

    $('.cupom-input').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

    $('.read-only').focus(function () {
        $(this).attr("readonly", true);
    });

    $('.check-adicional').on('change', function () {
        if (this.checked) {
            $('.dropdown-adicionais').show();
        } else if (!this.checked) {
            $('.dropdown-adicionais').hide();
        }
    });

    $('.check-ing').on('change', function () {
        if (this.checked) {
            $('.dropdown-ingredientes').show();
        } else if (!this.checked) {
            $('.dropdown-ingredientes').hide();
        }
    });

    $('.check-cod-barra').on('change', function () {
        if (this.checked) {
            $('.campo-cod-barras').show();
            $('.campo-cod-barras').attr('required', 'true');
        } else if (!this.checked) {
            $('.campo-cod-barras').hide();
            $('.campo-cod-barras').attr('required', 'false');
        }
    });

    $('.radio-cadastro').on('change', function () {
        if ($(this).val() == 's') {
            $('.campo-detalhado').hide();
            $('.campo-simples').show();
        } else if ($(this).val() == 'd') {
            $('.campo-detalhado').show();
            $('.campo-simples').hide();
        }
    });

    $('.table-ingredientes #check-list-prod').change(function () {
        $(".check-in").each(function (i) {
            if (this.checked) {
                $(".number-in:eq( " + i + " )").prop("disabled", false);
                $(".number-val:eq( " + i + " )").prop("disabled", false);
            } else {
                $(".number-in:eq( " + i + " )").prop("disabled", true);
                $(".number-val:eq( " + i + " )").prop("disabled", true);
            }
        });
    });

    $('.check-list-tipo-produto').on('change', function () {
        $('.check-list-tipo-produto').not(this).prop('checked', false);
    });

    $(document).on('click', '#close-nv', function () {
        $('#nova-venda').remove();
    });

    $(document).on('click', '.aguardando-vendas', function () {
        $('.slider-ver-vendas-disp').click();
    });

    $(document).on('click', '.icon-ver-vendas-disp', function () {
        $('.slider-ver-vendas-disp').click();
    });

    printTermal();
    verifyChat();

    $(document).on('change', '.slider-ver-vendas-disp', function () {
        if (this.checked) {
            $('.aguardando-vendas .blink').show();
            printTermal();
        } else {
            $('.aguardando-vendas .blink').hide();
            $('#nova-venda').hide();
        }
    });

    $('.table-adicionais tr').click(function (event) {
        if ($(event.target).is('input[type=number]')) {
            return;
        } else {
            if (event.target.type !== 'checkbox') {
                $(':checkbox', this).trigger('click');
            }
        }
    });

    $('.table-ingredientes tr').click(function (event) {
        if ($(event.target).is('input[type=number]')) {
            return;
        } else {
            if (event.target.type !== 'checkbox') {
                $(':checkbox', this).trigger('click');
            }
        }
    });
});

function scrollChat() {
    var elem = document.getElementById('chat-all');
    elem.scrollTop = elem.scrollHeight;
}

function alterarTipo(value) {
    $("#form-tipo-radio").val(value);
    $("#form-tipo").submit();
}

function printTermal() {
    if ($('.slider-ver-vendas-disp').is(":checked")) {

        $.ajax({
            url: '../verifyPrinted.php',
            success: function (response) {
                $('#nova-venda').remove();
                $('body').append(response);
            }
        });

        setTimeout(function () {
            if (document.getElementById("nova-venda") !== null) {
                $(".venda").each(function (i) {
                    var printContents = $(this).html();
                    var originalContents = document.body.innerHTML;

                    document.body.innerHTML = printContents;

                    window.print();

                    $.post("../setPrinted.php", {
                        codigo: $(".title", this).text()
                    },
                        function (data, status) {
                            if (status == "success") {
                                $(this).remove();
                                printTermal();
                            } else {
                                alert('Algo deu errado...');
                            }
                        });

                    document.body.innerHTML = originalContents;
                });
            } else {
                printTermal();
            }
        }, 30000);
    }
}

function verifyChat() {
    setTimeout(function () {
        $.post("../verifyChat.php", {

        },
            function (data, status) {
                if (status == "success") {
                    if (data > 0) {
                        $('#cont-chat').show();
                        $('#cont-chat').text(data);
                    }
                } else {
                    alert('Algo deu errado...');
                }
            });

        verifyChat();
    }, 30000);
}

function startLoading() {
    $('body').append("<div id='loading-content' style='position: fixed; background: #f8f9fa; width: 100%; height: 83%; top: 203px; left: 0; z-index: 999999999999999;'><img class='center-div' src='../images/gifs/loading.gif' /></div>");
}

function stopLoading() {
    $('#loading-content').remove();
}

/****************************/

/*$('.table-ingredientes #check-list').change(function () {
    var checked = $(this).attr('ref');
    $(".table-ingredientes ." + checked + "").each(function (i) {
        if ($(this).prop("disabled", true)) {
            $(this).prop("disabled", false);
        } else {
            $(this).prop("disabled", true);
        }
    });
});
$('.table-adicionais #check-list').change(function () {
    var value = $(this).attr('ref');
    if (this.checked) {
        $(".table-adicionais ." + value + "").each(function (i) {
            $(this).prop("disabled", false);
        });
    } else {
        $(".table-adicionais ." + value + "").each(function (i) {
            $(this).prop("disabled", true);
        });
    }
});*/