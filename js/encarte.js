$(document).ready(function () {
    $('#loading-content').remove();
    
    var globalPosition = -1;
    var opened;

    var globalDocument;
    $(document).on('click', '.produto', function () {
        if (!$(this).hasClass("changed")) {
            globalDocument = this;
            $('#lista-produtos').show();
            globalPosition = $(this).attr('pos');
        }
    });

    $(document).on('click', '.produto img', function () {
        globalDocument = this;
        $('#lista-produtos').show();
        globalPosition = $(this).attr('pos');
    });

    $(document).on('contextmenu', '.produto img', function () {
        var posImg = $(this).attr('pos');
        $(".upload-" + posImg).click();

        var readURL = function (input, posImg) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.produto-foto').each(function (i) {
                        if ($(this).attr('pos') == posImg) {
                            $(this).attr('src', e.target.result);
                        }
                    });
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '.file-upload', function () {
            readURL(this, $(this).attr('pos'));
        });
    });

    $(document).on('click', '.item-produto', function () {
        $(globalDocument).addClass("changed")
        if (globalPosition > -1) {
            var pos = $(this).attr('pos');
            $('#lista-produtos').hide();
            $('.image-area-' + globalPosition + ' img').attr("src", $('.image-' + pos).attr("src"));
            $('.title-area-' + globalPosition + ' input').val($('.title-' + pos).text());
            $('.value-area-' + globalPosition + ' input').val('R$ ' + (Math.round($('.value-' + pos).text() * 100) / 100).toFixed(2));
        }
    });

    $('.btn-encarte').click(function () {
        $("html").addClass("remove-empty-space");
        window.scrollTo(0, 0);
        html2canvas(document.querySelector("#encarte"), { allowTaint: true, scale: 3 }).then(function (canvas) {
            saveColor();
            $("#modal-encarte .modal-body canvas").remove();
            $("#modal-encarte .modal-body").append(canvas);
            $('#modal-encarte').modal('show');
        });
        $("html").removeClass("remove-empty-space");
    });

    function saveColor(){
        $.post("../encarte/saveColor.php", {
            nome: $('#nomeCod').val(),
            data: $('#color-picker').val()
        },
            function (data, status) {
                
            });
    }

    /*function uploadEncarte(canvas) {
    var dataURL = canvas.toDataURL("image/png");
    document.getElementById('hidden_data').value = dataURL;
    var fd = new FormData(document.forms["form1"]);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '../encarte/uploadEncarte.php', true);

    xhr.upload.onprogress = function (e) {
        if (e.lengthComputable) {
            var percentComplete = (e.loaded / e.total) * 100;
            console.log(percentComplete + '% uploaded');
            alert('Encarte salvo');
        }
    };

    xhr.onload = function () {

    };
    xhr.send(fd);
    };*/

    $('#lista-produtos #close').click(function () {
        $('#lista-produtos').hide();
    });

    $('.check-fundo').on('change', function () {
        if ($(this).val() == "sfundo") {
            $('.fundo').attr("src", 'fundos/opcao2.jpg');
        } else if ($(this).val() == "cfundo") {
            $('.fundo').attr("src", 'fundos/opcao1.jpg');
        }
    });

    $('.check-quant-prod').on('change', function () {
        atualizarProd($(this).val());
    });

    function atualizarProd(quant) {
        $("#encarte .area-produtos div").remove();

        var produtos = '';
        if (quant < 3) {
            produtos = "<div class='area-1' style='width: 100%;height: 100%; display:inline-flex'>";
            for (var i = 1; i <= quant; i++) {
                produtos += "<div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='vertical-center-rel'><div class='image-area-" + i + "' style='text-align: center;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div></div>";
            }
            produtos += "</div>";
        } else if (quant < 5) {
            produtos = "<div class='area-1' style='width: 100%;height: 33.6em; display:inline-flex'>";
            for (var i = 1; i <= quant; i++) {
                if (i < 3) {
                    produtos += "<div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='vertical-center-rel'><div class='image-area-" + i + "' style='text-align: center;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div></div>";
                } else if (i == 3) {
                    produtos += "</div><div class='area-2' style='width: 100%;height: 33.6em; display:inline-flex'><div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='image-area-" + i + "' style='text-align: center; padding: 50px 0 0 0;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div>";
                } else {
                    produtos += "<div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='image-area-" + i + "' style='text-align: center; padding: 50px 0 0 0;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div>";
                }
            }
            produtos += "</div>";
        } else if (quant < 7) {
            produtos = "<div class='area-1' style='width: 100%;height: 33.6em; display:inline-flex'>";
            for (var i = 1; i <= quant; i++) {
                if (i < 4) {
                    produtos += "<div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='vertical-center-rel'><div class='image-area-" + i + "' style='text-align: center;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div></div>";
                } else if (i == 4) {
                    produtos += "</div><div class='area-2' style='width: 100%;height: 33.6em; display:inline-flex'><div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='image-area-" + i + "' style='text-align: center; padding: 50px 0 0 0;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div>";
                } else {
                    produtos += "<div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='image-area-" + i + "' style='text-align: center; padding: 50px 0 0 0;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div>";
                }
            }
            produtos += "</div>";
        } else {
            produtos = "<div class='area-1' style='width: 100%;height: 33.6em; display:inline-flex'>";
            for (var i = 1; i <= quant; i++) {
                if (i < 5) {
                    produtos += "<div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='vertical-center-rel'><div class='image-area-" + i + "' style='text-align: center;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div></div>";
                } else if (i == 5) {
                    produtos += "</div><div class='area-2' style='width: 100%;height: 33.6em; display:inline-flex'><div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='image-area-" + i + "' style='text-align: center; padding: 50px 0 0 0;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div>";
                } else {
                    produtos += "<div class='produto col' pos='" + i + "' style='width: 25%;height: 100%;'><div class='image-area-" + i + "' style='text-align: center; padding: 50px 0 0 0;'><img class='produto-foto' style='height: 140px;' pos='" + i + "'><div class='p-image'><input name='file' class='file-upload upload-" + i + "' pos='" + i + "' type='file' accept='image/*' /></div></div><div class='title-area-" + i + "' style='text-align: center;'><input type='text' value='TÍTULO' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#9a2125; width: 100%; font-weight: 700; font-size: 3em; text-align: center;'></div><div class='value-area-" + i + "' style='text-align: center;'><input type='text' value='R$ 00,00' style='border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fb6400; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;'></div></div>";
                }
            }
            produtos += "</div>";
        }
        $("#encarte .area-produtos").append(produtos);
    }

    var readURL = function (input, pos) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').each(function (i) {
                    if ($(this).attr('pos') == pos) {
                        $(this).attr('src', e.target.result);
                    }
                });
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $(document).on('change', '.file-upload', function () {
        readURL(this, $(this).attr('pos'));
        closeProfileHistory();
    });

    var pos;
    $(document).on('click', '.upload-area-encarte', function () {
        pos = $(this).attr('pos');
        openSaveProfile();
    });

    $(document).on('click', '.profile-img', function () {
        var src = $(this).attr('src');
        $('.profile-pic').each(function (i) {
            if ($(this).attr('pos') == pos) {
                if (!$(this).hasClass("changed")) {
                    $(this).addClass("changed")
                }
                $(this).attr('src', src);
                closeProfileHistory();
            }
        });
    });

    $(document).on('change', '.upload-profile', function () {
        $('#upload-image-form').submit();
    });

    /*$("#upload-image-form").on("submit", function (e) {
        var dataString = $(this).serialize();

        $.ajax({
            type: "POST",
            url: "uploadImg.php",
            file: dataString,
            success: function () {
                alert('Logo salva no histórico');
            }
        });

        e.preventDefault();
    });*/

    $(document).on('click', '.upload-image-btn', function () {
        $(".upload-" + pos).click();
    });

    $(document).on('click', '#close-preview-history', function () {
        closeProfileHistory()
    });

    function openSaveProfile() {
        var dt = new Date();
        dt = dt.getHours() + '' + dt.getMinutes() + '' + dt.getSeconds();

        var nomeCod = $('#nomeCod').val();

        var img1 = "<div class='area-image-history col'><img class='profile-img' pos='1' src='https://www.nomercadosoft.com.br/imagens/encartes/" + nomeCod + "/profiles/profile1.png?" + dt + "' alt='' onerror=this.style.display='none'></div>";
        var img2 = "<div class='area-image-history col'><img class='profile-img' pos='2' src='https://www.nomercadosoft.com.br/imagens/encartes/" + nomeCod + "/profiles/profile2.png?" + dt + "' alt='' onerror=this.style.display='none'></div>";
        var img3 = "<div class='area-image-history col'><img class='profile-img' pos='3' src='https://www.nomercadosoft.com.br/imagens/encartes/" + nomeCod + "/profiles/profile3.png?" + dt + "' alt='' onerror=this.style.display='none'></div>";
        var img4 = "<div class='area-image-history col'><img class='profile-img' pos='4' src='https://www.nomercadosoft.com.br/imagens/encartes/" + nomeCod + "/profiles/profile4.png?" + dt + "' alt='' onerror=this.style.display='none'></div>";
        var img5 = "<div class='area-image-history col'><img class='profile-img' pos='5' src='https://www.nomercadosoft.com.br/imagens/encartes/" + nomeCod + "/profiles/profile5.png?" + dt + "' alt='' onerror=this.style.display='none'></div>";

        $(".site-section").append("<div class='center-div profileHistory' style='height:25%;width:50%;background:#fefefe;z-index:999999999999; display: inline-flex;'><div style='height:100%;width:70%;border-right: 1px solid #cecece;'><div style='height:30%;width:100%; text-align:center; padding-top:10px'><h3>Ultimas logos usadas:</h3></div><div style='height:50%;width:100%; text-align:center; padding-top:10px'><div style='height:70%;width:100%; text-align:center; padding-top:10px; display:inline-flex;'>" + img1 + img2 + img3 + img4 + img5 + "</div></div><div style='height:20%;width:100%;text-align:center;'><a id='return-logo' style='cursor:pointer;'>Voltar logo padrão</a></div></div><div style='height:100%;width:30%;text-align:center; '><div><h3 style='margin-top:10px'>Buscar do computador</h3><button class='upload-image-btn btn btn-danger' style='margin-top:70px; font-size: 16px;'>Buscar</button><div></div><button id='close-preview-history' class='btn btn-danger' style='position: fixed;right: -10px;top: -10px; font-size: 18px; border-radius: 50%; padding:0 7px 2px 7px'>x</button></div>");

        var count = 1;
        $(".profile-history").each(function () {
            $($(this))
                .on('load', function () { count++; })
                .on('error', function () { });
        });
    }

    $(document).on('click', '#return-logo', function () {
        $('.profile-pic').each(function (i) {
            if ($(this).attr('pos') == pos) {
                $(this).attr('src', $('#profile-pic-default').val());
            }
        });
        closeProfileHistory();
    });

    function closeProfileHistory() {
        $('.profileHistory').remove();
    }

    /*$(document).on('keyup', '.color-adjust', function () {
        var value = $(this).val() / 100;

        if (value > 100) {
            $('.color-identity').css('opacity', 1);
        } else if (value == 100) {
            $('.color-identity').css('opacity', 1);
        } else {
            $('.color-identity').css('opacity', value);
        }
    });

    $(document).on('change', '.color-adjust', function () {
        var value = $(this).val() / 100;

        if (value > 100) {
            $('.color-identity').css('opacity', 1);
        } else if (value == 100) {
            $('.color-identity').css('opacity', 1);
        } else {
            $('.color-identity').css('opacity', value);
        }
    });*/

    $('#color-picker').spectrum({
        type: "component"
    });

    $(document).on('change', '#color-picker', function () {
        $('.color-identity').css('background-color', $(this).val());
    });
});