<?php
error_reporting(E_ALL ^ E_NOTICE);
session_start();
if ($_SESSION['nameUser'] == '' || $_SESSION['typeUser'] == '' || $_SESSION['cnpjUser'] == '') {
	header("Location: login.php");
} else {
	$globalData['cnpj'];
	$tipo = $_SESSION['typeUser'];
	$globalData['nameUser'];
	$globalData['codEst'];
	$globalData['url'] = $_SESSION['url'];
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>No Mercado</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="http://nomercadosoft.com.br/imagens/Originais/ldpi.png">
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<header role="banner">
	<div class='container'>
		<div class="row align-items-center">
			<div class="col-10 main-menu" style="display:flex">
				<di class="col-10" style="display:flex">
					<div class="col-4 dropdown">
						<button class="dropbtn"><?php echo $globalData['nameUser']; ?></button>
						<input class="url-imagem" value='<?php echo $globalData['url']; ?>' style='display:none'></input>
						<div class="dropdown-content">
							<a href="index.php">Inicio</a>
							<a href="#">Ver perfil</a>
						</div>
					</div>
					<div class="col-2 dropdown">
						<button class="dropbtn">Foods</button>
						<div class="dropdown-content">
							<div class="col dropdown">
								<h3 style='color:black; margin:5px'>Produtos</h3>
								<a href="cadastrar-produto.php">Cadastrar</a>
								<a href="ver-produtos.php">Visualizar</a>
							</div>
							<div class="col dropdown">
								<h3 style='color:black; margin:5px'>Adicionais</h3>
								<a href="cadastrar-adicional.php">Cadastrar</a>
								<a href="ver-adicional.php">Visualizar</a>
							</div>
							<div class="col dropdown">
								<h3 style='color:black; margin:5px'>Vendas</h3>
								<a href="ver-vendas.php">Visualizar</a>
							</div>
						</div>
					</div>
					<div class="col-2 dropdown">
						<button class="dropbtn">Supermercados</button>
						<div class="dropdown-content">
							<div class="col dropdown">
								<h3 style='color:black; margin:5px'>Produtos</h3>
								<a href="supermercados/cadastrar-produto.php">Cadastrar</a>
								<a href="supermercados/ver-produtos.php">Visualizar</a>
							</div>
							<div class="col dropdown">
								<h3 style='color:black; margin:5px'>Vendas</h3>
								<a href="supermercados/ver-vendas.php">Visualizar</a>
							</div>
						</div>
						<!--<div class="dropdown-content">
							<div class="col dropdown">
								<h3 style='color:black; margin:5px'>Produtos</h3>
								<a href="cadastrar-excel.php">Cadastrar</a>
								<a href="ver-produtos.php">Visualizar</a>
							</div>
							<div class="col dropdown">
								<h3 style='color:black; margin:5px'>Adicionais</h3>
								<a href="cadastrar-adicional.php">Cadastrar</a>
								<a href="ver-adicional.php">Visualizar</a>
							</div>
						</div>-->
					</div>
					<div class="col-2 dropdown">
						<a href="cupom.php"><button class="dropbtn">Cupons</button></a>
					</div>
			</div>
			<div class="col-2" style="display:flex">
				<li class="col-2"><a href="sair.php" class="col-12">Sair</a></li>
			</div>
		</div>
	</div>
	</div>
</header>