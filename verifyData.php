<?php
if ($_COOKIE['6zu41s'] != null && $_COOKIE['10zu41s'] != null && $_COOKIE['14zu41s'] == null) {
	session_start();

	$url = 'https://us-central1-nomercadoapp.cloudfunctions.net/api/v1/auth/login';

	$jsonData = [
		"email" => $_COOKIE['6zu41s'],
		"password" => $_COOKIE['10zu41s'],
		"keyWeb" => "AIzaSyARWxQfeawBVMkdnefN0YfQZN3-ciSK_Q0"
	];

	$options = array(
		'http' => array(
			'method'  => 'POST',
			'content' => json_encode($jsonData),
			'header' =>  "Content-Type: application/json\r\n" .
				"Accept: application/json\r\n"
		)
	);

	$context  = stream_context_create($options);
	$result = file_get_contents($url, true, $context);

	$jsonData = json_decode($result, true);

	//echo json_encode($jsonData);

	if ($jsonData['statusCode'] == 200) {
		$_SESSION['cnpjUser'] = $jsonData['items'][0]['cnpj']; // CNPJ Supermercado
		$_SESSION['typeUser'] = $jsonData['items'][0]['tipoEstabelecimento']; // Tipo Estabelecimento
		$_SESSION['nameUser'] = $jsonData['items'][0]['nome']; // Nome Supermercado
		//$_SESSION['planilhaLinha'] = $jsonData['items'][0]['planilhaLinha'];// Linha Planilha
		//$_SESSION['planilhaColunas'] = $jsonData['items'][0]['planilhaColuna'];// Colunas Planilha
		$_SESSION['codEst'] = $jsonData['items'][0]['codigoEstabelecimento']; // Código Estabelecimento
		$_SESSION['url'] = $jsonData['items'][0]['url']; // Url Imagem
		$_SESSION['tipoEstabelecimento'] = $jsonData['items'][0]['tipoEstabelecimento']; // Tipo Estabelecimento
		$_SESSION['telefone'] = $jsonData['items'][0]['telefone']; // Telefone Estabelecimento
		if (isset($jsonData['items'][0]['marketplaceRoute'])) {
			$_SESSION['marketplaceRoute'] = $jsonData['items'][0]['marketplaceRoute']; // Telefone Estabelecimento
		}

		header("Location: home/index.php");
	} else {
		header("Location: login.php");
	}
} else if ($_COOKIE['6zu41s'] != null && $_COOKIE['10zu41s'] != null && $_COOKIE['14zu41s'] != null) {
	header("Location: ../".$_COOKIE['14zu41s']."/verifyData.php");
}else{
	header("Location: login.php");
}
