<?php
include_once('rotas.php');

$urlChat = $rotas['api'] . 'chat/listar/';

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst']
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$resultChat = file_get_contents($urlChat, true, $context);

$jsonDataChat = json_decode($resultChat, true);

//echo json_encode($jsonDataChat);

$menus = '';
foreach ($jsonDataChat as $id => $values) {
    $menus .= "
        <div class='chat-template chat-1' ref='$id'>" . $values['nomeUsuario'] . "</div>
    ";
}

?>
<div id="chat-list">
    <div class='chat-header'>
        <h2>Chat</h2>
        <h5 class="chat-key" style="display: none;"></h5>
        <i class="fa fa-chevron-left arrow-back"></i>
    </div>
    <div class='chat-menus'>
        <?php echo $menus; ?>
    </div>
    <div class='chat-area' style="display: none;">
        <div id="chat-all" class="chat-all">
            <input id='key-value' value='' style='display:none'></input>
        </div>
        <div class="chat-text-field">
            <input type="text" class='chat-text' name="chat-text">
            <a id="chat-send"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
        </div>
    </div>
</div>
<a id="chat-button" class="float">
    <i class="fa fa-comments my-float"></i>
    <span id="cont-chat" style="display:none;background: #ffffff; border-radius: 50%; color: #000000; padding: 0px 13px; font-size: 18px; font-weight: 700; position: absolute; top: -10px; right: -10px;"></span>
</a>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<script src="../vendor/animsition/js/animsition.min.js"></script>
<script src="../vendor/bootstrap/js/popper.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="../vendor/select2/select2.min.js"></script>
<script src="../vendor/daterangepicker/moment.min.js"></script>
<script src="../vendor/daterangepicker/daterangepicker.js"></script>
<script src="../vendor/countdowntime/countdowntime.js"></script>
<script src="../js/js.js"></script>
<script src="../js/mascaras.js"></script>
<script src="../js/parceiros.js"></script>
<script>
    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
    }
</script>
<script src="../js/html2canvas.min.js"></script>
<script src="../js/dom-to-image.min.js"></script>
</body>

</html>