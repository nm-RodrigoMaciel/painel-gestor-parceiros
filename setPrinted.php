<?php
$postCodigo = trim(htmlspecialchars($_POST['codigo'], ENT_QUOTES));		

include_once('rotas.php');
include_once('variables.php');

$globalData['nameUser'];
$globalData['codEst'];

//API Url
$url = $rotas['api'] . 'sauros/vendas/printed';

$jsonData = [
	"nomeEstabelecimento" => $globalData['nameUser'],
		"codigoEstabelecimento" => $globalData['codEst'],
    "codigoTransacao" => $postCodigo,
    "isPrinted" => true
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$resultList = file_get_contents($url, true, $context);
