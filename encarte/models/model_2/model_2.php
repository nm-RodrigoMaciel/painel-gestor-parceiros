<div class="body" style="position: absolute; z-index: 9999; width: 111em;">
    <div class="cabecalho" style="width: 100%; height: 54.6em;">
        <div style="display:inline-flex; width: 100%; height: 46em;">
            <div class="area-1" style="width: 55%; padding:35px 0 0 40px;">
                <input type="text" value="FIM DE SEMANA" style="border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fdfafa; width: 100%; font-weight: 700; font-size: 7.5em;">
                <input type="text" value="     DA CARNE" style="border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#facb04; width: 100%; font-weight: 700; font-size: 7.5em; margin:-55px 0 0 0">
            </div>
            <div class="area-2" style="width: 45%; padding:40px 0 0 20px;">
                <input type="text" value="Acompanhe nossas ofertas" style="border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fdfafa; width: 100%; font-weight: 700; font-size: 2.5em;text-align: center;">
                <input type="text" value="<?php echo $globalData['telefone']; ?>" style="border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fdfafa; width: 100%; font-weight: 700; font-size: 2.5em;text-align: center;">
                <div class='upload-area-encarte' pos='profile' style="width: 100%; height: 15.4em; padding: 5px 150px 0 150px; text-align: center; z-index: 999999;">
                    <img class='profile-pic' pos='profile' src='<?php echo $globalData['url']; ?>' style="height: 145px; margin: 0 auto; width: auto;">
                    <input id='profile-pic-default' value='<?php echo $globalData['url']; ?>' style="display:none">
                </div>
                <div class='p-image'>
                    <form id="upload-image-form" target="_blank" method="post" enctype="multipart/form-data" action="uploadImg.php">
                        <input name='file' class='file-upload upload-profile' pos='profile' type='file' accept='image/*' />
                        <input id="nomeCod" name='nome' value='<?php echo $globalData['nameUser'] . $globalData['codEst']; ?>' style='display: none;' />
                    </form>
                </div>
            </div>
        </div>
        <div class="area-3" style="width: 100%; height: 8.6em; padding: 8px 0 0 15px;">
            <input type="text" value="Ofertas válidas dias 15/05/2020 e 16/06/2020" style="border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fdfafa; width: 100%; font-weight: 700; font-size: 1.8em;">
        </div>
    </div>
    <div class="middle" style="width: 100%; height: 78.6em;">
        <div class="area-produtos" style="height: 67.2em;">
            <div class="area-1" style="width: 100%; height: 34.4em; padding-left: 20px; padding-top: 45px; display: inline-flex;">
                <div class="produto" pos="1" style="width: 229px; height: 297px; margin-right: 45px;">
                    <div style="height: 297px;">
                        <div class="title-area-1" style="text-align: center; margin-top:15px;">
                            <input type="text" value="TÍTULO" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #49790f; width: 100%; font-weight: 700; font-size: 2em; text-align: center;" />
                        </div>
                        <div class="image-area-1" style="text-align: center;">
                            <img class="produto-foto" style="height: 185px;" pos="1" />
                            <div class="p-image"><input name="file" class="file-upload upload-1" pos="1" type="file" accept="image/*" /></div>
                        </div>
                        <div class="value-area-1" style="text-align: center;">
                            <input type="text" value="R$ 00,00" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #4a7a0f; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;" />
                        </div>
                    </div>
                </div>
                <div class="produto" pos="2" style="width: 229px; height: 298px; margin-right: 55px;">
                    <div class="vertical-center-rel">
                        <div class="image-area-2" style="text-align: center;">
                            <img class="produto-foto" style="height: 140px;" pos="2" />
                            <div class="p-image"><input name="file" class="file-upload upload-2" pos="2" type="file" accept="image/*" /></div>
                        </div>
                        <div class="title-area-2" style="text-align: center;">
                            <input type="text" value="TÍTULO" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #49790f; width: 100%; font-weight: 700; font-size: 3em; text-align: center;" />
                        </div>
                        <div class="value-area-2" style="text-align: center;">
                            <input type="text" value="R$ 00,00" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #4a7a0f; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;" />
                        </div>
                    </div>
                </div>
                <div class="produto" pos="3" style="width: 229px; height: 297px; margin-right: 54px;">
                    <div class="vertical-center-rel">
                        <div class="image-area-3" style="text-align: center;">
                            <img class="produto-foto" style="height: 140px;" pos="3" />
                            <div class="p-image"><input name="file" class="file-upload upload-3" pos="3" type="file" accept="image/*" /></div>
                        </div>
                        <div class="title-area-3" style="text-align: center;">
                            <input type="text" value="TÍTULO" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #49790f; width: 100%; font-weight: 700; font-size: 3em; text-align: center;" />
                        </div>
                        <div class="value-area-3" style="text-align: center;">
                            <input type="text" value="R$ 00,00" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #4a7a0f; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;" />
                        </div>
                    </div>
                </div>
                <div class="produto" pos="4" style="width: 229px; height: 298px;">
                    <div class="vertical-center-rel">
                        <div class="image-area-4" style="text-align: center;">
                            <img class="produto-foto" style="height: 140px;" pos="3" />
                            <div class="p-image"><input name="file" class="file-upload upload-3" pos="3" type="file" accept="image/*" /></div>
                        </div>
                        <div class="title-area-4" style="text-align: center;">
                            <input type="text" value="TÍTULO" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #49790f; width: 100%; font-weight: 700; font-size: 3em; text-align: center;" />
                        </div>
                        <div class="value-area-4" style="text-align: center;">
                            <input type="text" value="R$ 00,00" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #4a7a0f; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="area-2" style="width: 100%; height: 32.3em; padding-left: 22px; padding-top: 23px; display: inline-flex;">
                <div class="produto" pos="5" style="width: 229px; height: 298px; margin-right: 45px;">
                    <div class="image-area-5" style="text-align: center; padding: 50px 0 0 0;">
                        <img class="produto-foto" style="height: 140px;" pos="4" />
                        <div class="p-image"><input name="file" class="file-upload upload-4" pos="4" type="file" accept="image/*" /></div>
                    </div>
                    <div class="title-area-5" style="text-align: center;">
                        <input type="text" value="TÍTULO" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #49790f; width: 100%; font-weight: 700; font-size: 3em; text-align: center;" />
                    </div>
                    <div class="value-area-5" style="text-align: center;">
                        <input type="text" value="R$ 00,00" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #4a7a0f; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;" />
                    </div>
                </div>
                <div class="produto" pos="6" style="width: 229px; height: 299px; margin-right: 55px;">
                    <div class="image-area-6" style="text-align: center; padding: 50px 0 0 0;">
                        <img class="produto-foto" style="height: 140px;" pos="5" />
                        <div class="p-image"><input name="file" class="file-upload upload-5" pos="5" type="file" accept="image/*" /></div>
                    </div>
                    <div class="title-area-6" style="text-align: center;">
                        <input type="text" value="TÍTULO" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #49790f; width: 100%; font-weight: 700; font-size: 3em; text-align: center;" />
                    </div>
                    <div class="value-area-6" style="text-align: center;">
                        <input type="text" value="R$ 00,00" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #4a7a0f; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;" />
                    </div>
                </div>
                <div class="produto" pos="7" style="width: 229px; height: 298px; margin-right: 54px;">
                    <div class="image-area-7" style="text-align: center; padding: 50px 0 0 0;">
                        <img class="produto-foto" style="height: 140px;" pos="6" />
                        <div class="p-image"><input name="file" class="file-upload upload-6" pos="6" type="file" accept="image/*" /></div>
                    </div>
                    <div class="title-area-7" style="text-align: center;">
                        <input type="text" value="TÍTULO" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #49790f; width: 100%; font-weight: 700; font-size: 3em; text-align: center;" />
                    </div>
                    <div class="value-area-7" style="text-align: center;">
                        <input type="text" value="R$ 00,00" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #4a7a0f; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;" />
                    </div>
                </div>
                <div class="produto" pos="8" style="width: 229px; height: 299px;">
                    <div class="image-area-8" style="text-align: center; padding: 50px 0 0 0;">
                        <img class="produto-foto" style="height: 140px;" pos="6" />
                        <div class="p-image"><input name="file" class="file-upload upload-6" pos="6" type="file" accept="image/*" /></div>
                    </div>
                    <div class="title-area-8" style="text-align: center;">
                        <input type="text" value="TÍTULO" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #49790f; width: 100%; font-weight: 700; font-size: 3em; text-align: center;" />
                    </div>
                    <div class="value-area-8" style="text-align: center;">
                        <input type="text" value="R$ 00,00" style="border: 0; appearance: none; -webkit-appearance: none; background-color: rgba(0, 0, 0, 0); color: #4a7a0f; width: 100%; font-weight: 700; font-size: 3.5em; margin: -20px 0 0 0; text-align: center;" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer" style="width: 100%; height: 27.7em;">
        <div class="color-identity" style="position:absolute; width: 100%; height: 19.9em; z-index:-1;"></div>
        <div class="area-1" style="width: 100%;height: 7.8em; padding:15px 0 0 0">
            <input type="text" value="Fique ligado em nossas ofertas!" style="border: 0; appearance: none; -webkit-appearance: none; background-color:rgba(0, 0, 0, 0); color:#fdfafa; font-weight: 700; font-size: 3em; margin: 0 auto; width: 100%;text-align: center;">
        </div>
        <div class="area-2" style="width: 45%;height: 20em;margin:0 auto; text-align: center;">
            <div class='upload-area-encarte' pos='profile' style="width: 100%; height: 15.4em; padding: 5px 150px 0 150px; text-align: center; z-index: 999999;">
                <img class='profile-pic' pos='profile' src='<?php echo $globalData['url']; ?>' style="height: 170px; margin: 0 auto; width: auto;">
            </div>
            <div class='p-image'>
                <input name='file' class='file-upload upload-profilef' pos='profile' type='file' accept='image/*' />
            </div>
        </div>
    </div>
</div>
<img class="fundo" src='models/model_2/fundos/opcao1.png'>