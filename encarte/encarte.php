<?php
include("../header.php");
include("../rotas.php");

$model = 1;
if ($_GET['model'] != null)
    $model = $_GET['model'];

//API Url
$urlProd = $rotas['api'] . 'web/produtos/listar';

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst']
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$resultProd = file_get_contents($urlProd, true, $context);
$jsonDataProd = json_decode($resultProd, true);

if (sizeOf($jsonDataProd) > 0) {
    $cont = 0;
    foreach ($jsonDataProd as $key => $value) {

        $Produtos[] = [
            "nome" => $value['produto'],
            "codigo" => $value['codigo'],
            "form" => "<div class='item-produto' pos='$count' style=' display: inline-flex; padding: 15px; text-align:center;'>
            <div class='image-area' style='margin-right: 25px; width:100px'>
                <img class=' image-$count' src='" . $value['url'] . "' style='height: 45px'>
            </div>
            <h2 class='col-6 title-$count' style='margin-right: 25px;  width:150px'>" . $value['produto'] . "</h2>
            <h2 class='col value-$count' style='width:100px'>" . round($value['valor'], 2) . "</h2>
        </div>"
        ];

        $count++;

        if (!isset($Produtos)) {
            $Produtos = "<div class='area-nada-por-aqui'><h3>Nenhum produto encontrado...</h3></div>";
        }
    }
}

include_once('../conn/id.php');

function escapefile_url($url)
{
    $parts = parse_url($url);
    $path_parts = array_map('rawurldecode', explode('/', $parts['path']));

    return
        $parts['scheme'] . '://' .
        $parts['host'] .
        implode('/', array_map('rawurlencode', $path_parts));
}

if (ftp_nlist($conn_id, "/public/imagens/encartes/" . $globalData['nameUser'] . $globalData['codEst'] . "/color.txt") == false) {
    $last_color = "transparent";
} else {
    $last_color = file_get_contents(escapefile_url("https://www.nomercadosoft.com.br/imagens/encartes/" . $globalData['nameUser'] . $globalData['codEst'] . "/color.txt"));
}

ftp_close($conn_id);
?>

<body id="target">
    <div class="site-section bg-light" id="planilha-section">
        <div class="container">
            <div class="col-12 text-center">
                <h2 class="section-title mb-3" style="margin-top:5px; height: 1.7em;">Gerenciar encartes</h2>
                <div style='position: absolute; top:0%;'>
                    <fieldset id="group1">
                        <label style='margin-right:3px' for='cfundo'>Com fundo</label>
                        <input class="check-fundo" type='radio' id='cfundo' name='group1' value='cfundo' style='margin-right:7px'>
                        <label style='margin-right:3px' for='sfundo'>Sem fundo</label>
                        <input class="check-fundo" type='radio' id='sfundo' name='group1' value='sfundo' checked>
                    </fieldset>
                </div>
                <div style='position: absolute; top:40%;'>
                    <fieldset id="group2">
                        <label style='margin-right:10px'>Quantidade produtos:</label>
                        <input class="check-quant-prod" type='radio' id='2' name='group2' value='2'>
                        <label style='margin-right:5px' for='2'>2</label>
                        <input class="check-quant-prod" type='radio' id='3' name='group2' value='3'>
                        <label style='margin-right:5px' for='3'>3</label>
                        <input class="check-quant-prod" type='radio' id='4' name='group2' value='4'>
                        <label style='margin-right:5px' for='4'>4</label>
                        <input class="check-quant-prod" type='radio' id='5' name='group2' value='5'>
                        <label style='margin-right:5px' for='5'>5</label>
                        <input class="check-quant-prod" type='radio' id='6' name='group2' value='6' checked>
                        <label style='margin-right:5px' for='6'>6</label>
                        <input class="check-quant-prod" type='radio' id='7' name='group2' value='7'>
                        <label style='margin-right:5px' for='7'>7</label>
                        <input class="check-quant-prod" type='radio' id='8' name='group2' value='8'>
                        <label style='margin-right:5px' for='8'>8</label>
                    </fieldset>
                </div>
                <div style='position: absolute; top:79%;'>
                    <label style='margin-right:10px'>Cor de fundo:</label>
                    <input id="color-picker" value='<?php echo $last_color; ?>' style="box-shadow: 2px 2px 4px #d2d2d2; border-radius: 3px;" />
                </div>
            </div>
            <div id="encarte">
                <div id="loading-content" style="position: fixed; background: #f8f9fa; width: 100%; height: 100%; z-index: 999999999999;">
                    <img class="center-div" src="../images/gifs/loading.gif" />
                </div>
                <?php include_once('models/model_2/model_2.php'); ?>
            </div>
            <div id="editor"></div>
            <div style="width: 100%; text-align: center; padding: 10px 0 10px 0;">
                <button class="btn btn-encarte btn-danger" style="font-size: 16px;">Gerar encarte</button>
            </div>
        </div>
    </div>
    <div id="lista-produtos" class="center-div" style="z-index: 99999; background: #ffffff; overflow: auto; height: 90%; width: 90%; display: none; text-align: center;">
        <img class="loading" src="../images/gifs/loading.gif" style="position: absolute; margin-top: 26px;">
        <button id="close" class='btn btn-danger' style='position: fixed;right: 10px;top: 10px; font-size: 18px; border-radius: 50%; padding:0 7px 2px 7px'>x</button>
        <?php
        if (sizeOf($jsonDataProd) > 0) {
            echo "
            <h2 class='section-title mb-3 text-center' style='margin:30px 0 0 0'>Produtos</h2>
            <div class='area-search-produto col-8' style='display: flex;'>
				<span id='clean-search'>x</span>
				<input id='campo-search-produto' type='text' class='form-control col-11' placeholder='Nome'>
				<button id='button-search-produto' class='btn btn-danger col-1'>Filtrar</button>
			</div>
			<div class='section-ver-produtos ver-produtos'>
				<div class='produtos'>";
            $cont = 0;
            $fim = 50;
            while ($cont < $fim) {
                echo $Produtos[$cont]["form"];
                $cont++;
            }
            echo "<button id='ver-mais-produtos' class='btn btn-danger'>Listar mais produtos</button>					
				</div>
			</div>";
        }
        ?>
    </div>
    <div id="modal-encarte" class="modal fade" role="dialog" style="z-index: 999999;">
        <div class="modal-dialog" style="min-width: max-content; max-width: fit-content;">
            <div class="modal-content">
                <div class="modal-header" style="display: block; text-align: center;">
                    <h3 class="modal-title">Encarte gerado</h3>
                    <h4 class="modal-sub-title" style="margin-top: 10px;">Para baixar o encarte basta clicar com o botão direto sobre a imagem e depois em Salvar imagem como...</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-modal-no" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
    <?php include("../footer.php"); ?>
    <script src="../js/encarte.js"></script>
    <script src="../js/spectrum.min.js"></script>
    <script>
        var obj = <?php echo json_encode($Produtos); ?>;
        var i = 50;
        var aux = 50;
        var fim = 100;
        $('#ver-mais-produtos').on('click', function(event) {
            for (i; i < fim; i++) {
                $("#ver-mais-produtos").before(obj[i]["form"]);
            }
            i = fim;
            fim += aux;
        });

        $('#button-search-produto').on('click', function(event) {
            buscarProduto();
        });

        $("#campo-search-produto").keypress(function(event) {
            if (event.key == 'Enter') {
                buscarProduto();
            }
        });

        $('#campo-search-produto').keyup(function(i) {
            if ($(this).val() == "") {
                limparPesquisa();
            }
        });

        $('.area-search-produto #clean-search').on('click', function(event) {
            limparPesquisa();
        });

        function buscarProduto() {
            if ($('#campo-search-produto').val() != "") {
                $(".area-search-produto #clean-search").show();
                $(".ver-produtos .resultado").remove();
                $(".ver-produtos .produtos").hide();
                $(".ver-produtos").append("<div class='resultado'></div>");
                for (var c = 0; c <= filterItemsByName($('#campo-search-produto').val()).length; c++) {
                    $(".ver-produtos .resultado").append(filterItemsByName($('#campo-search-produto').val())[c].form);
                }
            }
        }

        function limparPesquisa() {
            $(".ver-produtos .resultado").remove();
            $(".ver-produtos .produtos").show();
            $('#campo-search-produto').val("");
            $(".area-search-produto #clean-search").hide();
        }

        function filterItemsByName(query) {
            return obj.filter(function(el) {
                return el.nome.toLowerCase().indexOf(query.toLowerCase()) > -1;
            })
        }
    </script>