<?php

$who = trim(htmlspecialchars($_POST['nome'], ENT_QUOTES));

include_once('../conn/id.php');

$destination_file = "/public/imagens/encartes/" . $who . "/profiles/";
$source_file = $_FILES['file'];

if (ftp_nlist($conn_id, "/public/imagens/encartes/" . $who . "/") == false || ftp_nlist($conn_id, "/public/imagens/encartes/" . $who . "/profiles/") == false) {
    ftp_mkdir($conn_id, "/public/imagens/encartes/" . $who . "/");
    ftp_mkdir($conn_id, "/public/imagens/encartes/" . $who . "/profiles/");
} else {
    $position = file_get_contents("https://www.nomercadosoft.com.br/imagens/encartes/" . $who . "/profiles/config.txt");
}

if (!isset($position))
    $position = 0;

$position = $position + 1;

if ($position == 6)
    $position = 1;

$nomeImagem = "profile" . $position . ".png";

$upload = ftp_put($conn_id, $destination_file . $nomeImagem, $source_file['tmp_name'], FTP_BINARY);

if (!$upload) {
    echo "FTP upload has failed!";
} else {
    echo "FTP upload sucess on $destination_file$nomeImagem";
}

$fp = fopen('php://temp', 'r+');
fwrite($fp, $position);
rewind($fp);
$remote_file_name = "/public/imagens/encartes/" . $who . "/profiles/config.txt";

if (ftp_fput($conn_id, $remote_file_name, $fp, FTP_ASCII)) {
    echo 'Successfully uploaded $file.';
} else {
    echo 'Error uploading $file.';
}

ftp_close($conn_id);

echo "<script>window.close();</script>";

?>