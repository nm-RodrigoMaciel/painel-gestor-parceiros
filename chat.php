<?php
include_once('rotas.php');
include_once('variables.php');

$id = trim(htmlspecialchars($_POST['id'], ENT_QUOTES));

$urlChat = $rotas['api'].'chat/listar/';

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst'],
    "key" => $id
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$resultChat = file_get_contents($urlChat, true, $context);
$jsonDataChat = json_decode($resultChat, true);

$menus = '';
$chats = '';
foreach ($jsonDataChat as $id => $values) {
    if (isset($values['name']) && isset($values['text'])) {
        if ($values['name'] == $globalData['nameUser']) {
            $chats .= "<div class='chat chat-in'><p>" . $values['text'] . "</p></div>";
        } else {
            $chats .= "<div class='chat chat-out'><p>" . $values['text'] . "</p></div>";
        }
    }
}

echo $chats;
