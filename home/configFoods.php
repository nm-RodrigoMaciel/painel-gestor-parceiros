<body>
	<div class="site-section bg-light" id="planilha-section">
		<div class="full-index">
			<div class="area-options col-9">
				<div class="col">
					<a href="../home/ver-perfil.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-address-card-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Ver Perfil</h4>
							</div>
						</div>
					</a>
				</div>
				<div class="col">
					<a href="../foods/cadastrar-produto.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Cadastrar Produto</h4>
							</div>
						</div>
					</a>
					<a href="../foods/ver-produtos.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-external-link" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Ver Produtos</h4>
							</div>
						</div>
					</a>
				</div>
				<div class="col">
					<a href="../foods/cadastrar-adicional.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Cadastrar Adicionais</h4>
							</div>
						</div>
					</a>
					<a href="../foods/ver-adicional.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-external-link" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Ver Adicionais</h4>
							</div>
						</div>
					</a>
				</div>
				<div class="col">
					<a href="../foods/ver-vendas.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-clone" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Ver Vendas</h4>
							</div>
						</div>
					</a>
					<a href="../cupom/cupom.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-credit-card" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Cupons</h4>
							</div>
						</div>
					</a>
				</div>
				<div class="col">
					<a href="../notificacoes/notificacoes.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-commenting-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Notificações Usúario</h4>
							</div>
						</div>
					</a>
					<a href="../notificacoes/notificacoes-produtos.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-comment-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Notificações Produto</h4>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>