<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('../foods/functions.php');
?>

<body>

	<?php
	if($globalData['tipoEstabelecimento'] == "Supermercados"){
		include_once('configSupermercado.php');
	}else if($globalData['tipoEstabelecimento'] == "Hamburguerias"){
		include_once('configFoods.php');
	}else{
		include_once('configSupermercado.php');
	}
	?>

	<?php
	include_once('../footer.php');
	?>