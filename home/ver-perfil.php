<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('../foods/functions.php');

if (isset($_POST['submit'])) {   
        
    $postDisponivel = $_POST['disponivel'];
    $postResp = trim(htmlspecialchars($_POST['nomeResp'], ENT_QUOTES));
    $postEmail = trim(htmlspecialchars($_POST['email'], ENT_QUOTES));
    $postTelefone = trim(htmlspecialchars($_POST['telefone'], ENT_QUOTES));
    $postEndereco = trim(htmlspecialchars($_POST['endereco'], ENT_QUOTES));
    $postPedidoMin = trim(htmlspecialchars($_POST['pedidoMin'], ENT_QUOTES));
    $postDistanciaMax = trim(htmlspecialchars($_POST['distanciaMax'], ENT_QUOTES));
    $postMinimoCompras = trim(htmlspecialchars($_POST['minimoCompras'], ENT_QUOTES));
    $postFuncionamentoFeriado = trim(htmlspecialchars($_POST['funcionamentoFeriado'], ENT_QUOTES));
    $postNaoAbre = trim(htmlspecialchars($_POST['naoAbre'], ENT_QUOTES));
    $postHorarioUteisA = trim(htmlspecialchars($_POST['horarioUteisA'], ENT_QUOTES));
    $postHorarioUteisF = trim(htmlspecialchars($_POST['horarioUteisF'], ENT_QUOTES));
    $postHorarioSabadoA = trim(htmlspecialchars($_POST['horarioSabadoA'], ENT_QUOTES));
    $postHorarioSabadoF = trim(htmlspecialchars($_POST['horarioSabadoF'], ENT_QUOTES));
    $postHorarioDomingoA = trim(htmlspecialchars($_POST['horarioDomingoA'], ENT_QUOTES));
    $postHorarioDomingoF = trim(htmlspecialchars($_POST['horarioDomingoF'], ENT_QUOTES));
    $postHorarioFeriadoA = trim(htmlspecialchars($_POST['horarioFeriadoA'], ENT_QUOTES));
    $postHorarioFeriadoF = trim(htmlspecialchars($_POST['horarioFeriadoF'], ENT_QUOTES));

    if ($postDisponivel == "on") {
        $disponivel = "sim";
    } else {
        $disponivel = "nao";
    }

    $info = array(
        "cnpj"=> $globalData['cnpjUser'],
        "disponivel"=> $disponivel,
        "email"=> $postEmail,
        "endereco"=> $postEndereco,
        "horaCompraApos"=> $postHorarioUteisA,
        "horaCompraAte"=> $postHorarioUteisF,
        "horarioAbre"=> $postHorarioUteisA,
        "horarioDomingoAbre"=> $postHorarioDomingoA,
        "horarioDomingoFecha"=> $postHorarioDomingoF,
        "horarioFecha"=> $postHorarioUteisF,
        "horarioFeriadoAbre"=> $postHorarioFeriadoA,
        "horarioFeriadoFecha"=> $postHorarioFeriadoA,
        "horarioSabadoAbre"=> $postHorarioSabadoA,
        "horarioSabadoFecha"=> $postHorarioSabadoF,
        "rangeDistancia"=> $postDistanciaMax,
        "responsavel"=> $postResp,
        "telefone"=> $postTelefone,
        "valorMinimo"=> (int)$postMinimoCompras,
        "diasFeriados"=>$postFuncionamentoFeriado,
        "diasIndisponiveis"=>$postNaoAbre,
        "tipoEstabelecimento"=> $globalData['tipoEstabelecimento'],
        "nome"=> $globalData['nameUser']
    );

    $urlAlterar = $rotas['api'].'web/estabelecimentos/alterar';
    if ($rotas['apiBase'] != "") {
        $urlBase = $rotas['apiBase'] . 'web/estabelecimentos/alterar';
    }

    $ch = curl_init($urlAlterar);

    $jsonData = [
        "corporateName"=> "ADMIN",
        "nomeEstabelecimento"=> $globalData['nameUser'],
        "token"=> "55b3b80e7124348a1e5eb8050098e23c",
        "estabelecimento" => $info
    ];

    $options = array(
        'http' => array(
            'method'  => 'POST',
            'content' => json_encode($jsonData),
            'header' =>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
        )
    );

    //echo $urlAlterar ." e ".json_encode($jsonData);

    $context  = stream_context_create($options);
    if ($rotas['apiBase'] != "") {
		$result = file_get_contents($urlBase, true, $context);
	}
    $result = file_get_contents($urlAlterar, true, $context);
}

//API Url
$urlList = $rotas['api'].'web/estabelecimentos/listar';

$jsonData = [
    "corporateName" => "ADMIN",
    "token" => "55b3b80e7124348a1e5eb8050098e23c",
    "tipoEstabelecimento" => $globalData['tipoEstabelecimento'],
    "nomeEstabelecimento" => $globalData['nameUser']
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$resultList = file_get_contents($urlList, true, $context);

$jsonDataList = json_decode($resultList, true);

?>

<body>
    <div class="site-section">
        <form method="post" class="col-12 text-center" enctype="multipart/form-data" action="ver-perfil.php">
            <div>
                <h2 class="section-title title-cad-pro mb-3 text-center" style="margin-top:25px">Meu Perfil</h2>
                <div class='flex area-switch-disponivel' style='text-align: left;margin-bottom: 15px;'>
                    <div class='flex'>
                        <h4 class='vertical-center' style='margin-right:10px;'>Disponível?</h4>
                        <label class="switch switch-est-disp">
                            <input class="slider-est-disp" type="checkbox" name='disponivel' <?php echo ($jsonDataList['items']['disponivel'] == 'sim') ? 'checked' : ''; ?>>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-12 main-form" style="display: inline-flex;">
                <div class="col-6 text-left">
                    <div>
                        <h5>Nome do responsável</h5>
                        <input type='text' name='nomeResp' class='form-control' value='<?php echo $jsonDataList['items']['responsavel']; ?>'>
                    </div>
                    <div>
                        <h5>E-mail</h5>
                        <input type='text' name='email' class='form-control' value='<?php echo $jsonDataList['items']['email']; ?>'>
                    </div>
                    <div>
                        <h5>CNPJ</h5>
                        <input type='text' name='email' class='form-control' value='<?php echo $jsonDataList['items']['cnpj']; ?>' disabled>
                    </div>
                    <div>
                        <h5>Telefone</h5>
                        <input type='text' name='telefone' class='form-control' value='<?php echo $jsonDataList['items']['telefone']; ?>'>
                    </div>
                    <div>
                        <h5>Endereço</h5>
                        <input type='text' name='endereco' class='form-control' value='<?php echo $jsonDataList['items']['endereco']; ?>'>
                    </div>
                    <div>
                        <h5>Tempo para preparar o pedido em minutos</h5>
                        <input type='text' name='pedidoMin' class='form-control' value='<?php echo $jsonDataList['items']['entrega']; ?>'>
                    </div>
                    <div>
                        <h5>Distância maxima de entrega aceita (em km)</h5>
                        <input type='text' name='distanciaMax' class='form-control' value='<?php echo $jsonDataList['items']['rangeDistancia']; ?>'>
                    </div>
                    <div>
                        <h5>Valor minimo de compra (em reais)</h5>
                        <input type='text' name='minimoCompras' class='form-control' value='<?php echo $jsonDataList['items']['valorMinimo']; ?>'>
                    </div>
                    <div>
                        <h5>Dias que funcionará em horário de feriado</h5>
                        <input type='text' name='funcionamentoFeriado' class='form-control' value='<?php echo $jsonDataList['items']['diasFeriados']; ?>'>
                    </div>
                    <div>
                        <h5>Dias que o estabelecimento NÃO abrirá</h5>
                        <input type='text' name='naoAbre' class='form-control' value='<?php echo $jsonDataList['items']['diasIndisponiveis']; ?>'>
                    </div>
                </div>
                <div class=" col-6">
                    <div>
                        <h5>Horário de funcionamento - Dias úteis</h5>
                        <div class='area-horario'>
                            <div class='col-12 flex'>
                                <div class='col-6 text-center'>
                                    <h5>Horário abre</h5>
                                    <input type="time" class='form-control col-12' name='horarioUteisA' value='<?php echo $jsonDataList['items']['horaCompraApos']; ?>'>
                                </div>
                                <div class='col-6 text-center'>
                                    <h5>Horário fecha</h5>
                                    <input type="time" class='form-control col-12' name='horarioUteisF' value='<?php echo $jsonDataList['items']['horaCompraAte']; ?>'>
                                </div>
                            </div>
                        </div>
                    </div>                    
                    <div>
                        <h5>Horário de funcionamento - Aos sábados</h5>
                        <div class='area-horario'>
                            <div class='col-12 flex'>
                                <div class='col-6 text-center'>
                                    <h5>Horário abre</h5>
                                    <input type="time" class='form-control col-12' name='horarioSabadoA' value='<?php echo $jsonDataList['items']['horarioSabadoAbre']; ?>'>
                                </div>
                                <div class='col-6 text-center'>
                                    <h5>Horário fecha</h5>
                                    <input type="time" class='form-control col-12' name='horarioSabadoF' value='<?php echo $jsonDataList['items']['horarioSabadoFecha']; ?>'>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h5>Horário de funcionamento - Dias domingos</h5>
                        <div class='area-horario'>
                            <div class='col-12 flex'>
                                <div class='col-6 text-center'>
                                    <h5>Horário abre</h5>
                                    <input type="time" class='form-control col-12' name='horarioDomingoA' value='<?php echo $jsonDataList['items']['horarioDomingoAbre']; ?>'>
                                </div>
                                <div class='col-6 text-center'>
                                    <h5>Horário fecha</h5>
                                    <input type="time" class='form-control col-12' name='horarioDomingoF' value='<?php echo $jsonDataList['items']['horarioDomingoFecha']; ?>'>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <h5>Horário de funcionamento - Aos feriados</h5>
                        <div class='area-horario'>
                            <div class='col-12 flex'>
                                <div class='col-6 text-center'>
                                    <h5>Horário abre</h5>
                                    <input type="time" class='form-control col-12' name='horarioFeriadoA' value='<?php echo $jsonDataList['items']['horarioFeriadoAbre']; ?>'>
                                </div>
                                <div class='col-6 text-center'>
                                    <h5>Horário fecha</h5>
                                    <input type="time" class='form-control col-12' name='horarioFeriadoF' value='<?php echo $jsonDataList['items']['horarioFeriadoFecha']; ?>'>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" name="submit" class="btn btn-submit btn-danger">Atualizar Perfil</button>
        </form>
    </div>
    <?php
    include_once('../footer.php');
    ?>