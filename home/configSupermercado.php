<div class="site-section bg-light" id="planilha-section">
		<div class="full-index">
			<div class="area-options col-9">
				<div class="col">
					<a href="../home/ver-perfil.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-address-card-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Ver Perfil</h4>
							</div>
						</div>
					</a>
				</div>
				<div class="col">
					<a href="../supermercados/cadastrar-produto.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Cadastrar Produto</h4>
							</div>
						</div>
					</a>
					<a href="../supermercados/cadastrar-excel.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-file-excel-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Atualizar Planilha</h4>
							</div>
						</div>
					</a>
					<a href="../supermercados/ver-produtos.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-external-link" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Ver Produtos</h4>
							</div>
						</div>
					</a>
				</div>
				<div class="col">
					<a href="../supermercados/ver-vendas.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-clone" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Ver Vendas</h4>
							</div>
						</div>
					</a>
					<a href="../encarte/encarte.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-newspaper-o " aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Encarte</h4>
							</div>
						</div>
					</a>
				</div>
				<div class="col">
					<a href="../cupom/cupom.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-credit-card" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Cupons</h4>
							</div>
						</div>
					</a>
					<a href="../notificacoes/notificacoes.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-commenting-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Notificações Usúario</h4>
							</div>
						</div>
					</a>
					<a href="../notificacoes/notificacoes-produtos.php">
						<div class="item-index">
							<div class="image">
								<i class="fa fa-comment-o" aria-hidden="true"></i>
							</div>
							<div class="text">
								<h4>Notificações Produto</h4>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>