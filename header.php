<?php
error_reporting(E_ALL ^ E_NOTICE);

include_once('variables.php');

if($globalData['cnpjUser']  == null)
{	
	header("Location: ../verifyData.php");
}

if ($globalData['tipoEstabelecimento'] == "Supermercados") {
	$menu = "
	<div class='col dropdown'>
		<button class='dropbtn'>Produtos</button>
		<div class='dropdown-content'>
			<div class='col'>
				<a href='../supermercados/cadastrar-produto.php'>Cadastrar individual</a>
				<a href='../supermercados/cadastrar-excel.php'>Atualizar planilha</a>
				<a href='../supermercados/ver-produtos.php'>Visualizar</a>
			</div>
		</div>
	</div>
	<div class='col dropdown'>
		<a href='../supermercados/ver-vendas.php'>Vendas</a>
	</div>
	<div class='col dropdown'>
		<a href='../encarte/encarte.php'>Encarte</a>
	</div>
	<div class='col dropdown'>
	<a href='../cupom/cupom.php'>Cupons</a>
</div>";
} else {
	$menu = "
	<div class='col dropdown'>
			<a href='../foods/etiquetas-qr.php'>Etiquetas QR</a>
		</div>
		<div class='col dropdown'>
		<button class='dropbtn'>Produtos</button>
			<div class='dropdown-content'>
				<div class='col'>
					<a href='../foods/cadastrar-produto.php'>Cadastrar</a>
					<a href='../foods/ver-produtos.php'>Visualizar</a>
				</div>
			</div>
		</div>
		<div class='col dropdown'>
		<button class='dropbtn'>Adicionais</button>
			<div class='dropdown-content'>
				<div class='col'>
					<a href='../foods/cadastrar-adicional.php'>Cadastrar</a>
					<a href='../foods/ver-adicional.php'>Visualizar</a>
				</div>
			</div>
		</div>
		<div class='col dropdown'>
			<a href='../foods/ver-vendas.php'>Vendas</a>
		</div>";
}
?>
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo $globalData['nameUser'] ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
	if (isset($_SESSION['nomecod'])) {
		echo "<link rel='shortcut icon' href='/" . $_SESSION['nomecod'] . "/images/favicon.png'>";
	} else {
		echo "<link rel='shortcut icon' href='http://nomercadosoft.com.br/imagens/Originais/ldpi.png'>";
	}
	?>
	<link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../fonts/iconic/css/material-design-iconic-font.min.css">
	<link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
	<link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css">
	<link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
	<link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
	<link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css">
	<link rel="stylesheet" type="text/css" href="../css/util.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="../css/switch.css">
	<link rel="stylesheet" type="text/css" href="../css/spectrum.min.css">
	<script type="text/javascript" src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
	<link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
</head>
<header role="banner">
	<div class='container'>
		<div class="row align-items-center">
			<div class="col-12 main-menu" style="display:flex; text-align:center;">
				<di class="col" style="display:flex">
					<div class='aguardando-vendas' style='top: 0; left: 0; position: fixed; padding: 10px; margin:5px'>
						<label class="switch switch-ver-vendas-disp">
							<input class="slider-ver-vendas-disp" type="checkbox" name='disponivel' checked<?php //echo ($jsonDataList['items']['disponivel'] == 'sim') ? 'checked' : ''; 
																											?>>
							<span class="slider round icon-ver-vendas-disp"></span>
						</label>
						</br>
						<div style="display:inline-flex">
						<label style='font-size:12px;'>Imprimir vendas</label>
						<div class='blink' style='width: 15px; height: 15px; background: #ff0018; border-radius: 50%; margin: 4px 4px 4px 8px;'></div>
						</div>
					</div>
					<img id="img-profile" src="<?php echo $globalData['url']; ?>">
					<div class="col dropdown">
						<div class="area-perfil-header" style="display: inline-grid;">
							<button class="dropbtn"><?php echo $globalData['nameUser']; ?></button>
							<input class="url-imagem" value='<?php echo $globalData['url']; ?>' style='display:none'></input>
							<span><?php echo $globalData['cnpjUser']; ?></span>
						</div>
						<div class="dropdown-content">
							<a href="../home/index.php">Inicio</a>
							<a href="../home/ver-perfil.php">Ver perfil</a>
						</div>
					</div>
					<?php echo $menu; ?>
					<!--<div class="col dropdown">
						<div class="area-perfil-header" style="display: inline-grid;">
							<a href='../notificacoes/notificacoes.php'>Notificações</a>
						</div>
						<div class="dropdown-content">
							<a href="../notificacoes/notificacoes.php">Usuários</a>
							<a href="../notificacoes/notificacoes-produtos.php">Produtos</a>
						</div>
					</div>-->
					<div class="col-2" style="display:flex">
						<li class="col-2"><a href="../sair.php" class="col-12">Sair</a></li>
					</div>
			</div>
		</div>
	</div>
	</div>
</header>