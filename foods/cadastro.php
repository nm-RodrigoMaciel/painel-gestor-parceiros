<?php
include_once('../rotas.php');
include_once('functions.php');

$url = $rotas['api'].'web/produtos/cadastrar';
/*if ($rotas['apiBase'] != "") {
    $urlBase = $rotas['apiBase'] . 'web/produtos/cadastrar';
}*/

$ch = curl_init($url);

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst'],
    "itens" => $produtos
];

echo json_encode($jsonData);

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
/*if ($rotas['apiBase'] != "") {
    $result = file_get_contents($urlBase, true, $context);
}*/
$result = file_get_contents($url, true, $context);

$jsonData = json_decode($result, true);

include_once('../conn/id.php');

$destination_file = "/public/imagens/produtos/";
$source_file = $_FILES['file'];

/*switch (exif_imagetype($source_file['tmp_name'])) {
    case 2:
        $type = ".jpg";
        break;
    case 3:
        $type = ".png";
        break;
}*/

$cnpjSplit = substr($globalData['cnpjUser'], -3);
$nomeImagem = $globalData['nameUser'] . "-" . $cnpjSplit . "-" . $jsonData[0]['codigoBarras'] . ".png";
$nomeImagem = str_replace(" ","",$nomeImagem);

$upload = ftp_put($conn_id, $destination_file . $nomeImagem, $source_file['tmp_name'], FTP_BINARY);

if (!$upload) {
    //echo "FTP upload has failed!";
} else {
    //echo "FTP upload sucess on $destination_file$nomeImagem";
}

ftp_close($conn_id);
?>