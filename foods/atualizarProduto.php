<?php
include_once('../rotas.php');
include_once('../variables.php');

$url = $rotas['api'] . 'web/produtos/alterarproduto';
if ($rotas['apiBase'] != "") {
    $urlBase = $rotas['apiBase'] . 'web/produtos/alterarproduto';
}

$itens = json_decode($_POST["itens"]);

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst'],
    "itens" => $itens
];

echo json_encode($jsonData);

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
if ($rotas['apiBase'] != "") {
    file_get_contents($urlBase, true, $context);
}
file_get_contents($url, true, $context);

