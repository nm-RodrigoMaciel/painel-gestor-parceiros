<?php

include_once('../header.php');
include_once('../rotas.php');
include_once('functions.php');

if (isset($_POST['submit'])) {
	$postNomeAdicional = trim(htmlspecialchars($_POST['nomeAdicional'], ENT_QUOTES));
	$postValor = trim(htmlspecialchars($_POST['valorAdicional'], ENT_QUOTES));
	$postEstoque = trim(htmlspecialchars($_POST['estoque'], ENT_QUOTES));
	$postCategoria = trim(htmlspecialchars($_POST['categorias'], ENT_QUOTES));
	$postNovaLista = trim(htmlspecialchars($_POST['valorNovaLista'], ENT_QUOTES));
	$postLista = trim(htmlspecialchars($_POST['listas'], ENT_QUOTES));
	$postDisponivel = trim(htmlspecialchars($_POST['disponivel'], ENT_QUOTES));

	if ($postDisponivel == "on") {
		$disponivel = true;
	} else {
		$disponivel = false;
	}

	$itens[] = array(
		"disponivel" => $disponivel,
		"categoria" => $postCategoria,
		"key" => $postLista,
		"nomeProduto" => $postNomeAdicional,
		"quantidadeEscolha" => "0",
		"valorInicial" => (string)$postValor,
		"estoque" => (float)$postEstoque
	);

	$url = $rotas['api'] . 'web/listas/cadastrar';
	if ($rotas['apiBase'] != "") {
		$urlBase = $rotas['apiBase'] . 'web/listas/cadastrar';
	}

	$ch = curl_init($url);

	$jsonData = [
		"nomeEstabelecimento" => $globalData['nameUser'],
		"codigoEstabelecimento" => $globalData['codEst'],
		"itens" => $itens
	];

	//echo json_encode($jsonData);

	$options = array(
		'http' => array(
			'method'  => 'POST',
			'content' => json_encode($jsonData),
			'header' =>  "Content-Type: application/json\r\n" .
				"Accept: application/json\r\n"
		)
	);

	$context  = stream_context_create($options);
	if ($rotas['apiBase'] != "") {
		$result = file_get_contents($urlBase, true, $context);
	}
	$result = file_get_contents($url, true, $context);

	$jsonData = json_decode($result, true);
}

//API Url
$urlProd = $rotas['api'] . 'web/listas';

$jsonData = [
	"nomeEstabelecimento" => $globalData['nameUser'],
	"codigoEstabelecimento" => $globalData['codEst'],
];

$options = array(
	'http' => array(
		'method'  => 'POST',
		'content' => json_encode($jsonData),
		'header' =>  "Content-Type: application/json\r\n" .
			"Accept: application/json\r\n"
	)
);

$context  = stream_context_create($options);
$resultList = file_get_contents($urlProd, true, $context);

$jsonDataList = json_decode($resultList, true);

//echo json_encode($jsonDataList);

foreach ($jsonDataList as $key => $value) {
	$arrayListasNomes[] = $key;
	foreach ($value as $key => $value) {
		$arrayListasCategorias[] = $value['categoria'];
	}
}

if (isset($arrayListasNomes)) {
	for ($i = 0; $i < sizeof($arrayListasNomes); $i++) {
		if (substr($arrayListasNomes[$i], -5) != 'PRDFL') {
			$valListaNomes .=
				"<tr>
		<label class='area-radio'>
			<p style='margin-left:15px; margin-top:6px;'>" . $arrayListasNomes[$i] . "</p>
			<input type='radio' class='radio-listas-adicional' value='" . $arrayListasNomes[$i] . "'>
			<span style='margin-left:15px;' class='checkmark'></span>
		</label>
		</tr>";
		}
	}
}

if (isset($arrayListasCategorias)) {
	$terminou = false;
	$cont = 0;

	if (!isset($arrayCatTmp)) {
		$arrayCatTmp[] = '';
	}

	while (!$terminou) {
		if (!in_array($arrayListasCategorias[$cont], $arrayCatTmp)) {
			$valCategorias .=
				"<tr>
			<label class='area-radio'>
				<p style='margin-left:15px; margin-top:6px;'>" . $arrayListasCategorias[$cont] . "</p>
				<input type='radio' class='radio-categorias' value='" . $arrayListasCategorias[$cont] . "'>
				<span style='margin-left:15px;' class='checkmark'></span>
			</label>
			</tr>";
			$arrayCatTmp[] = $arrayListasCategorias[$cont];
		}
		$cont++;
		if (!isset($arrayListasCategorias[$cont])) {
			$terminou = true;
		}
	}
}
?>

<body>
	<div class="site-section">
		<div class="container">
			<h2 class="section-title title-cad-pro mb-3 text-center" style="margin-top:25px">Cadastrar Adicional</h2>
			<form method="post" class="text-center" enctype="multipart/form-data" action="cadastrar-adicional.php">
				<div class="col-12" style="display: inline-flex;">
					<div class="main-section col-12 text-center">
						<div class="col-12">
							<div class="dropdown show col-12" style="padding-right: 0px; padding-left: 0px; margin-bottom:10px;">
								<a class="btn dropdown-toggle col-12" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="text-listas-adicional">Selecionar lista de adicionais</span>
									<input class="value-listas-adicional" name='listas' style="display: none;"></input>
								</a>
								<div id="dropdown" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<table class="table-dropdown-listas">
										<tbody>
											<tr>
												<input type="text" name="valorNovaLista" class="form-control col-12 campo-adicionar-listas" placeholder="Nova lista">
												<?php echo $valListaNomes; ?>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<input type="text" name="nomeAdicional" class="form-control" placeholder="Nome do adicional" required>
							<input type="text" name="valorAdicional" class="form-control campo-valor" placeholder="Valor do adicional" required>
							<input type="text" name="estoque" class="form-control" placeholder="Estoque" required>
							<div class="dropdown show col-12" style="padding-right: 0px; padding-left: 0px; margin-bottom:10px;">
								<a class="btn dropdown-toggle col-12" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="text-categoria">Selecionar categoria</span>
									<input class="value-categoria" name='categorias' style="display: none;"></input>
								</a>
								<div id="dropdown" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<table class="table-dropdown-categorias">
										<tbody>
											<tr>
												<input type="text" name="valorNovaCategoria" class="form-control campo-categoria col" placeholder="Nova categoria">
												<?php echo $valCategorias; ?>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<div style="text-align: left; display: flex;">
								<p style="margin-right:10px;">Disponível</p>
								<input id='check-list' type='checkbox' class='check-disp' name='disponivel' class='input-checkbox' checked>
							</div>
						</div>
					</div>
				</div>
				<button type="submit" name="submit" class="btn btn-submit btn-danger">Cadastrar Adicional</button>
			</form>
		</div>
	</div>
	<?php
	include_once('../footer.php');
	?>

	<script>
		$(".campo-adicionar-listas").keyup(function() {
			if ($(this).val() != '') {
				$('.text-listas-adicional').text($(this).val());
				$('.value-listas-adicional').val($(this).val());
			} else {
				$('.text-listas-adicional').text('Selecionar lista de adicionais');
				$('.value-listas-adicional').val('');
			}
		});
	</script>