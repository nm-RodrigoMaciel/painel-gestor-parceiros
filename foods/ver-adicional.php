<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('functions.php');

if (isset($_POST['submit'])) {
	$postNomeProduto = trim(htmlspecialchars($_POST['nomeAdicional'], ENT_QUOTES));
	$postValor = trim(htmlspecialchars($_POST['valorAdicional'], ENT_QUOTES));
	$postQuantidade = trim(htmlspecialchars($_POST['quantidadeAdicional'], ENT_QUOTES));
	$postCategoria = trim(htmlspecialchars($_POST['categorias'], ENT_QUOTES));
	$postTmpCategoria = trim(htmlspecialchars($_POST['tmp-categoria'], ENT_QUOTES));
	$postTmpCodBarras = trim(htmlspecialchars($_POST['tmp-cod-barras'], ENT_QUOTES));
	$postTmpKeyPush = trim(htmlspecialchars($_POST['tmp-key-push'], ENT_QUOTES));

	if ($postCategoria == '') {
		$postCategoria = $postTmpCategoria;
	}

	$produtos[] = array(
		"nomeProduto" => $postNomeProduto,
		"valorInicial" => $postValor,
		"keyPush" => $postTmpKeyPush,
		"estoque" => (int)$postQuantidade,
		"codigoBarras" => $postTmpCodBarras,
		"categoria" => $postCategoria
	);

	$url = $rotas['api'] . 'web/listas/alteraradicional';
	if ($rotas['apiBase'] != "") {
		$urlBase = $rotas['apiBase'] . 'web/listas/alteraradicional';
	}

	$ch = curl_init($url);

	$jsonData = [
		"nomeEstabelecimento" => $globalData['nameUser'],
		"codigoEstabelecimento" => $globalData['codEst'],
		"itens" => $produtos
	];

	//echo json_encode($jsonData);

	$options = array(
		'http' => array(
			'method'  => 'POST',
			'content' => json_encode($jsonData),
			'header' =>  "Content-Type: application/json\r\n" .
				"Accept: application/json\r\n"
		)
	);

	$context  = stream_context_create($options);
	if ($rotas['apiBase'] != "") {
		$result = file_get_contents($urlBase, true, $context);
	}
	$result = file_get_contents($url, true, $context);
}

//API Url
$urlProd = $rotas['api'] . 'web/listas';

$jsonData = [
	"nomeEstabelecimento" => $globalData['nameUser'],
		"codigoEstabelecimento" => $globalData['codEst']
];

$options = array(
	'http' => array(
		'method'  => 'POST',
		'content' => json_encode($jsonData),
		'header' =>  "Content-Type: application/json\r\n" .
			"Accept: application/json\r\n"
	)
);

$context  = stream_context_create($options);
$resultList = file_get_contents($urlProd, true, $context);

$jsonDataList = json_decode($resultList, true);

//echo json_encode($jsonDataList);

$conta = 0;
$cont_drop = 0;
$cont_ad = 0;

foreach ($jsonDataList as $key => $lista) {
	if (substr($key, -5) == 'PRDFL') {
		$type = 'p';
	} else {
		$type = 'i';
	}

	$cont_ad = 0;
	$arrayListasNomes[$type][] .=
		"<div class='dropdown show col-12' style='padding-right: 0px; padding-left: 0px; margin-bottom:10px;'>
		<a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
			<span id='text-listas-adicional'>" . $key . "</span>
		</a>
		<div id='dropdown' class='dropdown-menu' aria-labelledby='dropdownMenuLink'>";
	foreach ($lista as $key => $value) {
		$arrayListasNomes[$type][] .=
			"<div class='col-12 adicional-lista' ref='$cont_drop-$cont_ad' style='margin-bottom:10px;'>
				<a class='btn dropdown-toggle col-12' href='#' role='button'>
					<input class='nome-$cont_drop-$cont_ad' value='" . $value['nomeProduto'] . "' style='display:none'></input>
					<input class='valor-$cont_drop-$cont_ad' value='" . $value['valorInicial'] . "' style='display:none'></input>
					<input class='quantidade-$cont_drop-$cont_ad' value='" . $value['estoque'] . "' style='display:none'></input>
					<input class='categoria-$cont_drop-$cont_ad' value='" . $value['categoria'] . "' style='display:none'></input>
					<input class='keypush-$cont_drop-$cont_ad' value='" . $value['keyPush'] . "' style='display:none'></input>
					<input class='codbarras-$cont_drop-$cont_ad' value='" . $value['codigoBarras'] . "' style='display:none'></input>
					<span id='text-listas-adicional'>" . $value['nomeProduto'] . "</span>
				</a>
			</div>";

		if (!isset($arrayCatTmp)) {
			$arrayCatTmp[] = '';
		}

		if (!in_array($value['categoria'], $arrayCatTmp)) {
			$valCategorias .=
				"<tr>
					<label class='area-radio'>
						<p style='margin-left:15px; margin-top:6px;'>" . $value['categoria'] . "</p>
						<input type='radio' class='radio-categorias' name='categorias' value='" . $value['categoria'] . "'>
						<span style='margin-left:15px;' class='checkmark'></span>
					</label>
					</tr>";
			$arrayCatTmp[] = $value['categoria'];
		}

		$cont_ad += 1;
	}
	$arrayListasNomes[$type][] .=
		"</div>
	</div>";
	$cont_drop += 1;
}

if ($arrayListasNomes['i'] != null) {
	for ($i = 0; $i <= sizeof($arrayListasNomes['i']); $i++) {
		$ingredientes .= $arrayListasNomes['i'][$i];
	}
}
else{
	$ingredientes = "<h3>Nenhuma lista de ingredientes cadastrada.</h3>";
}

if ($arrayListasNomes['p'] != null) {
	for ($i = 0; $i <= sizeof($arrayListasNomes['p']); $i++) {
		$produtos .= $arrayListasNomes['p'][$i];
	}
}
else{
	$produtos = "<h3>Nenhuma lista de produtos cadastrada.</h3>";
}

?>

<body>
	<div class="site-section">
		<div class="container">
			<div class="col-12 text-center">
				<h2 class="section-title mb-3">Adicionais Cadastrados</h2>
				<div class="col" style='display:flex; margin-top:10px;'>
					<div class="col-6">
						<h3 style="margin-bottom:20px">Listas Ingredientes</h3>
						<?php echo $ingredientes; ?>
					</div>
					<div class="col-6">
						<h3 style="margin-bottom:20px">Listas Produtos</h3>
						<?php echo $produtos; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modal-adicional" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<form method="post" class="text-center" enctype="multipart/form-data" action="ver-adicional.php">
					<div class="modal-header">
						<h4 class="modal-title">Adicional</h4>
					</div>
					<div class="modal-body" style="text-align: center;">
						<div style="display: flex;">
							<h4 class="col-3">Nome</h4>
							<input type="text" name="nomeAdicional" class="form-control nomeAdicional col-9" placeholder="Nome do adicional">
						</div>
						<div style="display: flex;">
							<h4 class="col-3">Valor</h4>
							<input type="text" name="valorAdicional" class="form-control valorAdicional campo-valor col-9" placeholder="Valor do adicional">
						</div>
						<div style="display: flex;">
							<h4 class="col-3">Estoque</h4>
							<input type="text" name="quantidadeAdicional" class="form-control quantidadeAdicional col-9" placeholder="Estoque">
						</div>
						<input class='tmp-categoria-adicional' name="tmp-categoria" value="" style="display: none;"></input>
						<input class='tmp-codbarras-adicional' name="tmp-cod-barras" value="" style="display: none;"></input>
						<input class='tmp-keypush-adicional' name="tmp-key-push" value="" style="display: none;"></input>
						<div style="display: flex;">
							<h4 class="col-3">Categoria</h4>
							<div class="dropdown show col-9" style="padding-right: 0px; padding-left: 0px; margin-bottom:10px;">
								<a class="btn dropdown-toggle col-12" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span id="text-categoria">Selecionar categoria</span>
								</a>
								<div id="dropdown" class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="width: 100%;">
									<table class="table-dropdown-categorias">
										<tbody>
											<tr>
												<?php echo $valCategorias; ?>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-modal-no" data-dismiss="modal">Cancelar</button>
						<button type="submit" name="submit" class="btn btn-modal-yes">Atualizar</button>
					</div>
				</form>
			</div>

		</div>
	</div>
	<?php

	include_once('../footer.php');
	?>