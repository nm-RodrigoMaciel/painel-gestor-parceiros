<?php

function notNull($check)
{
	if (isset($check))
		return $check;
	else
		return "";
}

function getArrayListasIngredientes($jsonDataList)
{
	$contProduto = 0;

	foreach ($jsonDataList as $key => $value) {
		$nameAdi = $key;
		if (substr($nameAdi, -5) != 'PRDFL') {
			foreach ($value as $key => $value) {
				$arrayListasProduto[$contProduto]['nomeProduto'] = $value['nomeProduto'];
				$arrayListasProduto[$contProduto]['codProdutoListaPertence'] = $key . ";" . $value['key'];
				$arrayListasProduto[$contProduto]['categoriaProduto'] = $value['categoria'];
				$arrayListasProduto[$contProduto]['valorProduto'] = $value['valorInicial'];
				$arrayListasProduto[$contProduto]['listaPertence'] = $value['key'];
				$arrayListasProduto[$contProduto]['keyPush'] = $value['keyPush'];
				$contProduto++;
			}
		}
	}

	return $arrayListasProduto;
}

function getIngredientes($jsonDataList)
{
	$cont = 0;

	$arrayListasProduto = getArrayListasIngredientes($jsonDataList);

	$valComboIngredientes = '';

	foreach ($arrayListasProduto as $key => $value) {
		$valComboIngredientes .= "<tr>
        <th scope='row'><input id='check-list' class='check-in' type='checkbox' pos='$cont' name='ingredientes[" . $cont . "][p]' value='" . $value['codProdutoListaPertence'] . "' class='input-checkbox'></th>
        <td><span class='text-in-$cont'>" . $value['nomeProduto'] . "</span></td>
        <td><span>" . $value['listaPertence'] . "</span></td>
        <td><input type='number' min='1' class='number-in' pos='$cont' name='ingredientes[" . $cont . "][q]'style='max-width: 2em;' placeholder='1' value='1' disabled></td>
        </tr>";
		$cont++;
	}

	return $valComboIngredientes;
}

function getIngredientesPreenchidos($jsonDataList, $produtoIngredientes, $produtoListaIngredientes)
{
	$cont = 0;

	$arrayListasProduto = getArrayListasIngredientes($jsonDataList);

	$contC = 0;
	if (isset($produtoIngredientes)) {
		$listaIngredientesCompleta = array();
		for ($cont = 0; $cont < sizeof(explode(";", $produtoIngredientes)); $cont += 2) {
			$listaIngredientesCompleta[$contC]['cod'] = explode(";", $produtoIngredientes)[$cont];
			$listaIngredientesCompleta[$contC]['quantidade'] = explode(";", $produtoIngredientes)[$cont + 1];
			$listaIngredientesCompleta[$contC]['lista'] = explode(";", $produtoListaIngredientes)[$cont / 2];
			$contC++;
		}
	}

	$valComboIngredientes = "<thead>
	<tr>
		<th scope='col'>Selecionar</th>
		<th scope='col'>Nome</th>
		<th scope='col'>Lista</th>
		<th scope='col'>Quantidade</th>
		</tr>
	</thead>
	<tbody>";

	foreach ($arrayListasProduto as $key => $value) {

		$see['pertence'] = "";
		$see['quantidade'] = 1;
		$see['campo'] = "disabled";
		if (isset($produtoIngredientes)) {
			for ($contt = 0; $contt < sizeof($listaIngredientesCompleta); $contt++) {
				if (
					$value['listaPertence'] == $listaIngredientesCompleta[$contt]['lista'] &&
					explode(";", $value['codProdutoListaPertence'])[0] == $listaIngredientesCompleta[$contt]['cod']
				) {
					$see['pertence'] = "checked";
					$see['quantidade'] = $listaIngredientesCompleta[$contt]['quantidade'];
					$see['campo'] = "";
				}
			}
		}

		$valComboIngredientes .= "<tr>
        <th scope='row'><input id='check-list' class='check-in ci' type='checkbox' pos='$cont' name='ingredientes[" . $cont . "][p]' value='" . $value['codProdutoListaPertence'] . "' class='input-checkbox' " . $see['pertence'] . "></th>
        <td><span class='text-in-$cont'>" . $value['nomeProduto'] . "</span></td>
        <td><span>" . $value['listaPertence'] . "</span></td>
        <td><input type='number' min='1' class='number-in' pos='$cont' name='ingredientes[" . $cont . "][q]'style='max-width: 2em;' placeholder='1' value='" . $see['quantidade'] . "' " . $see['campo'] . "></td>
        </tr>";

		$cont++;
	}

	return $valComboIngredientes;
}

function getProdutos($jsonDataProd)
{

	$valComboProdutos = null;
	$contProd = 0;

	foreach ($jsonDataProd as $key => $value) {
		$valComboProdutos .=
			"<tr>
                <th scope='row'><input id='check-list-prod' class='check-in' type='checkbox' pos='$contProd' name='produtos[" . $contProd . "][p]' value='" . $value['codigoBarras'] . ";" . $value['nomeProduto'] . "' class='input-checkbox'></th>
                <td><span class='text-in-$contProd'>" . $value['nomeProduto'] . "</span></td>
                <td style='text-align:center;'><input type='number' min='1' class='number-val number-val-$contProd' pos='$contProd' name='produtos[" . $contProd . "][v]'style='max-width: 5em;' placeholder='1' value='1' disabled></td>		
                <td style='text-align:center;'><input type='number' min='1' class='number-in' pos='$contProd' name='produtos[" . $contProd . "][q]'style='max-width: 2em;' placeholder='1' value='1' disabled></td>
            </tr>";

		$contProd++;
	}

	return  $valComboProdutos;
}

function getProdutosPreenchido($jsonDataProd, $produtosCombo)
{
	$valComboProdutos = "<thead>
	<tr>
		<th scope='col'>Nome</th>
		<th scope='col'>Codigo Barras</th>
		<th scope='col'>Quantidade</th>
		<th scope='col'>Valor Desconto</th>
	</tr>
	</thead>
	<tbody>";

	$contProd = 0;

	foreach ($jsonDataProd as $key => $value) {

		$see['pertence'] = "";
		$see['valorDesconto'] = 1;
		$see['quantidade'] = 1;
		$see['campo'] = "disabled";
		if (isset($produtosCombo)) {
			for ($contt = 0; $contt < sizeof($produtosCombo); $contt++) {
				if ($value['codigoBarras'] == $produtosCombo[$contt]['codigoBarras']) {
					$see['pertence'] = "checked";
					$see['valorDesconto'] = $produtosCombo[$contt]['valorDesconto'];
					$see['quantidade'] = $produtosCombo[$contt]['quantidade'];
					$see['campo'] = "";
				}
			}
		}

		$valComboProdutos .=
			"<tr>
                <th scope='row'><input id='check-list-prod' class='check-in cp' type='checkbox' pos='$contProd' name='produtos[" . $contProd . "][p]' value='" . $value['codigoBarras'] . ";" . $value['nomeProduto'] . "' class='input-checkbox' " . $see['pertence'] . "></th>
                <td><span class='text-in-$contProd'>" . $value['nomeProduto'] . "</span></td>
                <td style='text-align:center;'><input type='number' min='1' class='number-val number-val-$contProd' pos='$contProd' name='produtos[" . $contProd . "][v]'style='max-width: 5em;' placeholder='1' value='" . $see['quantidade'] . "' " . $see['campo'] . "></td>		
                <td style='text-align:center;'><input type='number' min='1' class='number-in' pos='$contProd' name='produtos[" . $contProd . "][q]'style='max-width: 2em;' placeholder='1' value='" . $see['valorDesconto'] . "' " . $see['campo'] . "></td>
            </tr>";

		$contProd++;
	}

	return  $valComboProdutos;
}

function getAdicionais($jsonDataList)
{
	$arrayListasProduto = getArrayListasIngredientes($jsonDataList);

	$catArray = array();
	$valComboAdicionais = null;

	for ($cont = 0; $cont < sizeof($arrayListasProduto); $cont++) {

		$categoria = $arrayListasProduto[$cont]['categoriaProduto'];

		if (!isset($catArray[$categoria])) {
			$catArray[$categoria] .=
				"<tr class='titulo-categoria'>
			<th scope='row'><span>" . $categoria . "</span></th>
			<td style='text-align: center;'><input class='value-ad-title' name='adicionais[" . $cont . "][c]' type='checkbox' class='form-check-input' value='" . $categoria . "' ref='" . str_replace(' ', '',$categoria) . "' disabled checked></td>
			<td></td>
            <td></td>
			<td style='text-align: center;'><input type='number' min='0' class='number-ad-title' ref='" . str_replace(' ', '',$categoria) . "' name='adicionais[" . $cont . "][t]'style='max-width: 2em;' placeholder='0' value='0' disabled></td>
			<td class='linha-obrigatoria' style='margin: 0 auto; text-align: center;'><input class='obrig-ad-title " . $categoria . "' name='adicionais[" . $cont . "][o]' type='checkbox' class='form-check-input' value='".$categoria."' ref='" . str_replace(' ', '',$categoria) . "' disabled></td>
			</tr>";
		}

		$catArray[$categoria] .=
			"<tr>
			<th scope='row'><input id='check-list' type='checkbox' class='check-ad' name='adicionais[" . $cont . "][p]' value='" . $arrayListasProduto[$cont]['codProdutoListaPertence'] . "' class='input-checkbox' pos='$cont' ref='" . str_replace(' ', '',$categoria) . "'></th>
			<td><span class='text-ad-$cont'>" . $arrayListasProduto[$cont]['nomeProduto'] . "</span></td>
			<td><span>" . $arrayListasProduto[$cont]['valorProduto'] . "</span></td>
            <td><span>" . $arrayListasProduto[$cont]['listaPertence'] . "</span></td>
			<td style='text-align: center;'><input type='number' min='0' class='number-ad' ref='" . str_replace(' ', '',$categoria) . "' pos='$cont' name='adicionais[" . $cont . "][q]'style='max-width: 2em;' placeholder='1' value='0' disabled></td>
			</tr>";
	}

	if (isset($catArray)) {
		foreach ($catArray as $values) {
			$valComboAdicionais .= $values;
		}

		return $valComboAdicionais;
	}
}

function getAdicionaisPreenchidos($jsonDataList, $categoriasProduto, $itensAdicionaisProduto)
{
	$arrayListasProduto = getArrayListasIngredientes($jsonDataList);

	$catArray = array();
	$valComboAdicionais = null;

	for ($cont = 0; $cont < sizeof($arrayListasProduto); $cont++) {

		$categoria = $arrayListasProduto[$cont]['categoriaProduto'];

		if (!isset($catArray[$categoria])) {

			$see['quantidade'] = 0;
			$see['obrigatorio'] = "";
			$see['campo'] = "disabled";
			$see['enabled'] = "disabled";
			if (isset($categoriasProduto)) {
				foreach ($categoriasProduto as $key => $value) {
					if ($categoria == $value['id']) {

						$see['quantidade'] = $value['quantidade'];
						$see['campo'] = "";
						$see['enabled'] = "";

						if ($value['obrigatorio']) {
							$see['obrigatorio'] = "checked";
							$see['campo'] = "disabled";
						}
					}
				}
			}

			$catArray[$categoria] .=
				"<tr class='titulo-categoria'>
			<th scope='row'><span>" . $categoria . "</span></th>
			<td style='text-align: center;'><input class='value-ad-title' name='adicionais[" . $cont . "][c]' type='checkbox' class='form-check-input' value='" . $categoria . "' ref='" . str_replace(' ', '',$categoria) . "' ".$see['enabled']." checked></td>
			<td></td>
            <td></td>
			<td style='text-align: center;'><input type='number' min='0' class='number-ad-title' ref='" . str_replace(' ', '',$categoria) . "' name='adicionais[" . $cont . "][t]'style='max-width: 2em;' placeholder='0' value='" . $see['quantidade'] . "' " . $see['campo'] . "></td>
			<td class='linha-obrigatoria' style='margin: 0 auto; text-align: center;'><input class='obrig-ad-title " . $categoria . "' name='adicionais[" . $cont . "][o]' type='checkbox' class='form-check-input' value='".$categoria."' ref='" . str_replace(' ', '',$categoria) . "' " . $see['obrigatorio'] . "></td>
			</tr>";
		}

		$seeA['pertence'] = "";
		$seeA['quantidade'] = 0;
		$seeA['campo'] = "disabled";
		if (isset($itensAdicionaisProduto)) {
			foreach ($itensAdicionaisProduto as $key => $value) {
				if ($arrayListasProduto[$cont]['keyPush'] == $value['keyPush']) {
					$seeA['pertence'] = "checked";
					$seeA['quantidade'] = $value['quantidadeEscolha'];
					$seeA['campo'] = "";
				}
			}
		}

		$catArray[$categoria] .=
			"<tr>
			<th scope='row'><input id='check-list' type='checkbox' class='check-ad' name='adicionais[" . $cont . "][p]' value='" . $arrayListasProduto[$cont]['codProdutoListaPertence'] . "' class='input-checkbox' pos='$cont' ref='" . str_replace(' ', '',$categoria) . "' " . $seeA['pertence'] . "></th>
			<td><span class='text-ad-$cont'>" . $arrayListasProduto[$cont]['nomeProduto'] . "</span></td>
			<td><span>" . $arrayListasProduto[$cont]['valorProduto'] . "</span></td>
            <td><span>" . $arrayListasProduto[$cont]['listaPertence'] . "</span></td>
			<td style='text-align: center;'><input type='number' min='0' class='number-ad' ref='" . str_replace(' ', '',$categoria) . "' pos='$cont' name='adicionais[" . $cont . "][q]'style='max-width: 2em;' placeholder='1' value='" . $seeA['quantidade'] . "' " . $seeA['campo'] . "></td>
			</tr>";
	}

	if (isset($catArray)) {
		foreach ($catArray as $values) {
			$valComboAdicionais .= $values;
		}

		return $valComboAdicionais;
	}
}

function alterarCombo($POST, $globalData, $rotas)
{
	$source_file = $_FILES['file'];

	$postNomeProduto = trim(htmlspecialchars($_POST['nomeProduto'], ENT_QUOTES));
	$postFraseVendaProduto = trim(htmlspecialchars($_POST['fraseVendaProduto'], ENT_QUOTES));
	$postServePessoasProduto = trim(htmlspecialchars($_POST['servePessoasProduto'], ENT_QUOTES));
	$postValorProduto = trim(htmlspecialchars($_POST['valorProduto'], ENT_QUOTES));
	$postQuantidadeProduto = trim(htmlspecialchars($_POST['quantidadeProduto'], ENT_QUOTES));
	$postPromocao = trim(htmlspecialchars($_POST['promocao'], ENT_QUOTES));
	$postProdutos = $_POST['produtos'];
	$postAdicionais = $_POST['adicionais'];
	$postDisponivel = trim(htmlspecialchars($_POST['disponivel'], ENT_QUOTES));
	$postCategoria = trim(htmlspecialchars($_POST['categoria'], ENT_QUOTES));
	$postDescricao = trim(htmlspecialchars($_POST['descricao'], ENT_QUOTES));
	$postCodigoBarras = trim(htmlspecialchars($_POST['codigoBarras'], ENT_QUOTES));

	if ($postPromocao == null || $postPromocao == "") {
		$postPromocao = "0";
	}

	if (isset($postAdicionais)) {
		$custom = true;
	} else {
		$custom = false;
	}

	if ($postDisponivel == "on") {
		$disponivel = true;
	} else {
		$disponivel = false;
	}

	$cont = 0;
	if (isset($postAdicionais)) {
		foreach ($postAdicionais as $value => $subvalue) {
			if (isset($subvalue['o'])) {
				if ($subvalue['o'] == "on") {
					$con = true;
				} else {
					$con = false;
				}
			} else {
				$con = false;
			}

			if (isset($subvalue['c'])) {
				$categorias[] = array("id" => $subvalue['c'], "obrigatorio" => $con, "quantidade" => (int)$subvalue['t']);
			}

			if (!isset($subvalue['q'])) {
				$subvalue['q'] = '0';
			}

			$adicionais[] = array("key" => explode(";", $subvalue['p'])[1], "quantidadeEscolha" => (int)$subvalue['q'], "codigoBarras" => explode(";", $subvalue['p'])[0]);

			$cont++;
		}
	}

	if (isset($postProdutos)) {
		foreach ($postProdutos as $value => $subvalue) {
			$prodCombo[] = array("codigoBarras" => explode(";", $subvalue['p'])[0], "nomeProduto" => explode(";", $subvalue['p'])[1], "quantidade" => (int)$subvalue['q'], "valorDesconto" => $subvalue['v']);
		}
	} else {
		$prodCombo = null;
	}

	if ($POST['beforeimage'] == "") {
		$cnpjSplit = substr($globalData['cnpjUser'], -3);
		$nomeImagem = $globalData['nameUser'] . "-" . $cnpjSplit . "-" . $postCodigoBarras;
		$nomeImagem = str_replace(" ", "", $nomeImagem);

		$urlImage = 'http://www.nomercadosoft.com.br/imagens/produtos/' . $nomeImagem;
	} else {
		$urlImage = 'http://www.nomercadosoft.com.br/imagens/produtos/' . trim(htmlspecialchars($_POST['beforeimage'], ENT_QUOTES));
	}

	$produtos[] = array(
		"adicionais" => "Lista" . $postCodigoBarras . "PRDFL",
		"codigoBarras" => $postCodigoBarras,
		"nomeProduto" => $postNomeProduto,
		"fraseVenda" => $postFraseVendaProduto,
		"servePessoas" => (int)$postServePessoasProduto,
		"valorInicial" => (string)$postValorProduto,
		"produtosCombo" => $prodCombo,
		"categoria" => $postCategoria,
		"promocao" => $postPromocao,
		"estoque" => (float)$postQuantidadeProduto,
		"descricao" => $postDescricao,
		"categorias" => $categorias,
		"custom" => $custom,
		"disponivel" => $disponivel,
		"url" => $urlImage,
		"itensAdicionais" => $adicionais
	);

	$url = $rotas['api'] . 'web/produtos/alterarproduto';
	if ($rotas['apiBase'] != "") {
		$urlBase = $rotas['apiBase'] . 'web/produtos/alterarproduto';
	}

	$jsonData = [
		"nomeEstabelecimento" => $globalData['nameUser'],
		"codigoEstabelecimento" => $globalData['codEst'],
		"itens" => $produtos
	];

	//echo json_encode($jsonData);

	$options = array(
		'http' => array(
			'method'  => 'POST',
			'content' => json_encode($jsonData),
			'header' =>  "Content-Type: application/json\r\n" .
				"Accept: application/json\r\n"
		)
	);

	$context  = stream_context_create($options);
	if ($rotas['apiBase'] != "") {
		file_get_contents($urlBase, true, $context);
	}
	file_get_contents($url, true, $context);

	if ($source_file['tmp_name'] != "") {
		include_once('../conn/id.php');

		$destination_file = "/public/imagens/produtos/";

		if ($POST['beforeimage'] != "") {
			$nomeImagem = trim(htmlspecialchars($POST['beforeimage'], ENT_QUOTES));
		}

		$upload = ftp_put($conn_id, $destination_file . $nomeImagem, $source_file['tmp_name'], FTP_BINARY);

		if (!$upload) {
			echo "FTP upload has failed!";
		} else {
			echo "FTP upload sucess on $destination_file$nomeImagem";
		}

		ftp_close($conn_id);
	}
}
