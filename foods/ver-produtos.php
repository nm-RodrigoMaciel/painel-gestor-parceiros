<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('functions.php');

/*if (isset($_POST['produto'])) {
	alterarProduto($_POST, $globalData, $rotas);
}
if (isset($_POST['combo'])) {
	alterarCombo($_POST, $globalData, $rotas);
}*/

//API Url
$urlProd = $rotas['api'] . 'web/produtos/listar';
$urlList = $rotas['api'] . 'web/listas';

$jsonData = [
	"nomeEstabelecimento" => $globalData['nameUser'],
	"codigoEstabelecimento" => $globalData['codEst']
];

$options = array(
	'http' => array(
		'method'  => 'POST',
		'content' => json_encode($jsonData),
		'header' =>  "Content-Type: application/json\r\n" .
			"Accept: application/json\r\n"
	)
);

$context  = stream_context_create($options);
$resultProd = file_get_contents($urlProd, true, $context);
$resultList = file_get_contents($urlList, true, $context);

$jsonDataProd = json_decode($resultProd, true);
$jsonDataList = json_decode($resultList, true);

//echo json_encode($jsonDataProd);

$cont = 0;
$arrayCatTmp[] = array();

if (sizeOf($jsonDataProd) > 0 && !isset($jsonDataProd['statusCode'])) {
	foreach ($jsonDataProd as $key => $value) {

		if (!in_array($value['categoria'], $arrayCatTmp)) {
			$modalCategorias .=
				"<tr>
				<label class='area-radio'>
					<p style='margin-left:15px; margin-top:6px;'>" . $value['categoria'] . "</p>
					<input type='radio' class='radio-categorias-modal' onclick='fecharModal()' name='categorias' value='" . $value['categoria'] . "'>
					<span style='margin-left:15px;' class='checkmark'></span>
				</label>
			</tr>";
			$arrayCatTmp[] = $value['categoria'];
		}

		$checked = '';
		if ($value['disponivel']) {
			$checked = 'checked';
		}

		//Verificando se é Produto normal ou combo.
		if ($value['categoria'] == 'Combos' || $value['categoria'] == 'Combo') {
			$tipo = "combo";
		} else {
			$tipo = "produto";
		}

		//Verificando se é cadastro simples ou detalhado
		if ($value['descricao'] == "") {
			if ($tipo == "combo") {
				$ingredientesProdutos =  getProdutosPreenchido($jsonDataProd, $value['produtosCombo']);
				$tituloIngredientesProdutos  = "Produtos";
			} else {
				$ingredientesProdutos = getIngredientesPreenchidos($jsonDataList, $value['ingredientesIniciais'], $value['listasIngredientes']);
				$tituloIngredientesProdutos  = "Ingredientes";
			}

			$areaIngredientesProdutos = "<div class='dropdown-area col-2'>
			<h5>" . $tituloIngredientesProdutos . "</h5>					
			<div class='dropdown' style='width: 100%';>
				<a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
				" . $tituloIngredientesProdutos . "
				</a>
				<div id='dropdown-ing' class='dropdown-menu' aria-labelledby='dropdownMenuLink'>
					<div style='display:inline-flex'>
						<table class='table-dropdown table-ingredientes text-center'>
							" . $ingredientesProdutos . "
							</tbody>
						</table>
					</div>
				</div>
			</div>
			</div>";
		} else {
			$areaIngredientesProdutos = "<div class='col-2'>
			<h5>Descrição</h5>
			<input type='text' name='descricao' class='form-control' value='" . $value['descricao'] . "'>
			</div>";
		}

		$Produtos .=
			"<form method='post' class='form-editar-produtos' enctype='multipart/form-data' action='ver-produtos.php' style='display:flex'>
			<div class='col-9' style='display:display-block'>
				<div style='display:flex'>
					<div class='col-4'>
						<h5>Nome Produto</h5>
						<input type='text' name='nomeProduto' class='form-control' value='" . $value['nomeProduto'] . "'>
					</div>
					<input class='form-control' value='" . $value['codigoBarras'] . "' name='codigoBarras' style='display:none'></input>
					<div class='col-2'>
						<h5>Valor</h5>
						<input type='text' name='valorInicial' class='form-control campo-valor' value='" . $value['valorInicial'] . "'>
					</div>
					<div class='dropdown-area dropdown-categorias col-2' >
						<h5>Categoria</h5>			
						<div class='dropdown col-12'>
							<a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false' onclick='abrirModal($cont)'>
								<span class='text-categoria-$cont'>" . $value['categoria'] . "</span>
								<input class='value-categoria-$cont form-control' name='categoria' value='" . $value['categoria'] . "' style='display: none;'></input>
							</a>
						</div>
					</div>	
					$areaIngredientesProdutos
					<div class='dropdown-area col-2'>
						<h5>Adicionais</h5>				
						<div class='dropdown' style='width: 100%';>
							<a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
								Adicionais
							</a>
							<div id='dropdown-adi' class='dropdown-menu dropdown-center' aria-labelledby='dropdownMenuLink'>        
								<div style='display:inline-flex'>
									<table class='table-dropdown table-adicionais'>
										<thead>
											<tr>
												<th scope='col'>Selecionar</th>
												<th scope='col'>Nome</th>
												<th scope='col'>Valor</th>
												<th scope='col'>Lista</th>
												<th scope='col'>Quantidade</th>
												<th scope='col'>Obrigatorio</th>
											</tr>
											" . getAdicionaisPreenchidos($jsonDataList, $value['categorias'], $value['itensAdicionais']) . "
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div style='display:flex'>
					<div class='col-2'>
						<h5>Estoque</h5>
						<input type='number' min='0' name='estoque' class='form-control' min='0' placeholder='Quantidade' value='" . $value['estoque'] . "'>
					</div>		
					<div class='col-2'>
						<h5>Promoção</h5>
						<input type='text' name='promocao' class='form-control' value='" . $value['promocao'] . "' placeholder='Promoção'>
					</div>
					<div class='col-2'>
						<h5>Serve quantas pessoas?</h5>
						<input type='text' name='servePessoas' class='form-control' placeholder='Serve quantas pessoas?' value='" . $value['servePessoas'] . "'>
					</div>		
					<div class='col-6'>
						<h5>Frase Venda</h5>
						<input type='text' name='fraseVenda' class='form-control' placeholder='Frase Venda' value='" . $value['fraseVenda'] . "'>
					</div>	
				</div>
			</div>
			<div class='col-3' style='display:flex; padding-top:55px !important;'>
				<div class='col text-center'>
				<h5>Disponível</h5>
					<input id='check-list' type='checkbox' class='check-disp' name='disponivel' class='input-checkbox' $checked>
				</div>		
				<div class='area-image;'>
					<div class='circle upload-area' pos='$cont'>
						<img class='profile-pic profile-pic-$cont' src='" . $value['url'] . "?" . time() . "'>
					</div>
					<div class='p-image'>
						<input name='file' class='file-upload upload-$cont' pos='$cont' type='file' accept='image/png' />
					</div>
				</div>
				<div class='col text-center'>
					<span class='btn btn-atualizar-produtos btn-danger'><i class='fa fa-pencil' aria-hidden='true'></i><div class='lds-ring' style='display:none;'><div></div><div></div><div></div><div></div></div></span>
				</div>
			</div>	
		</form>";
		$cont += 1;
	}
} else {
	$Produtos = "<div class='area-nada-por-aqui'><div><h3>Não há produtos cadastratdos...</h3></div><img src='../images/nada_por_enquanto.png'></div>";
}

//Era usado para adiantar o processo de edição mas estava causando bugs...
/*<input name='url' class='form-control' value='" . $value['url'] . "'>*/
?>

<body>
	<div class='site-section'>
		<div class='col-12'>
			<h2 class='section-title mb-3 text-center' style='margin-top:25px'>Produtos Cadastrados</h2>
			<div class='section-ver-produtos ver-produtos produtos'>
				<?php echo $Produtos ?>
			</div>
		</div>
	</div>
	<div id="modal-categorias" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Categorias</h4>
				</div>
				<div class="modal-body">
					<?php echo $modalCategorias ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-modal-no" data-dismiss="modal">Fechar</button>
				</div>
			</div>

		</div>
	</div>
	<?php
	include_once('../footer.php');
	?>
	<script>
		var modal_cat;

		function abrirModal(i) {
			modal_cat = i;
			$('#modal-categorias').modal('show');
		}

		function fecharModal() {
			$('#modal-categorias').modal('hide');
		}

		$(".radio-categorias-modal").on('change', function() {
			$('.text-categoria-' + modal_cat).text($(this).val());
			$('.value-categoria-' + modal_cat).val($(this).val());
		});

		$(document).on('click', '.btn-atualizar-produtos', function() {

			$('.fa-pencil', this).hide();
			$('.lds-ring', this).show();

			var parent = $(this).parent().parent().parent();

			var itens = {};

			itens['adicionais'] = "Lista" + $('input[name=codigoBarras]', parent).val() + "PRDFL";
			itens['custom'] = true;

			$(".form-control", parent).each(function(index) {

				itens[$(this).attr('name')] = $(this).val();

			});

			itens['servePessoas'] = parseInt(itens['servePessoas']);
			itens['estoque'] = parseInt(itens['estoque']);

			//Preencher categorias e itensAdicionais
			itens.categorias = [];
			itens.itensAdicionais = [];
			$(".titulo-categoria", parent).each(function(index) {
				var has = false;
				var ref = $('.obrig-ad-title', this).attr('ref');

				$(".check-ad[ref=" + ref + "]", parent).each(function(index) {
					if ($(this).is(':checked')) {
						var parentAdi = $(this).parent().parent();
						var itensAdicionais = {};

						itensAdicionais['key'] = $(this).val().split(";")[1];
						itensAdicionais['quantidadeEscolha'] = $('td input[type=number]', parentAdi).val();
						itensAdicionais['codigoBarras'] = $(this).val().split(";")[0];

						itens.itensAdicionais.push(itensAdicionais);

						has = true;
					}
				});

				if (has) {
					var categorias = {};

					categorias['id'] = $('.linha-obrigatoria .obrig-ad-title', this).attr('value');
					categorias['obrigatorio'] = $('.linha-obrigatoria .obrig-ad-title', this).is(':checked');

					if (categorias['obrigatorio'])
						categorias['quantidade'] = 0;
					else
						categorias['quantidade'] = parseInt($('input[type=number]', this).val());

					itens.categorias.push(categorias);
				}
			});

			itens['disponivel'] = $(".check-disp", parent).is(':checked');

			if ($(".ci", parent).length > 0) {
				var ingredientesIniciais = "";
				var listasIngredientes = "";
				$(".ci", parent).each(function(index) {
					if ($(this).is(':checked')) {
						var parentIng = $(this).parent().parent();

						ingredientesIniciais += $('.ci', parentIng).val().split(";")[0] + ";" + $('.number-in', parentIng).val() + ";";

						listasIngredientes += $('.ci', parentIng).val().split(";")[1] + ";";
					}
				});

				itens['ingredientesIniciais'] = ingredientesIniciais.slice(0, -1);
				itens['listasIngredientes'] = listasIngredientes.slice(0, -1);
			} else if ($(".cp", parent).length > 0) {
				itens.produtosCombo = [];
				$(".cp", parent).each(function(index) {
					if ($(this).is(':checked')) {
						var prodCombo = {};
						var parentProd = $(this).parent().parent();

						prodCombo['codigoBarras'] = $('.cp', parentProd).val().split(";")[0];
						prodCombo['nomeProduto'] = $('.cp', parentProd).val().split(";")[1];
						prodCombo['quantidade'] = parseInt($('.number-val', parentProd).val());
						prodCombo['valorDesconto'] = $('.number-in', parentProd).val();

						itens.produtosCombo.push(prodCombo);
					}
				});
			}

			itens = [itens];

			/*var post = {};*/

			/*post.nomeEstabelecimento = "Rainha Hamburgueria";
			post.codigoEstabelecimento = 5615;
			post.itens = itens;*/

			$.post("atualizarProduto.php", {
					itens: JSON.stringify(itens)
				},
				function(data, status) {
					$('.fa-pencil').show();
					$('.lds-ring').hide();
					if (status == "success")
						alert("Produto atualizado com sucesso!");
					else
						alert("Algo aconteceu ao tentar atualizar o produto!");
				});

			//$('body').append(JSON.stringify(itens));
			//alert(JSON.stringify(post));
		});

		$(document).on('change', '.file-upload', function() {
			var parent = $(this).parent().parent().parent().parent();

			var url = $('input[name=url]', parent).val();
			var codigoBarras = $('input[name=codigoBarras]', parent).val();

			var file_data = $(this).prop('files')[0];

			if (file_data != null) {
				var form_data = new FormData();
				form_data.append('file', file_data);
				form_data.append('url', url);
				form_data.append('codigoBarras', codigoBarras);

				$.ajax({
					url: 'atualizarImagemProduto.php',
					dataType: 'text',
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'post',
					success: function(php_script_response) {
						alert('Imagem atualizada com sucesso!');
					}
				});
			}
		});
	</script>