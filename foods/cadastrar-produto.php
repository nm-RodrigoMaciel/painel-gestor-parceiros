<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('functions.php');

//API Url
$urlList = $rotas['api'] . 'web/listas';
$urlProd = $rotas['api'] . 'web/produtos/listar';

$jsonData = [
	"nomeEstabelecimento" => $globalData['nameUser'],
	"codigoEstabelecimento" => $globalData['codEst']
];

$options = array(
	'http' => array(
		'method'  => 'POST',
		'content' => json_encode($jsonData),
		'header' =>  "Content-Type: application/json\r\n" .
			"Accept: application/json\r\n"
	)
);

$context  = stream_context_create($options);
$resultList = file_get_contents($urlList, true, $context);
$resultProd = file_get_contents($urlProd, true, $context);

$jsonDataList = json_decode($resultList, true);
$jsonDataProd = json_decode($resultProd, true);

//echo json_encode($jsonDataList);

//Listando produtos para preencher o combo
$contProd = 0;
$arrayCatTmp[] = array();
foreach ($jsonDataProd as $key => $value) {
	//Listando categorias apartir das categorias dos produtos cadastrados, uso o in_array pra evitar categorias duplicadas 
	if (!in_array($value['categoria'], $arrayCatTmp)) {
		$valCategorias .=
			"<tr>
				<label class='area-radio'>
					<p style='margin-left:15px; margin-top:6px;'>" . $value['categoria'] . "</p>
					<input type='radio' class='radio-categorias' name='categorias' value='" . $value['categoria'] . "'>
					<span style='margin-left:15px;' class='checkmark'></span>
				</label>
			</tr>";
		$arrayCatTmp[] = $value['categoria'];
	}
	$contProd++;
}

//Obtendo adicionais
$valComboIngredientes = getIngredientes($jsonDataList);

//Obtendo adicionais
$valComboAdicionais = getAdicionais($jsonDataList);

$option = 'Produto';
if (isset($_POST['produto']) || !isset($_POST['combo'])) {
	$preview = "<div class='area-ing-adc col-2'>
	<div class='area-ing' style='text-align:center;'>
		<table class=''>
			<thead>
				<tr>
					<h3 style='margin: 0 10px 10px 10px;'>Ingredientes</h3>
				</tr>
				<tr>
					<th scope='col'>Nome</th>
					<th scope='col'>Quantidade</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<div class='area-adc' style='text-align:center;'>
		<table class=''>
			<thead>
				<tr>
					<h3 style='margin: 0 10px 10px 10px;'>Adicionais</h3>
				</tr>
				<tr>
					<th scope='col text-center'>Nome</th>
					<th scope='col text-center'>Quantidade</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>";

	$option = 'Produto';
	$tipo = "<div class='col-6 campo-detalhado' style='text-align: left; display: flex; margin: 5px 0 10px 0;'>
	<p style='margin-right:10px;'>Possui ingredientes?</p>
	<input type='checkbox' class='check-ing' class='input-checkbox'>
	</div>
	<div class='dropdown col-12 dropdown-ingredientes campo-detalhado' style='padding-right: 0px; padding-left: 0px; margin-bottom:10px; display:none;'>
	<a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
		Ingredientes iniciais
	</a>
	<div id='dropdown' class='dropdown-menu col-12' aria-labelledby='dropdownMenuLink'>
		<table class='table-dropdown table-ingredientes col-12 text-center'>
			<thead>
				<tr>
					<th scope='col'>Selecionar</th>
					<th scope='col'>Nome</th>
					<th scope='col'>Lista</th>
					<th scope='col'>Quantidade</th>
				</tr>
			</thead>
			<tbody>
				$valComboIngredientes
			</tbody>
		</table>
	</div>
</div>";
	$buttonSubmit = "<button type='submit' name='cadProduto' class='btn btn-submit btn-danger'>Cadastrar Produto</button>";
} else if (isset($_POST['combo'])) {
	$preview = "<div class='area-ing-adc col-2'>
	<div class='area-ing' style='text-align:center;'>
		<table class=''>
			<thead>
				<tr>
					<h3 style='margin: 0 10px 10px 10px;'>Produtos</h3>
				</tr>
				<tr>
					<th scope='col'	style='padding: 0 10px 10px 10px;'>Nome</th>
					<th scope='col' style='padding: 0 10px 10px 10px;'>Desconto</th>
					<th scope='col' style='padding: 0 10px 10px 10px;'>Quantidade</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<div class='area-adc' style='text-align:center;'>
		<table class=''>
			<thead>
				<tr>
					<h3 style='margin: 0 10px 10px 10px;'>Adicionais</h3>
				</tr>
				<tr>
					<th scope='col text-center'>Nome</th>
					<th scope='col text-center'>Quantidade</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>";

	$option = 'Combo';
	$tipo = "<div class='dropdown col-12 campo-detalhado' style='padding-right: 0px; padding-left: 0px; margin-bottom:10px;'>
	<a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
		Produtos iniciais
	</a>
	<div id='dropdown' class='dropdown-menu col-12' aria-labelledby='dropdownMenuLink'>
		<table class='table-dropdown table-ingredientes col-12 text-center'>
			<thead>
				<tr>
					<th scope='col'>Selecionar</th>
					<th scope='col'>Nome</th>
					<th scope='col'>Valor de Desconto</th>
					<th scope='col'>Quantidade</th>					
				</tr>
			</thead>
			<tbody>".getProdutos($jsonDataProd)."</tbody>
		</table>
	</div>
</div>";
	$buttonSubmit = "<button type='submit' name='cadCombo' class='btn btn-submit btn-danger'>Cadastrar Combo</button>";
}
?>

<body>
	<div class="site-section">
		<div class="col-12">
			<h2 class="section-title title-cad-pro mb-3 text-center">Cadastrar produtos</h2>
			<div class="col-8 text-center area-tipos">
				<div style="display:inline-flex;">
					<form name="produto" method="post" action="cadastrar-produto.php">
						<button type="submit" name="produto" class="btn btn-submit btn-danger">Produto</button>
					</form>
					<form name="combo" method="post" action="cadastrar-produto.php">
						<button type="submit" name="combo" class="btn btn-submit btn-danger">Combo</button>
					</form>
				</div>
			</div>
			<form method="post" class="text-center" enctype="multipart/form-data" action="cadastrar-produto.php">
				<div class="col-12" style="display: inline-flex;">
					<div class="main-section col-6 text-center">
						<div class="col-10 main-form">
							<div class="text-center" style="margin-bottom: 15px;">
								<h3><?php echo "Definições do " . $option ?></h3>
							</div>
							<div>
								<div style="display: inline-flex; margin: 5px 0">
									<h3 style="margin-right: 10px;">Tipo de cadastro:</h3>
									<input type="radio" id="detalhado" class='radio-cadastro' name="gender" value="d" style="margin-right: 5px;" checked>
									<label for="detalhado" style="margin-right: 10px; font-size: 14px;">Detalhado</label>
									<input type="radio" id="simples" class='radio-cadastro' name="gender" style="margin-right: 5px;" value="s">
									<label for="simples" style="font-size: 14px;">Simples</label>
								</div>
							</div>
							<input type="text" name="nomeProduto" class="form-control" placeholder="Nome do produto" required>
							<input type="text" name="fraseVendaProduto" class="form-control" placeholder="Descrição produto (Opcional)">
							<input type="number" name="servePessoasProduto" class="form-control" placeholder="Serve quantas pessoas? (Opcional)">
							<input type="text" name="medidaProduto" class="form-control" placeholder="Medida produto (Opcional)">
							<input type="text" name="valorProduto" class="form-control campo-valor" placeholder="Valor do produto" required>
							<input type="text" name="promocao" class="form-control campo-promocao" placeholder="Promoção (Opcional)">
							<div class="col" style="display: inline-flex;">
								<h5 style="margin-top: 4px;">O valor da promoção já está calculado?</h5>
								<input type="radio" id="nao" class='radio-calculo' style="margin-left: 10px;" value="nao" name="calc" disabled checked>
								<label for="nao" style="font-size: 12px;margin-left: 5px;">Sim</label>
								<input type="radio" id="sim" class='radio-calculo' value="sim" style="margin-left: 10px;" name="calc" disabled>
								<label for="sim" style="margin-left: 5px; font-size: 12px;">Não</label>
							</div>
							<div class="col-6" style="text-align: left; display: flex; margin: 10px 0 10px 0;">
								<p style="margin-right:10px;">Disponível</p>
								<input id='check-list' type='checkbox' class='check-disp' name='disponivel' class='input-checkbox' checked>
							</div>
							<div class="dropdown col-12" style="padding-right: 0px; padding-left: 0px; margin-bottom:10px;">
								<a class="btn dropdown-toggle col-12" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<span class="text-categoria">Selecionar categoria</span>
									<input class="value-categoria" name='categorias' style="display: none;"></input>
								</a>
								<div id="dropdown" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<table class="table-dropdown-categorias">
										<tbody>
											<tr>
												<input type="text" name="valorNovaCategoria" class="campo-categoria form-control col-12" placeholder="Nova categoria">
												<?php echo $valCategorias; ?>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<textarea class="form-control campo-simples" name='descricao' placeholder="Descrição. Exemplo: Duas carnes de hamburguer, dois alfaces e um queijo chedar" rows="5" style="display:none;"></textarea>
							<?php echo $tipo ?>
							<div class="col-6" style="text-align: left; display: flex; margin: 10px 0 10px 0;">
								<p style="margin-right:10px;">Possui adicionais?</p>
								<input type='checkbox' class='check-adicional' name='check-adicional' class='input-checkbox'>
							</div>
							<div class="dropdown col-12 dropdown-adicionais" style="padding-right: 0px; padding-left: 0px; margin-bottom:13px; display:none;">
								<a class="btn dropdown-toggle col-12" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									Adicionais
								</a>
								<div id="dropdown" class="dropdown-menu col-12" aria-labelledby="dropdownMenuLink">
									<table class="table-dropdown table-adicionais col-12 text-center">
										<thead>
											<tr>
												<th scope="col">Selecionar</th>
												<th scope="col">Nome</th>
												<th scope="col">Valor</th>
												<th scope="col">Lista</th>
												<th scope="col">Quantidade</th>
												<th scope="col">Obrigatorio</th>
											</tr>
										</thead>
										<tbody>
											<?php echo $valComboAdicionais; ?>
										</tbody>
									</table>
								</div>
							</div>
							<input type="number" name="quantidadeProduto" class="form-control" min="0" placeholder="Estoque" required>
						</div>
					</div>
					<div id="area-lista" class="col-4">
						<h2 style="color: #000; margin-bottom:20px;">Imagem do produto</h2>
						<div>
							<div class="circle upload-area" pos="0">
								<img class="profile-pic profile-pic-0" pos="0" src="https://nomercadosoft.com.br/img/default-image.png">
							</div>
							<div class="p-image">
								<input name="file" class="file-upload upload-0" pos="0" type="file" accept="image/png" />
							</div>
						</div>
					</div>
					<?php
					echo $preview
					?>
				</div>
				<?php
				echo $buttonSubmit
				?>
			</form>
			<?php
			if (isset($_POST['cadProduto'])) {
				$postNomeProduto = trim(htmlspecialchars($_POST['nomeProduto'], ENT_QUOTES));
				$postFraseVendaProduto = trim(htmlspecialchars($_POST['fraseVendaProduto'], ENT_QUOTES));
				$postServePessoasProduto = trim(htmlspecialchars($_POST['servePessoasProduto'], ENT_QUOTES));
				$postValorProduto = trim(htmlspecialchars($_POST['valorProduto'], ENT_QUOTES));
				$postQuantidadeProduto = trim(htmlspecialchars($_POST['quantidadeProduto'], ENT_QUOTES));
				$postPromocao = trim(htmlspecialchars($_POST['promocao'], ENT_QUOTES));
				$postIngredientes = $_POST['ingredientes'];
				$postAdicionais = $_POST['adicionais'];
				$postDisponivel = trim(htmlspecialchars($_POST['disponivel'], ENT_QUOTES));
				$postCategoria = trim(htmlspecialchars($_POST['categorias'], ENT_QUOTES));
				$postDescricao = trim(htmlspecialchars($_POST['descricao'], ENT_QUOTES));

				if($postPromocao == null || $postPromocao == ""){
					$postPromocao = "0";
				}

				if (isset($postAdicionais)) {
					$custom = true;
				} else {
					$custom = false;
				}

				if ($postDisponivel == "on") {
					$disponivel = true;
				} else {
					$disponivel = false;
				}

				$cont = 0;
				if (isset($postAdicionais)) {
					foreach ($postAdicionais as $value => $subvalue) {
						if (isset($subvalue['o'])) {
							if ($subvalue['o'] == "on") {
								$con = true;
							} else {
								$con = false;
							}
						} else {
							$con = false;
						}

						if (isset($subvalue['c'])) {
							$categorias[] = array("id" => $subvalue['c'], "obrigatorio" => $con, "quantidade" => (int)$subvalue['t']);
						}

						if (!isset($subvalue['q'])) {
							$subvalue['q'] = '0';
						}

						$adicionais[] = array("key" => explode(";", $subvalue['p'])[1], "quantidadeEscolha" => (int)$subvalue['q'], "codigoBarras" => explode(";", $subvalue['p'])[0]);

						$cont++;
					}
				}

				if (isset($postIngredientes)) {
					foreach ($postIngredientes as $value => $subvalue) {
						if (isset($ingredientesIniciais)) {
							$ingredientesIniciais = $ingredientesIniciais . ";" . explode(";", $subvalue['p'])[0] . ";" . $subvalue['q'];
							$listasIngredientes = $listasIngredientes . ";" . explode(";", $subvalue['p'])[1];
						} else {
							$ingredientesIniciais = explode(";", $subvalue['p'])[0] . ";" . $subvalue['q'];
							$listasIngredientes = explode(";", $subvalue['p'])[1];
						}
					}
				} else {
					$ingredientesIniciais = null;
					$listasIngredientes = null;
				}

				$cnpjSplit = substr($globalData['cnpjUser'], -3);
				$nomeImagem = $globalData['nameUser'] . "-" . $cnpjSplit . "-" . $jsonData[0]['codigoBarras'];
				$nomeImagem = str_replace(" ", "", $nomeImagem);

				$produtos[] = array(
					"nomeProduto" => $postNomeProduto,
					"fraseVenda" => $postFraseVendaProduto,
					"servePessoas" => (int)$postServePessoasProduto,
					"valorInicial" => (string)$postValorProduto,
					"categoria" => $postCategoria,
					"promocao" => $postPromocao,
					"estoque" => (float)$postQuantidadeProduto,
					"ingredientesIniciais" => $ingredientesIniciais,
					"descricao" => $postDescricao,
					"listasIngredientes" => $listasIngredientes,
					"categorias" => $categorias,
					"custom" => $custom,
					"disponivel" => $disponivel,
					"url" => 'http://www.nomercadosoft.com.br/imagens/produtos/' . $nomeImagem,
					"itensAdicionais" => $adicionais
				);
				include_once('cadastro.php');
			}

			if (isset($_POST['cadCombo'])) {
				$postNomeProduto = trim(htmlspecialchars($_POST['nomeProduto'], ENT_QUOTES));
				$postFraseVendaProduto = trim(htmlspecialchars($_POST['fraseVendaProduto'], ENT_QUOTES));
				$postServePessoasProduto = trim(htmlspecialchars($_POST['servePessoasProduto'], ENT_QUOTES));
				$postValorProduto = trim(htmlspecialchars($_POST['valorProduto'], ENT_QUOTES));
				$postQuantidadeProduto = trim(htmlspecialchars($_POST['quantidadeProduto'], ENT_QUOTES));
				$postPromocao = trim(htmlspecialchars($_POST['promocao'], ENT_QUOTES));
				$postAdicionais = $_POST['adicionais'];
				$postDisponivel = trim(htmlspecialchars($_POST['disponivel'], ENT_QUOTES));
				$postCategoria = trim(htmlspecialchars($_POST['categorias'], ENT_QUOTES));
				$postProdutos = $_POST['produtos'];
				$postDescricao = trim(htmlspecialchars($_POST['descricao'], ENT_QUOTES));

				if($postPromocao == null || $postPromocao == ""){
					$postPromocao = "0";
				}

				if (isset($postAdicionais)) {
					$custom = true;
				} else {
					$custom = false;
				}

				if ($postDisponivel == "on") {
					$disponivel = true;
				} else {
					$disponivel = false;
				}

				$cont = 0;
				if (isset($postAdicionais)) {
					foreach ($postAdicionais as $value => $subvalue) {
						if (isset($subvalue['o'])) {
							if ($subvalue['o'] == "on") {
								$con = true;
							} else {
								$con = false;
							}
						} else {
							$con = false;
						}

						if (isset($subvalue['c'])) {
							$categorias[] = array("id" => $subvalue['c'], "obrigatorio" => $con, "quantidade" => (int)$subvalue['t']);
						}

						if (!isset($subvalue['q'])) {
							$subvalue['q'] = '0';
						}

						$adicionais[] = array("key" => explode(";", $subvalue['p'])[1], "quantidadeEscolha" => $subvalue['q'], "codigoBarras" => explode(";", $subvalue['p'])[0]);

						$cont++;
					}
				}

				if (isset($postProdutos)) {
					foreach ($postProdutos as $value => $subvalue) {
						$prodCombo[] = array("codigoBarras" => explode(";", $subvalue['p'])[0], "nomeProduto" => explode(";", $subvalue['p'])[1], "quantidade" => (int)$subvalue['q'], "valorDesconto" => $subvalue['v']);
					}
				} else {
					$prodCombo = null;
				}

				$cnpjSplit = substr($globalData['cnpjUser'], -3);
				$nomeImagem = $globalData['nameUser'] . "-" . $cnpjSplit . "-" . $jsonData[0]['codigoBarras'];
				$nomeImagem = str_replace(" ", "", $nomeImagem);

				$produtos[] = array(
					"nomeProduto" => $postNomeProduto,
					"fraseVenda" => $postFraseVendaProduto,
					"servePessoas" => (int)$postServePessoasProduto,
					"valorInicial" => (string)$postValorProduto,
					"categoria" => $postCategoria,
					"promocao" => $promocao,
					"estoque" => (float)$postQuantidadeProduto,
					"produtosCombo" => $prodCombo,
					"descricao" => $postDescricao,
					"categorias" => $categorias,
					"custom" => $custom,
					"disponivel" => $disponivel,
					"url" => 'http://www.nomercadosoft.com.br/imagens/produtos/' . $nomeImagem,
					"itensAdicionais" => $adicionais
				);
				include_once('cadastro.php');
			}
			?>
		</div>
	</div>
	<?php
	include_once('../footer.php');
	?>