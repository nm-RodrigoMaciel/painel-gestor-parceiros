<?php
include_once('../rotas.php');
include_once('../variables.php');

$url = trim(htmlspecialchars($_POST['url'], ENT_QUOTES));
$codigoBarras = trim(htmlspecialchars($_POST['codigoBarras'], ENT_QUOTES));
$source_file = $_FILES['file'];

/*if ($url != ""){
    $url = explode("produtos/", $url)[1];
}
else{*/
    $cnpjSplit = substr($globalData['cnpjUser'], -3);
    $nomeImagem = $globalData['nameUser'] . "-" . $cnpjSplit . "-" . $codigoBarras . ".png";
    $url = str_replace(" ", "", $nomeImagem);
/*}*/

if ($source_file['tmp_name'] != "") {
    include_once('../conn/id.php');

    $destination_file = "/public/imagens/produtos/";

    $upload = ftp_put($conn_id, $destination_file . $url, $source_file['tmp_name'], FTP_BINARY);

    if (!$upload) {
        //echo "FTP upload has failed!";
    } else {
        //echo "FTP upload sucess on $destination_file$nomeImagem";
    }

    ftp_close($conn_id);
}
