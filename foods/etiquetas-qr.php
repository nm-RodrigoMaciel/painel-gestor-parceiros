<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('functions.php');
?>

<body>
	<div class="site-section etiquetas-qr-section">
		<div class="container">
			<div class="col-12">
				<div class="col" style='display:flex; margin-top:10px;'>
					<div class="col-6">
						<div class="flex header">
							<h3 class="title">Quantidade de mesas</h3>
							<input type="text" class="mesa">
						</div>
						<div class="search">
							<span>Baseado na quantidade de mesas do seu estabelecimento geraremos etiquetas para cada mesa a partir de 1.</span>
							<input type="button" value="Gerar etiquetas">
						</div>
						<div class="list-desks " style="display: none;">
							<div class="more-item flex">
								<div class="more flex">
									<div>
										<img src="../images/plus-qr.svg">
									</div>
									<span>Adicionar nova mesa</span>
								</div>
								<div class="all-qr flex">
									<div>
										<img src="../images/all-qr.svg">
									</div>
									<span>Baixar todas etiquetas</span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6">
						<div id="qr-code" style="display: none; margin: 72px 0px 0px 56px; height: 304px; width: 304px; border: 0.5px solid rgba(0, 0, 0, 0.096); text-align: center; background: white;">
							<span class="mesa-qr" style="position: relative; top: 9px; font-family: Arial; text-align: center; font-size: 24px; font-weight: 700; color: #000000;">MESA</span>
							<div class="qrcode" style="height: 202px; width: 202px; position: relative; top: 112px; left: 50%; transform: translate(-50%, -50%);"></div>
							<span class="text-qr" style="position: relative; bottom: -20px; font-family: Arial; text-align: center; font-size: 18px; color: #000000;">Escaneie com o celular</span>
						</div>
					</div>
				</div>
				<h3 id='title-printer' style='margin-top: 32px; margin-bottom: 24px; display: none;'>Ultima impressão:</h3>
			</div>
			<div id="printer-area" class="col-12" style="display: flex; flex-wrap: wrap; width: 942px;">

			</div>
		</div>
	</div>
	<?php include_once('../footer.php'); ?>
	<link rel="stylesheet" href="{{ public_path(mix('../css/style.css')) }}" />
	<script type="text/javascript" src="../js/jquery.qrcode.js"></script>
	<script type="text/javascript" src="../js/qrcode.js"></script>
	<script src="../js/es6-promise.auto.min.js"></script>
	<script src="../js/jspdf.min.js"></script>
	<script src="../js/html2pdf.bundle.min.js"></script>
	<script src="../js/html2pdf.min.js"></script>
	<script src="../js/jsmd5.min.js"></script>
	<script>
		$(document).on('click', '.etiquetas-qr-section .search input[type="button"]', function() {

			$('.etiquetas-qr-section .list-desks .item').remove();

			var qtd = parseInt($('.etiquetas-qr-section .header input[type="text"]').val());

			if (qtd > 0) {
				$('.etiquetas-qr-section .search').hide();
				for (var i = 1; i <= qtd; i++) {
					if (i < 10)
						i = '0' + i;

					$('.etiquetas-qr-section .more-item').before("<div class='item flex' pos='" + i + "' url='https://www.nomercadosoft.com.br/cardapio-digital/?mesa=" + i + "&v="+$.md5('https://www.nomercadosoft.com.br/cardapio-digital/?mesa=')+"' ><span>" + i + "</span> <div class='actions flex'> <div><img src='../images/trash-qr.svg'></div> <div><img src='../images/download-qr.svg'></div> </div>");
				}
				$('.etiquetas-qr-section .list-desks').show();
			} else {
				alert("Digite um valor valido.");
			}

		});

		$(document).on('click', '.etiquetas-qr-section .more-item .more', function() {
			var newItem = parseInt($('.etiquetas-qr-section .list-desks .item').last().attr('pos')) + 1;

			if (newItem < 10)
				newItem = '0' + newItem;

			$('.etiquetas-qr-section .more-item').before("<div class='item flex' pos='" + newItem + "' url='https://www.nomercadosoft.com.br/cardapio-digital/?mesa=" + newItem + "&v="+$.md5('https://www.nomercadosoft.com.br/cardapio-digital/?mesa=')+"' ><span>" + newItem + "</span> <div class='actions flex'> <div><img src='../images/trash-qr.svg'></div> <div><img src='../images/download-qr.svg'></div> </div>");
		});

		$(document).on('click', '.etiquetas-qr-section .list-desks .item .actions div:nth-child(1)', function() {
			$(this).parent().parent().remove();
		});

		$(document).on('click', '.etiquetas-qr-section .list-desks .item', function() {

			$('#qr-code').show();
			$('#qr-code canvas').remove();

			var pos = $(this).attr('pos');
			var url = $(this).attr('url');

			$('#qr-code .mesa-qr').text("MESA " + pos);

			$('.qrcode').qrcode({
				width: 202,
				height: 202,
				text: url
			});

			$(document).scrollTop(0);
		});

		$(document).on('click', '.etiquetas-qr-section .list-desks .item .actions div:nth-child(2)', function() {
			$('.etiquetas-qr-section #printer-area *').remove();
			
			$('#title-printer').show();

			var pos = $(this).parent().parent().attr('pos')

			$('.etiquetas-qr-section #printer-area').append("<div id='qr-code-" + pos + "' style='height: 204px; width: 204px; border: 0.5px solid rgba(0, 0, 0, 0.096); text-align: center; background: white;'> <span class='mesa-qr' style='position: relative; top: 9px; font-family: Arial; text-align: center; font-size: 24px; font-weight: 700; color: #000000;'>MESA</span> <div class='qrcode-" + pos + "' style='height: 102px; width: 102px; position: relative; top: 63px; left: 50%; transform: translate(-50%, -50%);'></div> <span class='text-qr' style='position: relative; bottom: -20px; font-family: Arial; text-align: center; font-size: 18px; color: #000000;'>Escaneie com o celular</span> </div>");

			$('#qr-code-' + pos + ' .mesa-qr').text("MESA " + pos);

			$('.qrcode-' + pos).qrcode({
				width: 102,
				height: 102,
				text: "https://www.nomercadosoft.com.br/cardapio-digital/?mesa=" + pos + "&v="+$.md5('https://www.nomercadosoft.com.br/cardapio-digital/?mesa=')
			});

			var element = document.getElementById('printer-area');
			var opt = {
				margin: 0.1,
				filename: 'qrcode-mesas.pdf',
				image: {
					type: 'jpeg',
					quality: 1
				},
				html2canvas: {
					scale: 5
				},
				jsPDF: {
					unit: 'in',
					format: 'letter',
					orientation: 'portrait',
				}
			};

			$(document).scrollTop(0);

			html2pdf().from(element).set(opt).save();
			//html2pdf(element, opt);
		});

		$('.etiquetas-qr-section .mesa').keypress(function(e) {
			if (e.which == 13) {
				$('.etiquetas-qr-section .search input[type="button"]').click();
			}
		});

		$(document).on('click', '.etiquetas-qr-section .more-item .all-qr', function() {
			$('.etiquetas-qr-section #printer-area *').remove();

			$(document).scrollTop(0);

			$('#title-printer').show();

			$(".etiquetas-qr-section .list-desks .item").each(function(index) {
                var i = parseInt($(this).attr('pos'));
            
				if (i < 10)
					i = '0' + i;

				$('.etiquetas-qr-section #printer-area').append("<div id='qr-code-" + i + "' style='height: 204px; width: 204px; border: 0.5px solid rgba(0, 0, 0, 0.096); text-align: center; background: white;'> <span class='mesa-qr' style='position: relative; top: 9px; font-family: Arial; text-align: center; font-size: 24px; font-weight: 700; color: #000000;'>MESA</span> <div class='qrcode-" + i + "' style='height: 102px; width: 102px; position: relative; top: 63px; left: 50%; transform: translate(-50%, -50%);'></div> <span class='text-qr' style='position: relative; bottom: -20px; font-family: Arial; text-align: center; font-size: 18px; color: #000000;'>Escaneie com o celular</span> </div>");

				$('#qr-code-' + i + ' .mesa-qr').text("MESA " + i);

				$('.qrcode-' + i).qrcode({
					width: 102,
					height: 102,
					text: "https://www.nomercadosoft.com.br/cardapio-digital/?mesa=" + i + "&v="+$.md5('https://www.nomercadosoft.com.br/cardapio-digital/?mesa=')
				});
			});

			var element = document.getElementById('printer-area');
			var opt = {
				margin: 0.19,
				filename: 'qrcode-mesas.pdf',
				image: {
					type: 'jpeg',
					quality: 1
				},
				html2canvas: {
					scale: 5
				},
				jsPDF: {
					unit: 'in',
					format: 'letter',
					orientation: 'portrait',
				}
			};

			html2pdf().from(element).set(opt).save();
			//html2pdf(element, opt);
		});
	</script>