<?php
include("../header.php");
include("../rotas.php");
include("../getEstado.php");

if (isset($_POST['submit'])) {
    $postUid = $_POST['users'];

    $postTitulo = trim(htmlspecialchars($_POST['titulo'], ENT_QUOTES));
    $postTexto = trim(htmlspecialchars($_POST['texto'], ENT_QUOTES));

    $users = array();
    foreach ($postUid as $value) {
        $exploded = explode(";", $value);
        foreach ($exploded as $i => $key) {
            if ($i != 0) {
                $users[] = $key;
            }
        }
    }

    $globalData[] = array(
        "users" => $users,
        "titulo" => $postTitulo,
        "texto" => $postTexto
    );

    $url = $rotas['api'] . 'app/notificacoes/userlist';

    $ch = curl_init($url);

    $jsonData = [
        "nomeEstabelecimento" => $globalData['nameUser'],
        "codigoEstabelecimento" => $globalData['codEst'],
        "estado" => $estado,
        "data" => $data
    ];

    //echo json_encode($jsonData);

    $options = array(
        'http' => array(
            'method'  => 'POST',
            'content' => json_encode($jsonData),
            'header' =>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
        )
    );

    $context  = stream_context_create($options);
    $result = file_get_contents($url, true, $context);
}

$url = $rotas['api'] . 'analisedados/list/listfromproductusers';

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst']
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$result = file_get_contents($url, true, $context);
$jsonData = json_decode($result, true);

if (sizeOf($jsonData) > 0) {
    $data = "<div><input id='check-all' type='checkbox'><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='$key-$contB'>Marcar todos</label></div>";
    $contD = 0;
    foreach ($jsonData as $key => $value) {
        $contJust = 0;
        $key = $value['produto']['descricao'];
        $cod = $value['produto']['codigo'];
        $data .= "<ul style='display: inline-flex;'><input type='checkbox' name='users[]' class='sub-values' id='ul-$contD' pos='$cod' value='";
        foreach ($value['usuarios'] as $keys => $values) {
            $data .= ";" . $keys;
            $contJust++;
        }
        $data .= "'><div style='width:80px;height:80px;text-align: center;'><img src='" . $value['produto']['url'] . "' style='height:50px;'></div><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='ul-$contD'>$key</label><span class='$cod' style='margin-left:10px; font-size:15px;'>$contJust Cliente$s</span></ul>";
        $contD++;
    }
}

$url = $rotas['api'] . 'analisedados/list/listfromcorporateproductusers';

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst']
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$result = file_get_contents($url, true, $context);
$jsonData = json_decode($result, true);

//echo json_encode($jsonData);

if ($jsonData['statusCode'] != 400) {
    $dataEst = "<div><input id='check-all-meu' type='checkbox'><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='$key-$contB'>Marcar todos</label></div>";
    $contD = 0;
    foreach ($jsonData as $key => $value) {
        $contJust = 0;
        $key = $value['produto']['descricao'];

        if($key == null)
            $key = $value['produto']['nomeProduto'];

        $cod = $value['produto']['codigo'];
        $dataEst .= "<ul style='display: inline-flex;'><input type='checkbox' name='users[]' class='sub-values' id='ul-$contD-$contD' pos='" . $contD . "-" . $cod . "' style='margin-top:5px;' value='";
        foreach ($value['usuarios'] as $keys => $values) {
            $dataEst .= ";" . $keys;
            $contJust++;
        }
        $dataEst .= "'><div style='width:80px;height:80px;text-align: center;'><img src='" . $value['produto']['url'] . "' style='height:50px;'></div><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='ul-$contD-$contD'>$key</label><span class='" . $contD . "-" . $cod . "' style='margin-left:10px; font-size:15px;'>$contJust Cliente$s</span></ul>";
        $contD++;
    }
}
?>

<body id="target">
    <div class="site-section bg-light" id="planilha-section">
        <div class="col-12 text-center">
            <h2 class="section-title mb-3" style="margin-top:5px; height: 1.7em;">Gerenciar notificações por produtos</h2>
        </div>
        <form method="post" enctype="multipart/form-data" action="notificacoes-produtos.php">
            <div class="col" style="display: inline-flex;">
                <div class="col-7">
                    <fieldset id="group1" style='margin-bottom:5px;'>
                        <label style='margin-right:3px; font-weight: 700; font-size: 16px;' for='1'>Todos</label>
                        <input class="radio-tipo-not" type='radio' id='1' name='group1' value='0' style='margin-right:14px' checked>
                        <label style='margin-right:3px; font-weight: 700; font-size: 16px;' for='2'>Somente o meu estabelecimento</label>
                        <input class="radio-tipo-not" type='radio' id='2' name='group1' value='1'>
                    </fieldset>
                    <div id="todos" style="display: grid;"><?php echo $data ?></div>
                    <div id="meu-estabelecimento" style="display: none; display: grid;"><?php echo $dataEst ?></div>
                </div>
                <div class="col-5">
                    <input type='text' name='titulo' class='form-control' placeholder='Título' maxlength="35" required>
                    <textarea class="form-control" rows="10" placeholder='Mensagem' name='texto' required></textarea>
                    <div id="qtd-todos"><span style='font-size:16px;'><span id="qtd">0</span> clientes irão receber essa notificação.</span></div>
                    <div id="qtd-meuEst" style="display: none;"><span style='font-size:16px;'><span id="qtd">0</span> clientes irão receber essa notificação.</span></div>
                    <button type="submit" name="submit" class="btn btn-submit btn-danger">Enviar notificação</button>
                </div>
            </div>
        </form>
    </div>
    <?php include("../footer.php"); ?>
    <script>
        $(document).on('click', '#check-all', function() {
            if ($(this).prop('checked')) {
                $('#todos input[type="checkbox"]').prop('checked', true);
            } else {
                $('#todos input[type="checkbox"]').prop('checked', false);
            }
        });

        $(document).on('click', '#check-all-meu', function() {
            if ($(this).prop('checked')) {
                $('#meu-estabelecimento input[type="checkbox"]').prop('checked', true);
            } else {
                $('#meu-estabelecimento input[type="checkbox"]').prop('checked', false);
            }
        });

        $(document).on('change', '.radio-tipo-not', function() {
            if ($(this).val() == 1) {
                $('#meu-estabelecimento').show();
                $("#meu-estabelecimento input[type='checkbox']").prop("disabled", false);
                $('#qtd-meuEst').show();
                $('#todos').hide();
                $("#todos input[type='checkbox']").prop("disabled", true);
                $('#qtd-todos').hide();
                $('#qtd-meuEst').show();
            } else {
                $('#meu-estabelecimento').hide();
                $("#meu-estabelecimento input[type='checkbox']").prop("disabled", true);
                $('#qtd-meuEst').hide();
                $('#todos').show();
                $("#todos input[type='checkbox']").prop("disabled", false);
                $('#qtd-todos').show();
                $('#qtd-meuEst').hide();
            }
        });

        $("form").submit(function(event) {
            if (!$('input[type="checkbox"]').is(':checked')) {
                alert("Selecine pelomenos uma cidade");
                event.preventDefault();
            }
        });

        var qtd;
        $(document).on('change', '#todos input[type="checkbox"]', function() {
            $('#qtd-todos #qtd').text(0);
            $("#todos .sub-values").each(function(i) {
                if (this.checked) {
                    qtd = parseInt($('#qtd-todos #qtd').text());
                    qtd += parseInt($('.' + $(this).attr('pos')).text().split(" ")[0]);
                    $('#qtd-todos #qtd').text(qtd);
                }
            });
        });

        var qtdEst;
        $(document).on('change', '#meu-estabelecimento input[type="checkbox"]', function() {
            $('#qtd-meuEst #qtd').text(0);
            $("#meu-estabelecimento .sub-values").each(function(i) {
                if (this.checked) {
                    qtdEst = parseInt($('#qtd-meuEst #qtd').text());
                    qtdEst += parseInt($('.' + $(this).attr('pos')).text().split(" ")[0]);
                    $('#qtd-meuEst #qtd').text(qtdEst);
                }
            });
        });
    </script>