<?php
include("../header.php");
include("../rotas.php");
include("../getEstado.php");

if (isset($_POST['submit'])) {
    $postUid = $_POST['users'];

    $postTitulo = trim(htmlspecialchars($_POST['titulo'], ENT_QUOTES));
    $postTexto = trim(htmlspecialchars($_POST['texto'], ENT_QUOTES));

    $users = array();
    foreach ($postUid as $value) {
        $exploded = explode(";", $value);
        foreach ($exploded as $i => $key) {
            if ($i != 0) {
                $users[] = $key;
            }
        }
    }

    $globalData[] = array(
        "users" => $users,
        "titulo" => $postTitulo,
        "texto" => $postTexto
    );

    $url = $rotas['api'] . 'app/notificacoes/userlist';

    $ch = curl_init($url);

    $jsonData = [
        "nomeEstabelecimento" => $globalData['nameUser'],
        "codigoEstabelecimento" => $globalData['codEst'],
        "estado" => $estado,
        "data" => $data
    ];

    //echo json_encode($jsonData);

    $options = array(
        'http' => array(
            'method'  => 'POST',
            'content' => json_encode($jsonData),
            'header' =>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
        )
    );

    $context  = stream_context_create($options);
    $result = file_get_contents($url, true, $context);
}

$url = $rotas['api'] . 'analisedados/list/listfromneighborhood';

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst'],
    "uf" => $estado
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

//echo json_encode($jsonData);

$context  = stream_context_create($options);
$result = file_get_contents($url, true, $context);
$jsonData = json_decode($result, true);

if ($jsonData['statusCode'] != 400 && $estado != null) {
    $data = "<input id='check-all' type='checkbox'><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='$key-$contB'>Marcar todos</label>";
    $contD = 0;
    foreach ($jsonData as $key => $value) {
        $tmpData = '';
        $contAll = 0;
        $contB = 0;
        foreach ($value as $keys => $values) {
            $contJust = 0;
            $tmpData .= "<ul><input type='checkbox' name='users[]' id='$key-$contB' pos='".$contD."-".substr($key, 0, 3)."-".$contB."' class='sub-values ref-$contD' value='";
            foreach ($values as $keyss => $valuess) {
                $tmpData .= ";" . $keyss;
                $contAll++;
                $contJust++;
            }
            ($contJust < 2) ? $s = '' : $s = 's';
            $tmpData .= "'><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='$key-$contB'>$keys</label><span class='".$contD."-".substr($key, 0, 3)."-".$contB."' style='margin-left:10px; font-size:15px;'>$contJust Cliente$s</span></ul>";
            $contB++;
        }
        $tmpData .= "</div></ul>";
        ($contAll < 2) ? $s = '' : $s = 's';
        $tmpData = substr_replace($tmpData, "<ul><span class='more-content' pos='$contD' style='margin-right: 7px; cursor: pointer; font-weight: 700; font-size: 16px;'>+</span><input type='checkbox' class='check-root' ref='$contD' id='ul-$contD'><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='ul-$contD'>$key</label><span style='margin-left:10px; font-size:15px;'>$contAll Cliente$s</span><div id='content-$contD' style='margin-left:25px; display:none'>", $pos, 0);
        $data .= $tmpData;
        $contD++;
    }
}

$url = $rotas['api'] . 'analisedados/list/listfromcorporateneighborhood';

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst'],
    "uf" => $estado
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$result = file_get_contents($url, true, $context);
$jsonData = json_decode($result, true);

if ($jsonData['statusCode'] != 400 && $estado != null) {
    $dataEst = "<input id='check-all-meu' type='checkbox'><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='$key-$contB'>Marcar todos</label>";
    $contD = 0;
    foreach ($jsonData as $key => $value) {
        $tmpData = '';
        $contAll = 0;
        $contB = 0;
        foreach ($value as $keys => $values) {
            $contJust = 0;
            $tmpData .= "<ul><input type='checkbox' name='users[]' id='$key-$contB-$contB' pos='".$contD."-".substr($key, 0, 3)."-".$contB."' class='sub-values ref-$contD-$contD' value='";
            foreach ($values as $keyss => $valuess) {
                $tmpData .= ";" . $keyss;
                $contAll++;
                $contJust++;
            }
            ($contJust < 2) ? $s = '' : $s = 's';
            $tmpData .= "'><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='$key-$contB-$contB'>$keys</label><span class='".$contD."-".substr($key, 0, 3)."-".$contB."' style='margin-left:10px; font-size:15px;'>$contJust Cliente$s</span></ul>";
            $contB++;
        }
        $tmpData .= "</div></ul>";
        ($contAll < 2) ? $s = '' : $s = 's';
        $tmpData = substr_replace($tmpData, "<ul><span class='more-content' pos='$contD-$contD' style='margin-right: 7px; cursor: pointer; font-weight: 700; font-size: 16px;'>+</span><input type='checkbox' class='check-root' ref='$contD-$contD' id='ul-$contD-$contD'><label style='margin-left: 10px; cursor: pointer; font-weight: 700; font-size: 16px;' for='ul-$contD-$contD'>$key</label><span style='margin-left:10px; font-size:15px;'>$contAll Cliente$s</span><div id='content-$contD-$contD' style='margin-left:25px; display:none'>", $pos, 0);
        $dataEst .= $tmpData;
        $contD++;
    }
}
?>

<body id="target">
    <div class="site-section bg-light" id="planilha-section">
        <div class="container">
            <div class="col-12 text-center">
                <h2 class="section-title mb-3" style="margin-top:5px; height: 1.7em;">Gerenciar notificações por usuários</h2>
            </div>
            <form method="post" enctype="multipart/form-data" action="notificacoes.php">
                <div class="col" style="display: inline-flex;">
                    <div class="col">
                        <fieldset id="group1" style='margin-bottom:5px;'>
                            <label style='margin-right:3px; font-weight: 700; font-size: 16px;' for='1'>Todos</label>
                            <input class="radio-tipo-not" type='radio' id='1' name='group1' value='0' style='margin-right:14px' checked>
                            <label style='margin-right:3px; font-weight: 700; font-size: 16px;' for='2'>Somente o meu estabelecimento</label>
                            <input class="radio-tipo-not" type='radio' id='2' name='group1' value='1'>
                        </fieldset>
                        <div id="todos"><?php echo $data ?></div>
                        <div id="meu-estabelecimento" style="display: none;"><?php echo $dataEst ?></div>
                    </div>
                    <div class="col">
                        <input type='text' name='titulo' class='form-control' placeholder='Título' maxlength="35" required>
                        <textarea class="form-control" rows="10" placeholder='Mensagem' name='texto' required></textarea>
                        <div id="qtd-todos"><span style='font-size:16px;'><span id="qtd">0</span> clientes irão receber essa notificação.</span></div>
                        <div id="qtd-meuEst" style="display: none;"><span style='font-size:16px;'><span id="qtd">0</span> clientes irão receber essa notificação.</span></div>
                        <button type="submit" name="submit" class="btn btn-submit btn-danger">Enviar notificação</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php include("../footer.php"); ?>
    <script>
        $(document).on('click', '.more-content', function() {
            pos = $(this).attr('pos');
            $('#content-' + pos).toggle();
        });

        $(document).on('click', '.check-root', function() {
            ref = $(this).attr('ref');
            if ($(this).prop('checked')) {
                $('.ref-' + ref).prop('checked', true);
            } else {
                $('.ref-' + ref).prop('checked', false);
            }
        });

        $(document).on('click', '#check-all', function() {
            if ($(this).prop('checked')) {
                $('#todos input[type="checkbox"]').prop('checked', true);
            } else {
                $('#todos input[type="checkbox"]').prop('checked', false);
            }
        });

        $(document).on('click', '#check-all-meu', function() {
            if ($(this).prop('checked')) {
                $('#meu-estabelecimento input[type="checkbox"]').prop('checked', true);
            } else {
                $('#meu-estabelecimento input[type="checkbox"]').prop('checked', false);
            }
        });

        $(document).on('change', '.radio-tipo-not', function() {
            if ($(this).val() == 1) {
                $('#meu-estabelecimento').show();
                $("#meu-estabelecimento input[type='checkbox']").prop("disabled", false);
                $('#qtd-meuEst').show();
                $('#todos').hide();
                $("#todos input[type='checkbox']").prop("disabled", true);
                $('#qtd-todos').hide();
                $('#qtd-meuEst').show();
            } else {
                $('#meu-estabelecimento').hide();
                $("#meu-estabelecimento input[type='checkbox']").prop("disabled", true);
                $('#qtd-meuEst').hide();
                $('#todos').show();
                $("#todos input[type='checkbox']").prop("disabled", false);
                $('#qtd-todos').show();
                $('#qtd-meuEst').hide();
            }
        });

        $("form").submit(function(event) {
            if (!$('input[type="checkbox"]').is(':checked')) {
                alert("Selecine pelomenos uma cidade");
                event.preventDefault();
            }
        });

        var qtd;
        $(document).on('change', '#todos input[type="checkbox"]', function() {
            $('#qtd-todos #qtd').text(0);
            $("#todos .sub-values").each(function(i) {
                if (this.checked) {
                    qtd = parseInt($('#qtd-todos #qtd').text());                            
                    qtd += parseInt($('.'+$(this).attr('pos')).text().split(" ")[0]);
                    $('#qtd-todos #qtd').text(qtd);
                }
            });
        });

        var qtdEst;
        $(document).on('change', '#meu-estabelecimento input[type="checkbox"]', function() {
            $('#qtd-meuEst #qtd').text(0);
            $("#meu-estabelecimento .sub-values").each(function(i) {                
                if (this.checked) {                    
                    qtdEst = parseInt($('#qtd-meuEst #qtd').text());                            
                    qtdEst += parseInt($('.'+$(this).attr('pos')).text().split(" ")[0]);
                    $('#qtd-meuEst #qtd').text(qtdEst);
                }
            });
        });
    </script>