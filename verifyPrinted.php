<?php
include_once('rotas.php');
include_once('variables.php');

$rota = 'sauros/vendas/listprinted';
if ($globalData['tipoEstabelecimento'] != "Supermercados")
    $rota = 'sauros/vendasfood/listprinted';

//API Url
$url = $rotas['api'] . $rota;

$jsonData = [
    "tipoEstabelecimento" => $globalData['tipoEstabelecimento'],
    "cnpjEstabelecimento" => $globalData['cnpjUser'],
    "idToken" => "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRjMGMzNWZlYjBjODIzYjQyNzdkZDBhYjIwNDQzMDY5ZGYzMGZkZWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbm9tZXJjYWRvYXBwIiwiYXVkIjoibm9tZXJjYWRvYXBwIiwiYXV0aF90aW1lIjoxNTg2NDU3NTAxLCJ1c2VyX2lkIjoieXNFQ1ZLZ1Mwb2EzUWpCTWdzWnpoTGRKZ1BqMSIsInN1YiI6InlzRUNWS2dTMG9hM1FqQk1nc1p6aExkSmdQajEiLCJpYXQiOjE1ODY0NTc1MDEsImV4cCI6MTU4NjQ2MTEwMSwiZW1haWwiOiJjYW1wdXNAbm9tZXJjYWRvLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJjYW1wdXNAbm9tZXJjYWRvLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.ume5EnxRYoyt31dkxPmgQHl5Sdf77yF-KJtaQMazrEohFBMZNFmZLrYJEYmG0HZRXuH6aba39VCY-dR2pkhD5AHqf10NMhIrZl6yOP2KlCCc9tYkpn7wsXYkHT0pz37lMnZl4AgWkYr1sjUF0uP2mJJiGcpcurUCqaDHOgP1Q7_4gjdCGF85dyeIvpIwXbeKbK7qMOpGx73ygT25BrY_qOLZldu-gH-g8nkV-GeIIM1Np4gs-iSuozj7-aX-zz69YGBRqsqyPFt4_kwschC7YZJqgCK3LZnJ7-T0MtO39EBTggQjnD-2ZX3WEmP7aRe4lILusM79Bs2DECADSWClSQ",
    "status" => "all",
    "date" => strval(date("d/m/Y"))
];

/*$jsonData = [
	"tipoEstabelecimento" => "Supermercados",
	"cnpjEstabelecimento" => "40035012000176",
    "idToken" => "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRjMGMzNWZlYjBjODIzYjQyNzdkZDBhYjIwNDQzMDY5ZGYzMGZkZWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbm9tZXJjYWRvYXBwIiwiYXVkIjoibm9tZXJjYWRvYXBwIiwiYXV0aF90aW1lIjoxNTg2NDU3NTAxLCJ1c2VyX2lkIjoieXNFQ1ZLZ1Mwb2EzUWpCTWdzWnpoTGRKZ1BqMSIsInN1YiI6InlzRUNWS2dTMG9hM1FqQk1nc1p6aExkSmdQajEiLCJpYXQiOjE1ODY0NTc1MDEsImV4cCI6MTU4NjQ2MTEwMSwiZW1haWwiOiJjYW1wdXNAbm9tZXJjYWRvLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJjYW1wdXNAbm9tZXJjYWRvLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.ume5EnxRYoyt31dkxPmgQHl5Sdf77yF-KJtaQMazrEohFBMZNFmZLrYJEYmG0HZRXuH6aba39VCY-dR2pkhD5AHqf10NMhIrZl6yOP2KlCCc9tYkpn7wsXYkHT0pz37lMnZl4AgWkYr1sjUF0uP2mJJiGcpcurUCqaDHOgP1Q7_4gjdCGF85dyeIvpIwXbeKbK7qMOpGx73ygT25BrY_qOLZldu-gH-g8nkV-GeIIM1Np4gs-iSuozj7-aX-zz69YGBRqsqyPFt4_kwschC7YZJqgCK3LZnJ7-T0MtO39EBTggQjnD-2ZX3WEmP7aRe4lILusM79Bs2DECADSWClSQ",
    "status" => "all",
    "date" => "21/01/2021"
];*/

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

//echo json_encode($jsonData);

$context  = stream_context_create($options);
$resultList = file_get_contents($url, true, $context);

$jsonDataList = json_decode($resultList, true);

//echo json_encode($jsonDataList);

$data = '';

if (sizeOf($jsonDataList) > 0 && !isset($jsonDataList['statusCode'])) {

    if ($globalData['tipoEstabelecimento'] == "Supermercados") {
        $vendas = null;
        $count = 0;
        $data = "<div class='center-div-abs nova-venda' id='nova-venda' style='width:80%;background:#ffffff; z-index:999999999999999; margin-top:2%; height: 100%; overflow: auto;'>
        <div style='width:100%;height:50px;box-shadow: 0 5px 6px -2px #cecece;padding-left: 10px; display:inline-flex; padding-top:10px;'>
            <h1>Novas vendas</h1>
        </div>";
        foreach ($jsonDataList as $key => $value) {
            if (explode('T', $value['dataHoraInsert'])[1] > date("H:i:s")) {
                $count++;
                $vendas = "<div class='venda' val='" . $value['codigoTransacao'] . "'>
                            <div class='text-center' style='width:100%;height:40px;box-shadow: 0 5px 6px -2px #cecece;'>
                                <h2 class='vertical-center-rel title'>" . $value['codigoTransacao'] . "</h2>
                            </div>
                            <table class='table' style='font-size:15px;'>
                                <tbody>
                                    <tr><th scope='row'>Previsão de entrega</th> 
                                    <td>" . date('d/m/Y H:i:s', strtotime($value['dataPrevistaEntrega'])) . "</td></tr>
                                    <tr> <th scope='row'>Nome do cliente</th> 
                                    <td>" . $value['cliente']['nomeUsuario'] . " - " . $value['cliente']['telefone'] . "</td></tr> 
                                    <tr> <th scope='row'>Endereço</th> 
                                    <td>" . $value['cliente']['rua'] . ", " . $value['cliente']['numero'] . " - " . $value['cliente']['bairro'] . " " . $value['cliente']['complemento'] . " " . $value['cliente']['cidade'] . " - " . $value['cliente']['estado'] . " - " . $value['cliente']['cep'] . "</td></tr>
                                    <tr><th scope='row'>Data da compra</th>                                 
                                    <td>" . date('d/m/Y H:i:s', strtotime($value['dataHoraInsert'])) . "</td></tr>
                                    <tr><th scope='row'>Forma de pagamento</th> 
                                    <td>" . $value['pagamento']['descPag'] . "</td></tr>";
                if (isset($value['compraEstabelecimento']['cupom']['cupom'])) {
                    $condicoes = "";
                    if ($value['compraEstabelecimento']['cupom']['freteGratis'] == "sim") {
                        $condicoes .= " - Ganhou frete grátis";
                    }
                    if ($value['compraEstabelecimento']['cupom']['valorDesconto'] != "0") {
                        if ($condicoes == "")
                            $condicoes .= " - Ganhou " . $value['compraEstabelecimento']['cupom']['valorDesconto'] . "% de desconto";
                        else
                            $condicoes .= " e " . $value['compraEstabelecimento']['cupom']['valorDesconto'] . "% de desconto";
                    }
                    if ($value['compraEstabelecimento']['cupom']['valorDescontoReal'] != "0") {
                        if ($condicoes == "")
                            $condicoes .= " - Ganhou " . $value['compraEstabelecimento']['cupom']['valorDesconto'] . "% de desconto";
                        else
                            $condicoes .= " e " . $value['compraEstabelecimento']['cupom']['valorDesconto'] . "% de desconto";
                    }
                    $vendas .= "<tr><th scope='row'>Cupom utilizado</th> 
                                        <td>" . $value['compraEstabelecimento']['cupom']['cupom'] . " " . $condicoes . ".</td></tr>";
                }
                $vendas .= "<tr> <th scope='row'>Frete</th> 
                                    <td>" . $value['pagamento']['vFrete'] . " Reais</td></tr> 
                                    <tr> <th scope='row'>Valor sem o frete</th> 
                                    <td>" . $value['pagamento']['vProds'] . " Reais</td></tr> 
                                    <tr> <th scope='row'>Valor total</th> 
                                    <td>" . $value['pagamento']['vPago'] . " Reais</td></tr> 
                                    <tr><th scope='row'>Produtos</th>
                                    <td></td></tr></tbody>
                                    </table>";
                foreach ($value['compraEstabelecimento']['carrinho'] as $values) {
                    $vendas .= "<div>
                                        <table class='table' style='font-size:14px;'><tbody>
                                        <tr><h4 style='margin: 0 0 5px 10px'>" . $values['quantidade'] . 'x ' . $values['produto'] . ' ' . $values['marca'] . ' ' . $values['caracteristica'] . ' ' . $values['medida'] . " [" . $values['codigo'] . "] - R$ " . number_format($values['valorTotal'], 2, '.', '') .  "</h4></tr>
                                        <tr><th scope='row' style='width:18%'>Valor Unitário</th>
                                        <td>" . number_format($values['valorUnitario'], 2, '.', '') . " Reais</td></tr>
                                        </tbody></table>                              
                                        </div>";
                }
                $vendas .= "<script> var audio = new Audio('../music/notificacao-venda.mp3'); audio.play();</script>
                        </div>";
                $data .= $vendas;
            }
        }
        $data .= "</div>";

        if ($vendas != null) {
            echo $data;
        }
    } else {

        $vendas = null;
        $count = 0;
        $data = "<div class='center-div-abs nova-venda' id='nova-venda' style='width:80%;background:#ffffff; z-index:999999999999999; margin-top:2%; height: 100%; overflow: auto;'>
        <div style='width:100%;height:50px;box-shadow: 0 5px 6px -2px #cecece;padding-left: 10px; display:inline-flex; padding-top:10px;'>
            <h1>Novas vendas</h1>
        </div>";
        foreach ($jsonDataList as $key => $value) {
            if (explode('T', $value['dataHoraInsert'])[1] > date("H:i:s")) {
                $count++;
                $vendas = "<div class='venda' val='" . $value['codigoTransacao'] . "'>
                            <div class='text-center' style='width:100%;height:40px;box-shadow: 0 5px 6px -2px #cecece;'>
                                <h2 class='vertical-center-rel title'>" . $value['codigoTransacao'] . "</h2>
                            </div>
                            <table class='table' style='font-size:15px;'>
                                <tbody>
                                    <tr><th scope='row'>Previsão de entrega</th> 
                                    <td>" . date('d/m/Y H:i:s', strtotime($value['dataPrevistaEntrega'])) . "</td></tr>
                                    <tr> <th scope='row'>Nome do cliente</th> 
                                    <td>" . $value['cliente']['nomeUsuario'] . " - " . $value['cliente']['telefone'] . "</td></tr> 
                                    <tr> <th scope='row'>Endereço</th> 
                                    <td>" . $value['cliente']['rua'] . ", " . $value['cliente']['numero'] . " - " . $value['cliente']['bairro'] . " " . $value['cliente']['complemento'] . " " . $value['cliente']['cidade'] . " - " . $value['cliente']['estado'] . " - " . $value['cliente']['cep'] . "</td></tr>
                                    <tr><th scope='row'>Data da compra</th>                                 
                                    <td>" . date('d/m/Y H:i:s', strtotime($value['dataHoraInsert'])) . "</td></tr>
                                    <tr><th scope='row'>Forma de pagamento</th> 
                                    <td>" . $value['pagamento']['descPag'] . "</td></tr>";
                if ($value['pagamento']['cupom'] != "") {
                    $condicoes = "";
                    if ($value['pagamento']['cupomFreteGratis'] == "sim") {
                        $condicoes .= " - Ganhou frete grátis";
                    }
                    if ($value['pagamento']['cupomDescontoPorcentagem'] != "0") {
                        $condicoes .= " e " . $value['pagamento']['cupomDescontoPorcentagem'] . "% de desconto";
                    }
                    if ($value['pagamento']['cupomDescontoReal'] != "0") {
                        $condicoes .= " e " . $value['pagamento']['cupomDescontoReal'] . " de desconto";
                    }
                    $vendas .= "<tr><th scope='row'>Cupom utilizado</th> 
                                        <td>" . $value['pagamento']['cupom'] . " " . $condicoes . ".</td></tr>";
                }
                $vendas .= "<tr> <th scope='row'>Frete</th> 
                                    <td>" . $value['pagamento']['vFrete'] . " Reais</td></tr> 
                                    <tr> <th scope='row'>Valor sem o frete</th> 
                                    <td>" . $value['pagamento']['vProds'] . " Reais</td></tr>                                    
                                    <tr><th scope='row'>Produtos</th>
                                    <td></td></tr></tbody>
                                    </table>";
                foreach ($value['carrinhoList'] as $keys => $values) {
                    $vendas .= "<div>
                                        <table class='table' style='font-size:14px;'><tbody>
                                        <tr><h4 style='margin: 0 0 5px 10px'>" . number_format($values['quantidade']) . 'x ' . $values['produto'][0]['nomeProduto'] . " [" . $values['produto'][0]['codigoBarras'] . "] - R$ " . number_format($values['produto'][0]['valorInicial'], 2, '.', '') .  "</h4></tr>";
                    if (isset($values['ingredientesEscolhidos'])) {
                        /*$first = true;                                             
                                            foreach($values['ingredientesEscolhidos'] as $keyss => $valuess){
                                                if($valuess['quantidade'] > 0){
                                                    if($first){
                                                        $vendas .= "<tr><th scope='row' >Ingredientes</th><td>".$valuess['quantidade']."x ".$valuess['nomeProduto']." - R$" . number_format($valuess['valorInicial'], 2, '.', '') . " Reais</td></tr>";
                                                        $first = false;
                                                    }else{
                                                        $vendas .= "<tr><th scope='row' style='font-weight: 100;'></th><td>".$valuess['quantidade']."x ".$valuess['nomeProduto']." - R$" . number_format($valuess['valorInicial'], 2, '.', '') . " Reais</td></tr>";
                                                    }
                                                }
                                            }*/
                        $vendas .= "<tr><th scope='row' >Ingredientes</th><td></td></tr>";
                        foreach ($values['ingredientesEscolhidos'] as $keyss => $valuess) {
                            if ($valuess['quantidade'] > 0) {
                                $vendas .= "<tr><th scope='row' style='font-weight: 100;'></th><td>" . $valuess['quantidade'] . "x " . $valuess['nomeProduto'] . " - R$" . number_format($valuess['valorInicial'], 2, '.', '') . " Reais</td></tr>";
                            }
                        }
                    }
                    if (isset($values['adicionais'])) {
                        /*$first = true; 
                                            foreach($values['adicionais'] as $keyss => $valuess){
                                                if($first){
                                                    $vendas .= "<tr><th scope='row' >Adicionais</th><td>".$valuess['quantidade']."x ".$valuess['nomeProduto']." - R$" . number_format($valuess['valorInicial'], 2, '.', '') . " Reais</td></tr>";
                                                    $first = false;
                                                }else{
                                                    $vendas .= "<tr><th scope='row' style='font-weight: 100;'></th><td>".$valuess['quantidade']."x ".$valuess['nomeProduto']." - R$" . number_format($valuess['valorInicial'], 2, '.', '') . " Reais</td></tr>";
                                                }
                                            }*/
                        $vendas .= "<tr><th scope='row' >Adicionais</th><td></td></tr>";
                        foreach ($values['adicionais'] as $keyss => $valuess) {
                            if ($valuess['quantidade'] > 0 && $valuess['quantidade'] != null) {
                                $quantidade = $valuess['quantidade'] . "x ";
                            } else {
                                $quantidade = "1 x ";
                            }
                            $vendas .= "<tr><th scope='row' style='font-weight: 100;'></th><td>" . $quantidade . $valuess['nomeProduto'] . " - R$" . number_format($valuess['valorInicial'], 2, '.', '') . " Reais</td></tr>";
                        }
                    }
                    if (isset($values['notasAdicionais']) && $values['notasAdicionais'] != "") {
                        $vendas .= "<tr><th scope='row'>Comentário do cliente</th><td>" . $values['notasAdicionais'] . "</td></tr>";
                    }
                    $vendas .= "<tr><th scope='row' style='width:20%'>Valor Total</th>
                                        <td>" . number_format($values['valorFinal'], 2, '.', '') . " Reais</td></tr>
                                        </tbody></table>                              
                                        </div>";
                }
                $vendas .= "<table class='table' style='font-size:15px; margin-top:5px;'><tbody><tr> <th scope='row' style='width:20%;'>Valor total da compra</th> <td>" . $value['compraEstabelecimento']['valorCompra'] . " Reais</td></tr>";
                if (isset($value['compraEstabelecimento']['valorPagoDinheiro'])) {
                    $vendas .= "<tr> <th scope='row' style='width:20%;'>Valor pago em dinheiro</th> <td>" . number_format((float)$value['compraEstabelecimento']['valorPagoDinheiro'], 2, '.', '') . " Reais</td></tr>";
                    $vendas .= "<tr> <th scope='row' style='width:20%;'>Troco</th> <td>" . number_format((float)$value['compraEstabelecimento']['troco'], 2, '.', '') . " Reais</td></tr>";
                }
                $vendas .= "</tbody></table> <script> var audio = new Audio('../music/notificacao-venda.mp3'); audio.play();</script>
                        </div>";

                $data .= $vendas;
            }
        }
        $data .= "</div>";

        if ($vendas != null) {
            echo $data;
        }
    }
}
