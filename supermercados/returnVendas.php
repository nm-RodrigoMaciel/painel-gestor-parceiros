<?php
/*if(isset($_POST['count'])){
	$postCount = $_POST['count'];	
}else{
	$postCount = 0;	
}*/

$postLastDays = $_POST['lastDays'];

include_once('../rotas.php');
include_once('../foods/functions.php');
include_once('../variables.php');

//API Url
$url = $rotas['api'] . 'paginadas/vendas/listagem';

$jsonData = [
	"tipoEstabelecimento" => $globalData['tipoEstabelecimento'],
	"cnpjEstabelecimento" => $globalData['cnpjUser'],
	"idToken" => "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRjMGMzNWZlYjBjODIzYjQyNzdkZDBhYjIwNDQzMDY5ZGYzMGZkZWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbm9tZXJjYWRvYXBwIiwiYXVkIjoibm9tZXJjYWRvYXBwIiwiYXV0aF90aW1lIjoxNTg2NDU3NTAxLCJ1c2VyX2lkIjoieXNFQ1ZLZ1Mwb2EzUWpCTWdzWnpoTGRKZ1BqMSIsInN1YiI6InlzRUNWS2dTMG9hM1FqQk1nc1p6aExkSmdQajEiLCJpYXQiOjE1ODY0NTc1MDEsImV4cCI6MTU4NjQ2MTEwMSwiZW1haWwiOiJjYW1wdXNAbm9tZXJjYWRvLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJjYW1wdXNAbm9tZXJjYWRvLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.ume5EnxRYoyt31dkxPmgQHl5Sdf77yF-KJtaQMazrEohFBMZNFmZLrYJEYmG0HZRXuH6aba39VCY-dR2pkhD5AHqf10NMhIrZl6yOP2KlCCc9tYkpn7wsXYkHT0pz37lMnZl4AgWkYr1sjUF0uP2mJJiGcpcurUCqaDHOgP1Q7_4gjdCGF85dyeIvpIwXbeKbK7qMOpGx73ygT25BrY_qOLZldu-gH-g8nkV-GeIIM1Np4gs-iSuozj7-aX-zz69YGBRqsqyPFt4_kwschC7YZJqgCK3LZnJ7-T0MtO39EBTggQjnD-2ZX3WEmP7aRe4lILusM79Bs2DECADSWClSQ",
	"status" => "all",
	//"lastIndex" => 0,
	"itemsPerPage" => 0,
	"date" => date('d/m/Y', strtotime('-'.$postLastDays.' days'))
];

$options = array(
	'http' => array(
		'method'  => 'POST',
		'content' => json_encode($jsonData),
		'header' =>  "Content-Type: application/json\r\n" .
			"Accept: application/json\r\n"
	)
);

//echo json_encode($jsonData);

$context  = stream_context_create($options);
$resultList = file_get_contents($url, true, $context);

$jsonDataList = json_decode($resultList, true);

//echo json_encode($jsonDataList);

$cont_drop = 0;
$arrayListasNomes = "";

if (sizeOf($jsonDataList) > 0) {
	foreach ($jsonDataList as $value) {
			$status = -1;
			$aprovadoTempo = "";
			$separacaoTempo = "";
			$transporteTempo = "";
			$finalizadoTempo = "";

			switch (sizeof($value['status']) - 1) {
				case 0:
					$status = "pago";
					$proxStatus = "Em Separação";
					$aprovadoTempo = explode("T", $value['status'][0]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][0]['dataHoraInsert'])[1];
					break;
				case 1:
					$status = "separacao";
					$proxStatus = "Em Transporte";
					$aprovadoTempo = explode("T", $value['status'][0]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][0]['dataHoraInsert'])[1];
					$separacaoTempo = explode("T", $value['status'][1]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][1]['dataHoraInsert'])[1];
					break;
				case 2:
					$status = "transporte";
					$proxStatus = "Pedido Entregue";
					$aprovadoTempo = explode("T", $value['status'][0]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][0]['dataHoraInsert'])[1];
					$separacaoTempo = explode("T", $value['status'][1]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][1]['dataHoraInsert'])[1];
					$transporteTempo = explode("T", $value['status'][2]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][2]['dataHoraInsert'])[1];
					break;
				case 3:
					$status = "finalizado";
					$proxStatus = "Finalizado";
					$aprovadoTempo = explode("T", $value['status'][0]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][0]['dataHoraInsert'])[1];
					$separacaoTempo = explode("T", $value['status'][1]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][1]['dataHoraInsert'])[1];
					$transporteTempo = explode("T", $value['status'][2]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][2]['dataHoraInsert'])[1];
					$finalizadoTempo = explode("T", $value['status'][3]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][3]['dataHoraInsert'])[1];
					break;
			}

			$arrayListasNomes .=
				"<div class='dropdown col-12' style='padding-right: 0px; padding-left: 0px; margin-bottom:10px;'>
			<a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
				<span id='text-listas-adicional'>" . $value['codigoTransacao'] . "</span>
			</a>
			<div id='dropdown' class='dropdown-menu' aria-labelledby='dropdownMenuLink'>
			<div class='col-12 vendas-lista supermercados' ref='$cont_drop' style='margin-bottom:10px;'>
				<table class='table'>
					<tbody>
						<tr>
							<th scope='row'>Forma de pagamento</th>
							<td>" . ucfirst(strtolower($value['pagamento']['descPag'])) . "</td>
						</tr>
						<tr>
							<th scope='row'>Frete</th>
							<td>" . $value['pagamento']['vFrete'] . " Reais</td>
						</tr>	
						<tr>
							<th scope='row'>Valor da compra</th>
							<td>" . $value['pagamento']['vPago'] . " Reais</td>
						</tr>	
						<tr>
							<th scope='row'>Valor sem o frete</th>
							<td>" . $value['pagamento']['vProds'] . " Reais</td>
						</tr>
						<tr>
							<th scope='row'>Nome do cliente</th>
							<td>" . $value['cliente']['nomeUsuario'] . "</td>
						</tr>
						<tr>
							<th scope='row'>Telefone</th>
							<td>" . $value['cliente']['telefone'] . "</td>
						</tr>
						<tr>
							<th scope='row'>Endereço</th>
							<td>" . $value['cliente']['rua'] . " - Número: " . $value['cliente']['numero'] . " Complemento: " . $value['cliente']['complemento'] . " " . $value['cliente']['bairro'] . " - " . $value['cliente']['cidade'] . "</td>
						</tr>																											
						<tr>
							<td><button class='btn btn-danger btn-atualizar-status' value='" . (sizeof($value['status']) - 1) . ";" . $value['codigoTransacao'] . "'>$proxStatus</button></td>
							<td><button class='btn btn-danger btn-contato' value='" . $value['codigoTransacao'] . "'>Entrar em Contato</button></td>
							<td><button class='btn btn-danger btn-ver-produtos' value='$cont_drop'>Ver Produtos</button></td>
						</tr>
					</tbody>
				</table>
				<div class='area-imagem-vendas'>
					<div>
						<img class='imagem-status' src='../images/$status.png'>
					</div>	
					<div>
						<div class='data-aprovado'>
							$aprovadoTempo    	        
						</div>
						<div class='data-separacao'>	
							$separacaoTempo			        		        
						</div>
						<div class='data-transporte'>	
							$transporteTempo			        		        
						</div>
						<div class='data-finalizado'>
							$finalizadoTempo			        			        
						</div>                                                            
					</div>			    
				</div>
				</div>
			</div>
		</div>";

	}

	if (isset($arrayListasNomes)) {
		$html = $arrayListasNomes;

		echo $html;
	} 
}
