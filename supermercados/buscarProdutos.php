<?php
include_once('../rotas.php');
include_once('../foods/functions.php');
include_once('../variables.php');

$arrayPosition = trim(htmlspecialchars($_POST['arrayPosition'], ENT_QUOTES));

$url = $rotas['api'] . 'sauros/vendas/';

$jsonData = [
    "tipoEstabelecimento" => $globalData['tipoEstabelecimento'],
    "cnpjEstabelecimento" => $globalData['cnpjUser'],
    "idToken" => "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRjMGMzNWZlYjBjODIzYjQyNzdkZDBhYjIwNDQzMDY5ZGYzMGZkZWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbm9tZXJjYWRvYXBwIiwiYXVkIjoibm9tZXJjYWRvYXBwIiwiYXV0aF90aW1lIjoxNTg2NDU3NTAxLCJ1c2VyX2lkIjoieXNFQ1ZLZ1Mwb2EzUWpCTWdzWnpoTGRKZ1BqMSIsInN1YiI6InlzRUNWS2dTMG9hM1FqQk1nc1p6aExkSmdQajEiLCJpYXQiOjE1ODY0NTc1MDEsImV4cCI6MTU4NjQ2MTEwMSwiZW1haWwiOiJjYW1wdXNAbm9tZXJjYWRvLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJjYW1wdXNAbm9tZXJjYWRvLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.ume5EnxRYoyt31dkxPmgQHl5Sdf77yF-KJtaQMazrEohFBMZNFmZLrYJEYmG0HZRXuH6aba39VCY-dR2pkhD5AHqf10NMhIrZl6yOP2KlCCc9tYkpn7wsXYkHT0pz37lMnZl4AgWkYr1sjUF0uP2mJJiGcpcurUCqaDHOgP1Q7_4gjdCGF85dyeIvpIwXbeKbK7qMOpGx73ygT25BrY_qOLZldu-gH-g8nkV-GeIIM1Np4gs-iSuozj7-aX-zz69YGBRqsqyPFt4_kwschC7YZJqgCK3LZnJ7-T0MtO39EBTggQjnD-2ZX3WEmP7aRe4lILusM79Bs2DECADSWClSQ",
    "status" => "all",
    "key" => $arrayPosition
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$resultList = file_get_contents($url, true, $context);

$jsonDataList = json_decode($resultList, true);

//echo json_encode($jsonDataList);

$produtos = '';
$values = $jsonDataList['carrinho'];

//<img src='https://i.pinimg.com/originals/c0/ed/55/c0ed55ef7efa3f08a185ce527557c561.png' style='width:40%; padding: 0 13px 13px 13px;'>

foreach ($values as $value) {

    $titulo = $value['codigo'];
    if (isset($value['produto'])) {
        $titulo = $value['produto'] ." ".$value['caracteristica']." ".$value['medida'];
        $subtitulo = "<tr><th scope='row'>Código</th><td>" . $value['codigo'] . "</td></tr>";
    }
    $complemento = "";
    if (isset($value['url'])) {
        $complemento = "<div class='col-4 text-center'><img src='" . $value['url'] . "' style='max-height:175px; padding: 0 13px 13px 13px;'></div>";
    }

    $produtos .= "<div class='content'><h3 style='margin-bottom:12px;'>" . $titulo . "</h3><div style='display:flex'><table class='table text-center' style='font-size:14px;'>    
        <tbody class='col-8'>
            $subtitulo
            <tr>
                <th scope='row'>Valor Unitario</th>
                <td> R$" . number_format($value['valorUnitario'], 2, '.', '') . "</td>
            </tr>
            <tr>
            <th scope='row'>Quantidade</th>
                <td>" . $value['quantidade'] . "</td>
            </tr>
            <tr>
                <th scope='row'>Valor Total</th>
                <td> R$" . number_format($value['valorTotal'], 2, '.', '') . "</td>
            </tr>
        </tbody>  
        $complemento
    </table>
    </div>
    </div>";
}

echo $produtos;
