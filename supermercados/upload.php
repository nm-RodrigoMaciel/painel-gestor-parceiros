<?php

error_reporting (E_ALL ^ E_NOTICE);

session_start();

$cnpj = $globalData['cnpj'];
$tipo = $globalData['typeUser'];
$linha = $globalData['planilhaLinha'];
$colunas = $globalData['planilhaColunas'];

$coluna = explode(';', $colunas);

include 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ColunaCodigoBarra implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($column, $row, $worksheetName = '') {
		global $linha;
		global $coluna;
        if ($row >= $linha) {
            if ($column == $coluna[0]) {
                return true;
            }
        }
        return false;
    }
}
class ColunaEstoque implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($column, $row, $worksheetName = '') {
		global $linha;
		global $coluna;
        if ($row >= $linha) {
            if ($column == $coluna[2]) {
                return true;
            }
        }
        return false;
    }
}
class ColunaValor implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($column, $row, $worksheetName = '') {
		global $linha;
		global $coluna;
        if ($row >= $linha) {
            if ($column == $coluna[3]) {
                return true;
            }
        }
        return false;
    }
}
class ColunaDescricao implements \PhpOffice\PhpSpreadsheet\Reader\IReadFilter
{
    public function readCell($column, $row, $worksheetName = '') {
		global $linha;
		global $coluna;
        if ($row >= $linha) {
            if ($column == $coluna[1]) {
                return true;
            }
        }
        return false;
    }
}

if($_FILES["select_excel"]["name"] != '')
{
	$file_array = explode(".", $_FILES['select_excel']['name']);
	$file_extension = end($file_array);

	if($file_extension == 'xlsx' || $file_extension == 'xls')
	{
		$filterCodBar = new ColunaCodigoBarra();
		$filterEstoque = new ColunaEstoque();
		$filterValor = new ColunaValor();
		$filterDescricao = new ColunaDescricao();
		
		if($file_extension == 'xlsx'){
			$reader = IOFactory::createReader('Xlsx');
		}
		else if($file_extension == 'xls'){
			$reader = IOFactory::createReader('Xls');
		}

		$codBarra = lerCodBarra($reader,$filterCodBar);
		$estoque = lerEstoque($reader,$filterEstoque);
		$valor = lerValor($reader,$filterValor);
		$descricao = lerDescricao($reader,$filterDescricao);
		
		$contArr=0;
		$contPro=0;
		foreach ($valor as $i){
			if(isset($codBarra[$contPro]) || isset($estoque[$contPro]) || isset($valor[$contPro])){
				if(isset($codBarra[$contPro]) && $codBarra[$contPro] != '' && isset($valor[$contPro]) && $valor[$contPro] != ''){
					$valores[$contArr] = array("codigo"=>$codBarra[$contPro], "quantidade"=>$estoque[$contPro], "valor"=>$valor[$contPro]);		
					$contArr++;
					$contPro++;		
				}
				else if($codBarra[$contPro] == '' && $descricao[$contPro] != ''){
					echo '<div class="alert alert-warning">O produto '.$descricao[$contPro].' está sem código de barras.</div>';
					$contPro++;	
				}
				else if($estoque[$contPro] == '' && $descricao[$contPro] != ''){
					echo '<div class="alert alert-warning">O produto '.$descricao[$contPro].' está sem estoque.</div>';
					$contPro++;	
				}
				else if($valor[$contPro] == '' && $descricao[$contPro] != ''){
					echo '<div class="alert alert-warning">O produto '.$descricao[$contPro].' está sem valor.</div>';
					$contPro++;	
				}
			}
			else
			{
				break;
			}
		}
		echo '<div class="alert alert-success">'.$contArr.' cadastros realizados!</div>';
		
		//API Url
		$globalData['url'] = $rotas['api'].'general/produtos/update';
		 
		//Initiate cURL.
		$ch = curl_init($globalData['url']);
		 
		//The JSON data.
		$jsonData = [ "tipoEstabelecimento" => $tipo,
			"cnpjEstabelecimento" => $globalData['cnpjUser'],
			"produtos" => $valores
		];
		 
		//Encode the array into JSON.
		echo $jsonDataEncoded = json_encode($jsonData);
		 
		//Tell cURL that we want to send a POST request.
		curl_setopt($ch, CURLOPT_POST, 1);
		 
		//Attach our encoded JSON string to the POST fields.
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
		 
		//Set the content type to application/json
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
		 
		//Execute the request
		//$result = curl_exec($ch);
		
	}
	else
	{
		echo '<div class="alert alert-warning">Somente .xlsx é permitido.</div>';
	}
}
else
{
	$message = '<div class="alert alert-danger">Primeiro selecione um arquivo.</div>';
}

function lerCodBarra($reader,$filter) {
	$cod = [];
	$reader->setReadFilter($filter);
	$spreadsheet = $reader->load($_FILES['select_excel']['tmp_name']);
	$worksheet = $spreadsheet->getActiveSheet();
	
	$i = 0;
	foreach ($worksheet->getRowIterator() AS $row) {
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(TRUE);
		foreach ($cellIterator as $cell) {		
			$cod[$i] = $cell->getValue();
			$i++;
		}
	}
	return $cod;
}

function lerEstoque($reader,$filter) {
	$estoque = [];
	$reader->setReadFilter($filter);
	$spreadsheet = $reader->load($_FILES['select_excel']['tmp_name']);
	$worksheet = $spreadsheet->getActiveSheet();
	
	$i = 0;
	foreach ($worksheet->getRowIterator() AS $row) {
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(TRUE);
		foreach ($cellIterator as $cell) {		
			$estoque[$i] = $cell->getValue();
			$i++;
		}
	}
	return $estoque;
}

function lerValor($reader,$filter) {
	$valor = [];
	$reader->setReadFilter($filter);
	$spreadsheet = $reader->load($_FILES['select_excel']['tmp_name']);
	$worksheet = $spreadsheet->getActiveSheet();
	
	$i = 0;
	foreach ($worksheet->getRowIterator() AS $row) {
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(TRUE);
		foreach ($cellIterator as $cell) {		
			$valor[$i] = $cell->getValue();
			$i++;
		}
	}	
	return $valor;
}

function lerDescricao($reader,$filter) {
	$descricao = [];
	$reader->setReadFilter($filter);
	$spreadsheet = $reader->load($_FILES['select_excel']['tmp_name']);
	$worksheet = $spreadsheet->getActiveSheet();
	
	$i = 0;
	foreach ($worksheet->getRowIterator() AS $row) {
		$cellIterator = $row->getCellIterator();
		$cellIterator->setIterateOnlyExistingCells(TRUE);
		foreach ($cellIterator as $cell) {		
			$descricao[$i] = $cell->getValue();
			$i++;
		}
	}	
	return $descricao;
}

?>