<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('../foods/functions.php');

if (isset($_POST['submit'])) {
	$codigoBarras = trim(htmlspecialchars($_POST['codigo'], ENT_QUOTES));
	$postValor = trim(htmlspecialchars($_POST['valor'], ENT_QUOTES));
	$postQuantidade = trim(htmlspecialchars($_POST['quantidade'], ENT_QUOTES));
	$postNome = trim(htmlspecialchars($_POST['produto'], ENT_QUOTES));
	$postMarca = trim(htmlspecialchars($_POST['marca'], ENT_QUOTES));
	$postValor = trim(htmlspecialchars($_POST['valor'], ENT_QUOTES));
	$postQuantidade = trim(htmlspecialchars($_POST['quantidade'], ENT_QUOTES));
	$postQuantidadeVendida = trim(htmlspecialchars($_POST['quantidadeVendida'], ENT_QUOTES));
	$postPromocao = trim(htmlspecialchars($_POST['promocao'], ENT_QUOTES));
	$postCaracteristica = trim(htmlspecialchars($_POST['caracteristica'], ENT_QUOTES));
	$postCategoria = trim(htmlspecialchars($_POST['categoria'], ENT_QUOTES));
	$postDescricao = trim(htmlspecialchars($_POST['descricao'], ENT_QUOTES));
	$postMedida = trim(htmlspecialchars($_POST['medida'], ENT_QUOTES));
	$postQuantidadeMargem = trim(htmlspecialchars($_POST['quantidadeMargem'], ENT_QUOTES));
	$postSubCategoria = trim(htmlspecialchars($_POST['subCategoria'], ENT_QUOTES));
	$postTipoProduto = trim(htmlspecialchars($_POST['tipoProduto'], ENT_QUOTES));
	$urlImagem = trim(htmlspecialchars($_POST['url'], ENT_QUOTES));
	$postDisponivel = trim(htmlspecialchars($_POST['disponivel'], ENT_QUOTES));
	$source_file = $_FILES['file'];

	if ($postDisponivel == "on") {
		$disponivel = true;
	} else {
		$disponivel = false;
	}

	$produtos[] = array(
		"codigo" => $codigoBarras,
		"valor" => (float)$postValor,
		"quantidade" => (int)$postQuantidade,
		"produto" => $postNome,
		"marca" => $postMarca,
		"quantidadeVendida" => (int)$postQuantidadeVendida,
		"promocao" => $postPromocao,
		"caracteristica" => $postCaracteristica,
		"categoria" => $postCategoria,
		"descricao" => $postDescricao,
		"medida" => $postMedida,
		"quantidadeMargem" => (int)$postQuantidadeMargem,
		"subCategoria" => $postSubCategoria,
		"tipoProduto" => $postTipoProduto,
		"disponivel" => $disponivel
	);

	$urlAtu = $rotas['api'] . 'sauros/produtos/alterar';
	if ($rotas['apiBase'] != "") {
		$urlBase = $rotas['apiBase'] . 'sauros/produtos/alterar';
	}

	$ch = curl_init($urlAtu);

	$jsonDataAtu = [
		"tipoEstabelecimento" => $globalData['tipoEstabelecimento'],
		"cnpjEstabelecimento" => $globalData['cnpjUser'],
		"produtos" => $produtos
	];

	//echo json_encode($jsonDataAtu);

	$optionsAtu = array(
		'http' => array(
			'method'  => 'POST',
			'content' => json_encode($jsonDataAtu),
			'header' =>  "Content-Type: application/json\r\n" .
				"Accept: application/json\r\n"
		)
	);

	$contextAtu  = stream_context_create($optionsAtu);
	if ($rotas['apiBase'] != "") {
		$resultAtu = file_get_contents($urlBase, true, $contextAtu);
	}
	$resultAtu = file_get_contents($urlAtu, true, $contextAtu);

	if ($source_file['tmp_name'] != "") {
		include_once('../conn/id.php');

		$destination_file = "/public/imagens/produtos/";
		$source_file = $_FILES['file'];

		switch (exif_imagetype($source_file['tmp_name'])) {
			case 2:
				$type = ".jpg";
				break;
			case 3:
				$type = ".png";
				break;
		}

		$nomeImagem = $urlImagem = trim(htmlspecialchars($_POST['beforeimage'], ENT_QUOTES));

		$upload = ftp_put($conn_id, $destination_file . $nomeImagem, $source_file['tmp_name'], FTP_BINARY);

		if (!$upload) {
			//echo "FTP upload has failed!";
		} else {
			//echo "FTP upload sucess on $destination_file$nomeImagem";
		}

		ftp_close($conn_id);
	}
}

//API Url
$urlProd = $rotas['api'] . 'web/produtos/listar';

$jsonData = [
	"nomeEstabelecimento" => $globalData['nameUser'],
		"codigoEstabelecimento" => $globalData['codEst']
];

$options = array(
	'http' => array(
		'method'  => 'POST',
		'content' => json_encode($jsonData),
		'header' =>  "Content-Type: application/json\r\n" .
			"Accept: application/json\r\n"
	)
);

$context  = stream_context_create($options);
$resultProd = file_get_contents($urlProd, true, $context);
$jsonDataProd = json_decode($resultProd, true);

if (sizeOf($jsonDataProd) > 0) {
	$cont = 0;
	foreach ($jsonDataProd as $key => $value) {

		$checked = '';
		if ($value['disponivel']) {
			$checked = 'checked';
		}

		$K = '';
		$IM = '';
		if ($value['tipoProduto'] == 'K') {
			$K = 'checked';
		} else if ($value['tipoProduto'] == 'IM') {
			$IM = 'checked';
		}

		$Produtos[] = [
			"nome" => $value['produto'],
			"codigo" => $value['codigo'],
			"form" => "<form method='post' class='form-editar-produtos' enctype='multipart/form-data' action='ver-produtos.php'>
			<div style='display:flex'>
				<input type='text' name='codigo' class='form-control' value='" . $value['codigo'] . "' style='display:none;'>
				<div class='col-2'>
					<h5>Código</h5>
					<input type='text' class='form-control' value='" . $value['codigo'] . "' disabled>
				</div>
				<div class='col-2'>
					<h5>Nome</h5>
					<input type='text' name='produto' class='form-control' value='" . $value['produto'] . "'>
				</div>
				<div class='col-2'>
					<h5>Marca</h5>
					<input type='text' name='marca' class='form-control' value='" . $value['marca'] . "'>
				</div>
				<div class='col-1'>
					<h5>Valor</h5>
					<input type='text' name='valor' class='form-control' value='" . $value['valor'] . "'>
				</div>
				<div class='col-1'>
					<h5>Quantidade</h5>
					<input type='text' name='quantidade' class='form-control' value='" . $value['quantidade'] . "'>
				</div>	
				<div class='col-1'>
					<h5>Quantidade Vendida</h5>
					<input type='text' name='quantidadeVendida' class='form-control' value='" . $value['quantidadeVendida'] . "'>
				</div>
				<div class='col-1 text-center'>	
					<h5>Disponível</h5>
					<input id='check-list' type='checkbox' class='check-disp' name='disponivel' class='input-checkbox' $checked>
				</div>	
				<div class='area-image'>
					<div class='circle upload-area' pos='$cont'>
						<img class='profile-pic profile-pic-$cont' src='" . $value['url'] . "?" . time() . "'>
						<input name='beforeimage' value='" . explode("produtos/",$value['url'])[1] . "'>
					</div>
					<div class='p-image'>
						<input name='file' class='file-upload upload-$cont' pos='$cont' value='$postUrl' type='file' accept='image/png' />
					</div>
				</div>
				<div class='col text-center'>
					<button type='submit' name='submit' class='btn btn-atualizar-produtos btn-danger' ><i class='fa fa-pencil' aria-hidden='true'></i></button>
				</div>	
			</div>
			<div class='dropdown col-12 text-center area-vermais' style='padding-right: 0px; padding-left: 0px; margin-bottom:10px;'>
				<a class='btn col-12 text-center' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
					<span id='text-listas-adicional'>Ver mais</span>
				</a>
				<div id='dropdown' class='dropdown-menu' aria-labelledby='dropdownMenuLink'>
					<div class='col-12 vendas-lista' ref='$cont_drop-$cont_ad' style='margin-bottom:10px; display:block;'>	
						<div style='display:flex'>		
							<div class='col-2'>
								<h5>Promoção</h5>
								<input type='text' name='promocao' class='form-control' value='" . $value['promocao'] . "'>
							</div>			
							<div class='col-2'>
								<h5>Característica</h5>
								<input type='text' name='caracteristica' class='form-control' value='" . $value['caracteristica'] . "'>
							</div>
							<div class='col-2'>
								<h5>Categoria</h5>
								<input type='text' name='categoria' class='form-control' value='" . $value['categoria'] . "'>
							</div>		
							<div class='col-6'>
								<h5>Descrição</h5>
								<input type='text' name='descricao' class='form-control' value='" . $value['descricao'] . "'>
							</div>	
						</div>
						<div style='display:flex'>	
							<div class='col-2'>
								<h5>Medida</h5>
								<input type='text' name='medida' class='form-control' value='" . $value['medida'] . "'>
							</div>						
							<div class='col-2'>
								<h5>Quantidade Margem</h5>
								<input type='text' name='quantidadeMargem' class='form-control' value='" . $value['quantidadeMargem'] . "'>
							</div>		
							<div class='col-2'>
								<h5>Sub Categoria</h5>
								<input type='text' name='subCategoria' class='form-control' value='" . $value['subCategoria'] . "'>
							</div>	
							<div class='col-2' style='margin-left:5px'>	
								<h5 style='margin-bottom:12px'>Tipo Produto</h5>
								<div style='display:flex'>
									<h4 style='margin-right:10px'>Vendido no KG</h4>
									<input type='checkbox' class='check-list-tipo-produto' name='tipoProduto' class='input-checkbox' value='K' style='margin-right:15px' $K>
									<h4 style='margin-right:10px'>Vendido fatiado</h4>
									<input type='checkbox' class='check-list-tipo-produto' name='tipoProduto' class='input-checkbox' value='IM' $IM>
								</div>
							</div>	
						</div>						
					</div>
				</div>
			</div>		
		</form>"
		];

		$cont++;

		if (!isset($Produtos)) {
			$Produtos = "<div class='area-nada-por-aqui'><div><h3>Não há produtos, por enquanto...</h3></div><img src='../images/nenhum_produto.png'></div>";
		}
	}
}

?>

<body>
	<div class='site-section'>
		<div class='col-12'>
			<h2 class='section-title mb-3 text-center' style='margin:30px 0 0 0'>Produtos Cadastrados</h2>
			<?php
			if (sizeOf($jsonDataProd) > 0) {
				echo "<div class='area-search-produto col-8' style='display: flex;'>
				<span id='clean-search'>x</span>
				<input id='campo-search-produto' type='text' class='form-control col-11'>
				<button id='button-search-produto' class='btn btn-danger col-1'>Filtrar</button>
			</div>
			<div class='area-search-produto-options col-8' style='display: flex;'>
				<h4 style='margin-right:10px'>Filtrar por:</h4>
				<div style='display:flex'>
					<label style='margin-right:5px' for='nome'>Nome</label>
					<input type='radio' id='nome' name='filtro' value='nome' style='margin-right:15px' checked>					
					<label style='margin-right:5px' for='codigo'>Código</label>
					<input type='radio' id='codigo' name='filtro' value='codigo'>
				</div>
			</div>
			<div class='section-ver-produtos ver-produtos'>
				<div class='produtos'>";
				$cont = 0;
				$fim = 50;
				while ($cont < $fim) {
					echo $Produtos[$cont]["form"];
					$cont++;
				}
				echo "<button id='ver-mais-produtos' class='btn btn-danger'>Listar mais produtos</button>					
				</div>
			</div>";
			} else {
				echo "<div class='area-nada-por-aqui'><div><h3>Não há produtos, por enquanto...</h3></div><img src='../images/nenhum_produto.png'></div>";
			}
			?>
		</div>
	</div>
	<?php
	include_once('../footer.php');
	?>

	<script>
		var obj = <?php echo json_encode($Produtos); ?>;
		var i = 50;
		var aux = 50;
		var fim = 100;
		$('#ver-mais-produtos').on('click', function(event) {
			for (i; i < fim; i++) {
				$("#ver-mais-produtos").before(obj[i]["form"]);
			}
			i = fim;
			fim += aux;
		});

		$('#button-search-produto').on('click', function(event) {
			buscarProduto();
		});

		$("#campo-search-produto").keypress(function(event) {
			if (event.key == 'Enter') {
				buscarProduto();
			}
		});

		$('#campo-search-produto').keyup(function(i) {
			if ($(this).val() == "") {
				limparPesquisa();
			}
		});

		$('.area-search-produto #clean-search').on('click', function(event) {
			limparPesquisa();
		});

		function buscarProduto() {
			var filtro = $('.area-search-produto-options input[name="filtro"]:checked').val();
			if ($('#campo-search-produto').val() != "") {
				$(".area-search-produto #clean-search").show();
				$(".ver-produtos .resultado").remove();
				$(".ver-produtos .produtos").hide();
				$(".ver-produtos").append("<div class='resultado'></div>");
				if (filtro === "nome") {
					for (var c = 0; c <= filterItemsByName($('#campo-search-produto').val()).length; c++) {
						$(".ver-produtos .resultado").append(filterItemsByName($('#campo-search-produto').val())[c].form);
					}
				} else {
					for (var c = 0; c <= filterItemsByCod($('#campo-search-produto').val()).length; c++) {
						$(".ver-produtos .resultado").append(filterItemsByCod($('#campo-search-produto').val())[c].form);
					}
				}
			}
		}

		function limparPesquisa() {
			$(".ver-produtos .resultado").remove();
			$(".ver-produtos .produtos").show();
			$('#campo-search-produto').val("");
			$(".area-search-produto #clean-search").hide();
		}

		function filterItemsByName(query) {
			return obj.filter(function(el) {
				return el.nome.toLowerCase().indexOf(query.toLowerCase()) > -1;
			})
		}

		function filterItemsByCod(query) {
			return obj.filter(function(el) {
				return el.codigo.toLowerCase().indexOf(query.toLowerCase()) > -1;
			})
		}
	</script>