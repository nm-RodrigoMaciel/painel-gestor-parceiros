<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('../foods/functions.php');
?>
<body>
	<div class="site-section bg-light" id="planilha-section">
		<div class="container">
			<div id="loading" class="spinner-border text-danger" role="status" style="display: none">
				<span class="sr-only">Cadastrando...</span>
			</div>
			<form method="post" id="load_excel_form" enctype="multipart/form-data">
				<div class="row mb-5">
					<div class="col-12 text-center">
						<h2 class="section-title mb-3" style="margin-top:25px">Atualizar produtos cadastrados</h2>
					</div>
				</div>
				<div class="row mb-5">
					<div class="col-lg-6">
						<img src="../images/gestao.png" class="img-fluid img-sobre-nos" alt="Image">
					</div>
					<div class="col-lg-5 ml-auto pl-lg-5">
						<h2 class="text-black mb-4">Adicionar planilha de produtos</h2>
						<p class="mb-4">É importante que a planilha enviada não esteja faltando as informações dos produtos ou com valores incoerentes, caso esteja, nós iremos exibir uma alerta indicando o produto. O formato pode ser xls ou xlsx.</p>
						<table class="table">
							<tr>
								<td width="25%">Insira a lista de produtos</td>
								<td width="50%"><input type="file" name="select_excel" /></td>
								<td width="25%"><input id="btn-submit" type="submit" name="load" class="btn btn-primary" /></td>
							</tr>
						</table>
					</div>
				</div>
			</form>
			<div id="excel_area"></div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="vendor/animsition/js/animsition.min.js"></script>
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
	<script src="vendor/select2/select2.min.js"></script>
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
	<script src="vendor/countdowntime/countdowntime.js"></script>
	<script src="js/js.js"></script>
	<script src="js/parceiros.js"></script>
</body>

</html>
<script>
	$(document).ready(function() {
		$('#load_excel_form').on('submit', function(event) {
			$('#btn-submit').prop('disabled', true);
			$('#loading').show();
			event.preventDefault();
			$.ajax({
				url: "upload.php",
				method: "POST",
				data: new FormData(this),
				contentType: false,
				cache: false,
				processData: false,
				success: function(data) {
					$('#excel_area').html(data);
					$('table').css('width', '100%');
					$('#btn-submit').prop('disabled', false);
					$('#loading').hide();
				}
			})
		});
	});
</script>
<?php
	include_once('../footer.php');
?>