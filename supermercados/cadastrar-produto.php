<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('../foods/functions.php');
?>

<body>
	<div class="site-section">
		<div class="col-12">
			<h2 class="section-title title-cad-pro mb-3 text-center" style="margin-top:25px">Cadastrar produtos</h2>
			<form method="post" class="text-center" enctype="multipart/form-data" action="cadastrar-produto.php">
				<div class="col-12" style="display: inline-flex;">
					<div class="main-section col-8 text-center">
						<div class="col-12 main-form" style="display: inline-flex;">
							<div class="col-6">
								<!--<div class='col-12' style='text-align: left; display: flex; margin: 0 0 15px 0;' style="display: none;">
									<p style='margin-right:10px;'>O produto possui código de barras?</p>
									<input type='checkbox' class='check-cod-barra' class='input-checkbox'>
								</div>-->
								<input type='text' name='codigo' class='form-control campo-cod-barras' placeholder='Código de barras' required>
								<input type='text' name='produto' class='form-control' placeholder="Nome do produto" required>
								<input type='text' name='marca' class='form-control' placeholder="Marca" required>
								<input type='text' name='valor' class='form-control campo-valor' placeholder="Valor" required>
								<input type="text" name="promocao" class="form-control campo-promocao" placeholder="Promoção">
								<input type='number' name='quantidade' class='form-control' placeholder="Quantidade Estoque" required>
								<input type='text' name='caracteristica' class='form-control' placeholder="Característica / Sabor">
								<input type='text' name='categoria' class='form-control' placeholder="Categoria / Departamento">
							</div>
							<div class="col-6">
								<!--<input type='text' class='form-control' style="opacity:0">-->
								<input type='text' name='descricao' class='form-control' placeholder="Descrição" required>
								<input type='text' name='medida' class='form-control' placeholder="Medida" required>
								<input type='text' name='quantidadeMargem' class='form-control' placeholder="Quantidade Margem" required>
								<input type='text' name='subCategoria' class='form-control' placeholder="Sub Categoria" required>
								<h5 class='text-left' style="margin: 12px 0 5px 0;">Caso o produto seja vendido à unidade não precisa preencher nada.</h5>
								<div class="dropdown col-12" style="padding-right: 0px; padding-left: 0px; margin-bottom:10px;">
									<a class="btn dropdown-toggle col-12" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span>Tipo Produto</span>
									</a>
									<div id="dropdown" class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="width: 100%;">
										<table>
											<tbody>
												<tr>
													<label class='area-radio'>
														<p style='margin-left:15px; margin-top:6px;'>Produto vendido no quilo</p>
														<h5 style="margin-left: 15px;">Exemplo: Carne.</h5>
														<input type='checkbox' class='check-list-tipo-produto' name='tipo' value='K'>
														<span style='margin-left:15px;' class='checkmark'></span>
													</label>
												</tr>
												<tr>
													<label class='area-radio'>
														<p style='margin-left:15px; margin-top:6px;'>Produto vendido fatiado</p>
														<h5 style="margin-left: 15px;">Exemplo: Melancia.</h5>
														<input type='checkbox' class='check-list-tipo-produto' name='tipo' value='IM'>
														<span style='margin-left:15px;' class='checkmark'></span>
													</label>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-6" style="text-align: left; display: flex;">
									<p style="margin-right:10px;">Disponível</p>
									<input id='check-list' type='checkbox' class='check-disp' name='disponivel' class='input-checkbox' checked>
								</div>
							</div>
						</div>
					</div>
					<div id="area-lista" class="col-4">
						<h2 style="color: #000; margin-bottom:20px;">Imagem do produto</h2>
						<div>
							<div class="circle upload-area" pos="0">
								<img class="profile-pic profile-pic-0" pos="0" src="https://nomercadosoft.com.br/img/default-image.png">
							</div>
							<div class="p-image">
								<input name="file" class="file-upload upload-0" pos="0" type="file" accept="image/png" required />
							</div>
						</div>
					</div>
				</div>
				<button type="submit" name="submit" class="btn btn-submit btn-danger">Cadastrar Produto</button>
			</form>
			<?php
			if (isset($_POST['submit'])) {
				$postCodigo = trim(htmlspecialchars($_POST['codigo'], ENT_QUOTES));
				$postProduto = trim(htmlspecialchars($_POST['produto'], ENT_QUOTES));
				$postMarca = trim(htmlspecialchars($_POST['marca'], ENT_QUOTES));
				$postValor = trim(htmlspecialchars($_POST['valor'], ENT_QUOTES));
				$postPromocao = trim(htmlspecialchars($_POST['promocao'], ENT_QUOTES));
				$postQuantidade = trim(htmlspecialchars($_POST['quantidade'], ENT_QUOTES));
				$postCaracteristica = trim(htmlspecialchars($_POST['caracteristica'], ENT_QUOTES));
				$postCategoria = trim(htmlspecialchars($_POST['categoria'], ENT_QUOTES));
				$postDescricao = trim(htmlspecialchars($_POST['descricao'], ENT_QUOTES));
				$postMedida = trim(htmlspecialchars($_POST['medida'], ENT_QUOTES));
				$postQuantidadeMargem = trim(htmlspecialchars($_POST['quantidadeMargem'], ENT_QUOTES));
				$postSubCategoria = trim(htmlspecialchars($_POST['subCategoria'], ENT_QUOTES));
				$postDisponivel = trim(htmlspecialchars($_POST['disponivel'], ENT_QUOTES));
				$postTipoProduto = trim(htmlspecialchars($_POST['tipo'], ENT_QUOTES));

				if ($postDisponivel == "on") {
					$disponivel = true;
				} else {
					$disponivel = false;
				}

				$nomeImagem = $globalData['nameUser'] . "-" . $globalData['codEst'] . "-" . $codigoBarras . ".png";
				$nomeImagem = str_replace(" ", "", $nomeImagem);

				$produtos[] = array(
					"caracteristica" => $postCaracteristica,
					"categoria" => $postCategoria,
					"codigo" => $postCodigo,
					"codigoEstabelecimento" => $globalData['codEst'],
					"descricao" => $postDescricao,
					"marca" => $postMarca,
					"medida" => $postMedida,
					"produto" => $postProduto,
					"quantidade" => (int)$postQuantidadeMargem,
					"subCategoria" => $postSubCategoria,
					"tipoProduto" => $postTipoProduto,
					"url" => 'http://www.nomercadosoft.com.br/imagens/produtos/'.$nomeImagem,
					"valor" => (int)$postValor,
					"disponivel" => $disponivel
				);

				$url = $rotas['api'] . 'sauros/produtos/cadastrar';
				if ($rotas['apiBase'] != "") {
					$urlBase = $rotas['apiBase'] . 'sauros/produtos/cadastrar';
				}

				$ch = curl_init($url);

				$jsonData = [
					"tipoEstabelecimento" => $globalData['tipoEstabelecimento'],
					"cnpjEstabelecimento" => $globalData['cnpjUser'],
					"produtos" => $produtos
				];

				//echo json_encode($jsonData);

				$options = array(
					'http' => array(
						'method'  => 'POST',
						'content' => json_encode($jsonData),
						'header' =>  "Content-Type: application/json\r\n" .
							"Accept: application/json\r\n"
					)
				);

				$context  = stream_context_create($options);
				if ($rotas['apiBase'] != "") {
					$result = file_get_contents($urlBase, true, $context);
				}
				$result = file_get_contents($url, true, $context);

				if ($postCodigo == '') {
					$jsonData = json_decode($result, true);
					$codigoBarras = $jsonData[0]['codigoBarras'];
				} else {
					$codigoBarras = $postCodigo;
				}

				include_once('cadastro.php');
			}
			?>
		</div>
	</div>
	<div id="modal-promocao" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Promoção</h4>
				</div>
				<div class="modal-body" style="text-align: center;">
					<p>O valor da promoção já está calculado?</p>
					<p>Se não estiver, iremos calcular para você! ;)</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-modal-yes" data-dismiss="modal">Sim</button>
					<button type="button" class="btn btn-modal-no" data-dismiss="modal">Não</button>
				</div>
			</div>

		</div>
	</div>
	<?php
	include_once('../footer.php');
	?>