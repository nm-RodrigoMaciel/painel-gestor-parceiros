<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('../foods/functions.php');

//API Url
$url = $rotas['api'] . 'paginadas/vendas/listagem';

$jsonData = [
	"tipoEstabelecimento" => $globalData['tipoEstabelecimento'],
	"cnpjEstabelecimento" => $globalData['cnpjUser'],
	"idToken" => "eyJhbGciOiJSUzI1NiIsImtpZCI6ImRjMGMzNWZlYjBjODIzYjQyNzdkZDBhYjIwNDQzMDY5ZGYzMGZkZWEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vbm9tZXJjYWRvYXBwIiwiYXVkIjoibm9tZXJjYWRvYXBwIiwiYXV0aF90aW1lIjoxNTg2NDU3NTAxLCJ1c2VyX2lkIjoieXNFQ1ZLZ1Mwb2EzUWpCTWdzWnpoTGRKZ1BqMSIsInN1YiI6InlzRUNWS2dTMG9hM1FqQk1nc1p6aExkSmdQajEiLCJpYXQiOjE1ODY0NTc1MDEsImV4cCI6MTU4NjQ2MTEwMSwiZW1haWwiOiJjYW1wdXNAbm9tZXJjYWRvLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiZmlyZWJhc2UiOnsiaWRlbnRpdGllcyI6eyJlbWFpbCI6WyJjYW1wdXNAbm9tZXJjYWRvLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.ume5EnxRYoyt31dkxPmgQHl5Sdf77yF-KJtaQMazrEohFBMZNFmZLrYJEYmG0HZRXuH6aba39VCY-dR2pkhD5AHqf10NMhIrZl6yOP2KlCCc9tYkpn7wsXYkHT0pz37lMnZl4AgWkYr1sjUF0uP2mJJiGcpcurUCqaDHOgP1Q7_4gjdCGF85dyeIvpIwXbeKbK7qMOpGx73ygT25BrY_qOLZldu-gH-g8nkV-GeIIM1Np4gs-iSuozj7-aX-zz69YGBRqsqyPFt4_kwschC7YZJqgCK3LZnJ7-T0MtO39EBTggQjnD-2ZX3WEmP7aRe4lILusM79Bs2DECADSWClSQ",
	"status" => "all",
	//"lastIndex" => 0,
	"itemsPerPage" => 0,
	"date" => date('d/m/Y', strtotime('-7 days'))
];

$options = array(
	'http' => array(
		'method'  => 'POST',
		'content' => json_encode($jsonData),
		'header' =>  "Content-Type: application/json\r\n" .
			"Accept: application/json\r\n"
	)
);

//echo json_encode($jsonData);

$context  = stream_context_create($options);
$resultList = file_get_contents($url, true, $context);

$jsonDataList = json_decode($resultList, true);

//echo json_encode($jsonDataList);

$cont_drop = 0;
$arrayListasNomes = "";

if ($jsonDataList['statusCode'] != 400) {
	foreach ($jsonDataList as $value) {
		$status = -1;
		$aprovadoTempo = "";
		$separacaoTempo = "";
		$transporteTempo = "";
		$finalizadoTempo = "";

		switch (sizeof($value['status']) - 1) {
			case 0:
				$status = "pago";
				$proxStatus = "Em Separação";
				$aprovadoTempo = explode("T", $value['status'][0]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][0]['dataHoraInsert'])[1];
				break;
			case 1:
				$status = "separacao";
				$proxStatus = "Em Transporte";
				$aprovadoTempo = explode("T", $value['status'][0]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][0]['dataHoraInsert'])[1];
				$separacaoTempo = explode("T", $value['status'][1]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][1]['dataHoraInsert'])[1];
				break;
			case 2:
				$status = "transporte";
				$proxStatus = "Pedido Entregue";
				$aprovadoTempo = explode("T", $value['status'][0]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][0]['dataHoraInsert'])[1];
				$separacaoTempo = explode("T", $value['status'][1]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][1]['dataHoraInsert'])[1];
				$transporteTempo = explode("T", $value['status'][2]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][2]['dataHoraInsert'])[1];
				break;
			case 3:
				$status = "finalizado";
				$proxStatus = "Finalizado";
				$aprovadoTempo = explode("T", $value['status'][0]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][0]['dataHoraInsert'])[1];
				$separacaoTempo = explode("T", $value['status'][1]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][1]['dataHoraInsert'])[1];
				$transporteTempo = explode("T", $value['status'][2]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][2]['dataHoraInsert'])[1];
				$finalizadoTempo = explode("T", $value['status'][3]['dataHoraInsert'])[0] . "</br>" . explode("T", $value['status'][3]['dataHoraInsert'])[1];
				break;
		}

		$date_exploded = explode("T", $value['dataPrevistaEntrega'])[0];
		$day = explode("-", $date_exploded)[2];
		$month = explode("-", $date_exploded)[1];
		$year = explode("-", $date_exploded)[0];
		$time_exploded = explode("T", $value['dataPrevistaEntrega'])[1];

		$arrayListasNomes .=
			"<div class='dropdown col-12' style='padding-right: 0px; padding-left: 0px; margin-bottom:10px;'>
			<a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
				<span id='text-listas-adicional'>" . $value['codigoTransacao'] . "</span>
			</a>
			<div id='dropdown' class='dropdown-menu' aria-labelledby='dropdownMenuLink'>
			<div class='col-12 vendas-lista supermercados' ref='$cont_drop' style='margin-bottom:10px;'>
				<table class='table'>
					<tbody>
						<tr>
							<th scope='row'>Forma de pagamento</th>
							<td>" . ucfirst(strtolower($value['pagamento']['descPag'])) . "</td>
						</tr>
						<tr>
							<th scope='row'>Frete</th>
							<td>" . $value['pagamento']['vFrete'] . " Reais</td>
						</tr>	
						<tr>
							<th scope='row'>Valor da compra</th>
							<td>" . $value['pagamento']['vPago'] . " Reais</td>
						</tr>	
						<tr>
							<th scope='row'>Valor sem o frete</th>
							<td>" . $value['pagamento']['vProds'] . " Reais</td>
						</tr>
						<tr>
							<th scope='row'>Nome do cliente</th>
							<td>" . $value['cliente']['nomeUsuario'] . "</td>
						</tr>
						<tr>
							<th scope='row'>Telefone</th>
							<td>" . $value['cliente']['telefone'] . "</td>
						</tr>
						<tr>
							<th scope='row'>Endereço</th>
							<td>" . $value['cliente']['rua'] . " - Número: " . $value['cliente']['numero'] . " Complemento: " . $value['cliente']['complemento'] . " " . $value['cliente']['bairro'] . " - " . $value['cliente']['cidade'] . "</td>
						</tr>
						<tr>
							<th scope='row'>Previsão de entrega</th>
							<td>" . $day . '/' . $month . '/' . $year . ' ' . $time_exploded . "</td>						
						</tr>																											
						<tr>
							<td><button class='btn btn-danger btn-atualizar-status' value='" . (sizeof($value['status']) - 1) . ";" . $value['codigoTransacao'] . "'>$proxStatus</button></td>
							<td><button class='btn btn-danger btn-contato' value='" . $value['codigoTransacao'] . "'>Entrar em Contato</button></td>
							<td><button class='btn btn-danger btn-ver-produtos' value='" . $value['codigoTransacao'] . "'>Ver Produtos</button></td>
						</tr>
					</tbody>
				</table>
				<div class='area-imagem-vendas'>
					<div>
						<img class='imagem-status' src='../images/$status.png'>
					</div>	
					<div>
						<div class='data-aprovado'>
							$aprovadoTempo    	        
						</div>
						<div class='data-separacao'>	
							$separacaoTempo			        		        
						</div>
						<div class='data-transporte'>	
							$transporteTempo			        		        
						</div>
						<div class='data-finalizado'>
							$finalizadoTempo			        			        
						</div>                                                            
					</div>			    
				</div>
				</div>
			</div>
		</div>";
		$cont_drop += 1;
	}

	if (isset($arrayListasNomes)) {
		/*$arrayListasNomes .= "<button id='ver-mais-vendas' class='btn btn-danger'>Listar mais vendas</button>";*/
		$html = $arrayListasNomes;
	} else {
		$html = "<div class='area-nada-por-aqui'><div><h3>Não há vendas, por enquanto...</h3></div><img src='../images/nada_por_enquanto.png'></div>";
	}
} else {
	$html = "<div class='area-nada-por-aqui'><div><h3>Não há vendas, por enquanto...</h3></div><img src='../images/nada_por_enquanto.png'></div>";
}

?>

<body>
	<div class="site-section">
		<div class="container">
			<div class="col-12 text-center">
				<h2 class="section-title mb-3" style="margin-top:5px">Vendas realizadas</h2>
				<fieldset id="group1">
					<label style='margin-right:3px' for='sete'>7 dias</label>
					<input class="check-time" type='radio' id='sete' name='group1' value='7' style='margin-right:7px' checked>
					<label style='margin-right:3px' for='trinta'>30 dias</label>
					<input class="check-time" type='radio' id='trinta' name='group1' value='30' style='margin-right:7px'>
					<label style='margin-right:3px' for='sessenta'>60 dias</label>
					<input class="check-time" type='radio' id='sessenta' name='group1' value='60'>
				</fieldset>
				<div class="section-listas">
					<div id="loading-content" style="position: fixed; background: #f8f9fa; width: 100%; height: 100%; z-index: 999999999999;">
						<img class="center-div" src="../images/gifs/loading.gif" />
					</div>
					<?php echo $html; ?>
				</div>
			</div>
		</div>
	</div>
	<div id="modal-vendas" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h3 class="modal-title">Produtos</h3>
				</div>
				<div class="modal-body" style="text-align: center; display: inline-flex">
					<div class="modal-produtos" style="width: 100%;">
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-modal-no" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	<?php
	include_once('../footer.php');
	?>
	<script>
		/*$(document).on('click', '#ver-mais-vendas', function() {
			startLoading();
			var count = 0;
			var days = 7;

			$('.check-time').each(function (i) {
				if (this.checked) {
					var days = $(this).val();
				}
			});

			$(".section-listas .dropdown").each(function(i) {
				count++;
			});

			$.post('returnVendas.php', {
					count: count,
					lastDays: 7
				},
				function(data, status) {
					if (status == 'success') {
						$('#ver-mais-vendas').before(data);
						stopLoading();
					}
				});
		});*/

		$('.check-time').on('change', function() {
			startLoading();
			if ($(this).val() == "7") {
				$.post('returnVendas.php', {
						lastDays: 7
					},
					function(data, status) {
						if (status == 'success') {
							$('.section-listas .dropdown').remove();
							$('.section-listas').append(data);
							stopLoading();
						}
					});
			} else if ($(this).val() == "30") {
				$.post('returnVendas.php', {
						lastDays: 30
					},
					function(data, status) {
						if (status == 'success') {
							$('.section-listas .dropdown').remove();
							$('.section-listas').append(data);
							stopLoading();
						}
					});
			} else if ($(this).val() == "60") {
				$.post('returnVendas.php', {
						lastDays: 60
					},
					function(data, status) {
						if (status == 'success') {
							$('.section-listas .dropdown').remove();
							$('.section-listas').append(data);
							stopLoading();
						}
					});
			}
		});

		$(document).on('click', '.supermercados .btn-ver-produtos', function() {
			$.post("buscarProdutos.php", {
					arrayPosition: $(this).val()
				},
				function(data, status) {
					$("#modal-vendas .modal-adicionais div").remove();
					$("#modal-vendas .modal-produtos .content").remove();
					$("#modal-vendas .modal-produtos").append(data);
					$('#modal-vendas').modal('show');
				});
		});

		$('.supermercados .btn-ver-produtos').on('click', function(event) {
			$.post("buscarProdutos.php", {
					arrayPosition: $(this).val()
				},
				function(data, status) {
					$("#modal-vendas .modal-adicionais div").remove();
					$("#modal-vendas .modal-produtos .content").remove();
					$("#modal-vendas .modal-produtos").append(data);
					$('#modal-vendas').modal('show');
				});
		});
	</script>