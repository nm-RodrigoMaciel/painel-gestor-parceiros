<?php
include_once('rotas.php');
include_once('variables.php');

$key = trim(htmlspecialchars($_POST['key'], ENT_QUOTES));

switch (explode(";",$key)[0]) {
    case 0:
        $url = $rotas['api'].'sauros/vendas/separacao';
        if ($rotas['apiBase'] != "") {
            $urlBase = $rotas['apiBase'] . 'sauros/vendas/separacao';
        }
        break;
    case 1:
        $url = $rotas['api'].'sauros/vendas/transporte';
        if ($rotas['apiBase'] != "") {
            $urlBase = $rotas['apiBase'] . 'sauros/vendas/transporte';
        }
        break;
    case 2:
        $url = $rotas['api'].'sauros/vendas/finalizado';
        if ($rotas['apiBase'] != "") {
            $urlBase = $rotas['apiBase'] . 'sauros/vendas/finalizado';
        }
        break;
}

$jsonData = [
    "tipoEstabelecimento" => $globalData['tipoEstabelecimento'],
	"cnpjEstabelecimento" => $globalData['cnpjUser'],
	"compraId" => explode(";",$key)[1]
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
if ($rotas['apiBase'] != "") {
    $resultList = file_get_contents($urlBase, true, $context);
}
$resultList = file_get_contents($url, true, $context);