<?php
include_once('rotas.php');
include_once('variables.php');

//API Url
$url = $rotas['api'] . 'chat/listarcodigos/';

$jsonData = [
    "nomeEstabelecimento" => $globalData['nameUser'],
	"codigoEstabelecimento" => $globalData['codEst']
];

$options = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonData),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$context  = stream_context_create($options);
$resultList = file_get_contents($url, true, $context);

$jsonDataList = json_decode($resultList, true);

$chats = $jsonDataList['items'];

echo sizeof($chats);

$data = '';
if (sizeOf($jsonDataList) > 0 && !isset($jsonDataList['statusCode'])) {
    foreach($chats as $key => $value){
        $data .= $key;
    }

    //echo $data;
}
