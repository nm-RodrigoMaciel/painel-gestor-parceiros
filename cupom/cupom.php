<?php
include_once('../header.php');
include_once('../rotas.php');
include_once('../foods/functions.php');

if (isset($_POST['cadastrar'])) {
    $postCupom = trim(htmlspecialchars($_POST['cupom'], ENT_QUOTES));
    $postDisponivel = trim(htmlspecialchars($_POST['disponibilidade'], ENT_QUOTES));
    $postEstabelecimento = trim(htmlspecialchars($_POST['estabelecimento'], ENT_QUOTES));
    $postFrete = trim(htmlspecialchars($_POST['freteGratis'], ENT_QUOTES));
    $postQuantidadePerm = trim(htmlspecialchars($_POST['quantidadePermitida'], ENT_QUOTES));
    $postUsoRepetido = trim(htmlspecialchars($_POST['usoRepete'], ENT_QUOTES));
    $postValorDesconto = trim(htmlspecialchars($_POST['valorDesconto'], ENT_QUOTES));
    $postValorDescontoReal = trim(htmlspecialchars($_POST['valorDescontoReal'], ENT_QUOTES));
    $postValorMinimoCompra = trim(htmlspecialchars($_POST['valorMinimoCompra'], ENT_QUOTES));

    if ($postDisponivel == "on") {
        $disponivel = "sim";
    } else {
        $disponivel = "nao";
    }

    if ($postFrete == "on") {
        $frete = "sim";
    } else {
        $frete = "nao";
    }

    $urlCad = $rotas['api'] . 'web/cupons/cadastrar';

    if ($postValorDesconto == "")
        $postValorDesconto = "0";

    if ($postValorDescontoReal == "")
        $postValorDescontoReal = "0";

    $itemsCad = array(
        "cupom" => $postCupom,
        "disponibilidade" => $disponivel,
        "estabelecimentos" => $postEstabelecimento,
        "freteGratis" => $frete,
        "quantidadePermitida" => $postQuantidadePerm,
        "usoRepete" => $postUsoRepetido,
        "valorDesconto" => $postValorDesconto,
        "valorDescontoReal" => $postValorDescontoReal,
        "valorMinimoCompra" => $postValorMinimoCompra
    );

    $jsonDataCad = [
        "nomeEstabelecimento" => $globalData['nameUser'],
        "codigoEstabelecimento" => $globalData['codEst'],
        "cupom" => $itemsCad
    ];

    $optionsCad = array(
        'http' => array(
            'method' => 'POST',
            'content' => json_encode($jsonDataCad),
            'header' => "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
        )
    );

    $contextCad = stream_context_create($optionsCad);
    $resultCad = file_get_contents($urlCad, true, $contextCad);

    $jsonDataCad = json_decode($resultCad, true);
} else if (isset($_POST['editar'])) {
    $postCupom = trim(htmlspecialchars($_POST['cupom'], ENT_QUOTES));
    $postDisponivel = trim(htmlspecialchars($_POST['disponibilidade'], ENT_QUOTES));
    $postEstabelecimento = trim(htmlspecialchars($_POST['estabelecimento'], ENT_QUOTES));
    $postFrete = trim(htmlspecialchars($_POST['freteGratis'], ENT_QUOTES));
    $postQuantidadePerm = trim(htmlspecialchars($_POST['quantidadePermitida'], ENT_QUOTES));
    $postUsoRepetido = trim(htmlspecialchars($_POST['usoRepete'], ENT_QUOTES));
    $postValorDesconto = trim(htmlspecialchars($_POST['valorDesconto'], ENT_QUOTES));
    $postValorDescontoReal = trim(htmlspecialchars($_POST['valorDescontoReal'], ENT_QUOTES));
    $postValorMinimoCompra = trim(htmlspecialchars($_POST['valorMinimoCompra'], ENT_QUOTES));

    if ($postDisponivel == "on") {
        $disponivel = "sim";
    } else {
        $disponivel = "nao";
    }

    if ($postFrete == "on") {
        $frete = "sim";
    } else {
        $frete = "nao";
    }

    if ($postValorDesconto == "")
        $postValorDesconto = "0";

    if ($postValorDescontoReal == "")
        $postValorDescontoReal = "0";

    $urlEdi = $rotas['api'] . 'web/cupons/alterar';

    $itemsEdi = array(
        "cupom" => $postCupom,
        "disponibilidade" => $disponivel,
        "estabelecimentos" => $postEstabelecimento,
        "freteGratis" => $frete,
        "quantidadePermitida" => $postQuantidadePerm,
        "usoRepete" => $postUsoRepetido,
        "valorDesconto" => $postValorDesconto,
        "valorDescontoReal" => $postValorDescontoReal,
        "valorMinimoCompra" => $postValorMinimoCompra
    );

    $jsonDataEdi = [
        "nomeEstabelecimento" => $globalData['nameUser'],
        "codigoEstabelecimento" => $globalData['codEst'],
        "cupom" => $itemsEdi
    ];

    //echo json_encode($jsonDataEdi);

    $optionsEdi = array(
        'http' => array(
            'method' => 'POST',
            'content' => json_encode($jsonDataEdi),
            'header' => "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
        )
    );

    $contextEdi = stream_context_create($optionsEdi);
    $resultEdi = file_get_contents($urlEdi, true, $contextEdi);

    $jsonDataEdi = json_decode($resultEdi, true);
}

//API Url
$urlLis = $rotas['api'] . 'web/cupons/listar';

$jsonDataLis = [
    "nomeEstabelecimento" => $globalData['nameUser'],
    "codigoEstabelecimento" => $globalData['codEst'],
    "key" => ""
];

$optionsLis = array(
    'http' => array(
        'method'  => 'POST',
        'content' => json_encode($jsonDataLis),
        'header' =>  "Content-Type: application/json\r\n" .
            "Accept: application/json\r\n"
    )
);

$contextLis  = stream_context_create($optionsLis);
$resultLis = file_get_contents($urlLis, true, $contextLis);

$jsonDataLis = json_decode($resultLis, true);

//echo json_encode($jsonDataLis);

foreach ($jsonDataLis as $value) {

    $botaoEditar = '';

    $disponibilidade = ($value['disponibilidade'] == 'sim') ? 'checked' : '';
    $freteGratis = ($value['freteGratis'] == 'sim') ? 'checked' : '';

    $arrayApp = '<div class="col"><h2 style="margin:5px 0 10px 0">Cupons do app</h2>';               

    if ($value['estabelecimentos'] == $globalData['nameUser']) {
        $botaoEditar = " <tr><td><button type='submit' name='editar' class='btn btn-danger'>Editar</button></td></tr>";

        $arraySeus .=
            "<div class='dropdown col-12' style='padding-right: 0px; padding-left: 0px; margin-bottom:10px;'>
		<a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
			<span id='text-listas-adicional'>" . $value['cupom'] . "</span>
		</a>
		<div id='dropdown' class='dropdown-menu' aria-labelledby='dropdownMenuLink'>
		<div class='col-12' style='margin-bottom:10px;'>
		   <form method='post' enctype='multipart/form-data' action='cupom.php'>
			<table class='table'>
				<tbody>
                    <tr>
                        <th scope='row'>Cupom</th>
                        <td><input class='read-only' type='text' name='cupom' value='" . $value['cupom'] . "'></td>
                    </tr>
                    <tr>
                        <th scope='row'>Disponíbiliidade</th>
                        <td style='text-align: center'><input type='checkbox' name='disponibilidade' $disponibilidade></td>
                    </tr>
                    <tr>
                        <th scope='row'>Estabelecimento</th>
                        <td style='text-align: center'><input type='text' class='read-only' name='estabelecimento' value='" . $value['estabelecimentos'] . "'></td>
                    </tr>
                    <tr>
                        <th scope='row'>Frete Gratis</th>
                        <td style='text-align: center'><input type='checkbox' name='freteGratis' $freteGratis></td>
                    </tr>
                    <tr>
                        <th scope='row'>Quantidade Permitida</th>
                        <td><input type='text' name='quantidadePermitida' value='" . $value['quantidadePermitida'] . "'></td>
                    </tr>
                    <tr>
                        <th scope='row'>Uso Repetido</th>
                        <td><input type='text' name='usoRepete' value='" . $value['usoRepete'] . "'></td>
                    </tr>
                    <tr class='desconto'>
                        <th scope='row'>Valor Desconto</th>
                        <td><input type='text' name='valorDesconto' class='desconto-text' value='" . $value['valorDesconto'] . "'></td>
                        <td style='text-align: center'><fieldset id='group1'> <label style='margin-right:3px' for='valorDesconto'>Usar este</label> <input class='uso-desconto' type='radio' name='group1' checked></td>
                    </tr>
				</fieldset>
                    <tr>
                        <th scope='row'>Valor Desconto Real</th>
                        <td><input type='text' name='valorDescontoReal' class='desconto-text' value='" . $value['valorDescontoReal'] . "' disabled></td>
                        <td style='text-align: center'><label style='margin-right:3px' for='valorDescontoReal'>Usar este</label> <input class='uso-desconto' type='radio' name='group1'></fieldset></td>
                    </tr>
                    <tr>
                        <th scope='row'>Valor Minimo Compra</th>
                        <td><input type='text' name='valorMinimoCompra' value='" . $value['valorMinimoCompra'] . "'></td>
                    </tr>																																										
                    $botaoEditar
				</tbody>
			</table>
			</form>
			</div>
		</div>
    </div>";
    } else {
        $arrayApp .=
            "<div class='dropdown col-12' style='padding-right: 0px; padding-left: 0px; margin-bottom:10px;'>
        <a class='btn dropdown-toggle col-12' href='#' role='button' id='dropdownMenuLink' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span id='text-listas-adicional'>" . $value['cupom'] . "</span>
        </a>
        <div id='dropdown' class='dropdown-menu' aria-labelledby='dropdownMenuLink'>
        <div class='col-12' style='margin-bottom:10px;'>
        <form method='post' enctype='multipart/form-data' action='cupom.php'>
            <table class='table'>
                <tbody>
                    <tr>
                        <th scope='row'>Cupom</th>
                        <td><input class='read-only' type='text' name='cupom' value='" . $value['cupom'] . "'></td>
                    </tr>
                    <tr>
                        <th scope='row'>Disponíbiliidade</th>
                        <td style='text-align: center'><input type='checkbox' name='disponibilidade' $disponibilidade></td>
                    </tr>
                    <tr>
                        <th scope='row'>Estabelecimento</th>
                        <td style='text-align: center'><input type='text' class='read-only' name='estabelecimento' value='" . $value['estabelecimentos'] . "'></td>
                    </tr>
                    <tr>
                        <th scope='row'>Frete Gratis</th>
                        <td style='text-align: center'><input type='checkbox' name='freteGratis' $freteGratis></td>
                    </tr>
                    <tr>
                        <th scope='row'>Quantidade Permitida</th>
                        <td><input type='text' name='quantidadePermitida' value='" . $value['quantidadePermitida'] . "'></td>
                    </tr>
                    <tr>
                        <th scope='row'>Uso Repetido</th>
                        <td><input type='text' name='usoRepete' value='" . $value['usoRepete'] . "'></td>
                    </tr>
                    <tr>
                        <th scope='row'>Valor Desconto</th>
                        <td><input type='text' name='valorDesconto' value='" . $value['valorDesconto'] . "'></td>
                    </tr>
                    <tr>
                        <th scope='row'>Valor Desconto Real</th>
                        <td><input type='text' name='valorDescontoReal' value='" . $value['valorDescontoReal'] . "'></td>
                    </tr>
                    <tr>
                        <th scope='row'>Valor Minimo Compra</th>
                        <td><input type='text' name='valorMinimoCompra' value='" . $value['valorMinimoCompra'] . "'></td>
                    </tr>																																										
                    $botaoEditar
                </tbody>
            </table>
            </form>
            </div>
        </div>
    </div>";
    }

    $arrayApp .= '</div>';
}
?>

<body>
    <div class="site-section bg-light" id="planilha-section">
        <div class="container">
            <div class="col-12 text-center">
                <h2 class="section-title mb-3" style="margin-top:5px">Gerenciar Cupons</h2>
                <div class="section-listas section-cupons" style="display: inline-flex;">
                    <div class="col">
                        <h2 style="margin-top:5px">Seus cupons</h2>
                        <div class="dropdown col-12" style="padding-right: 0px; padding-left: 0px; margin:10px 0 25px 0;">
                            <a class="btn dropdown-toggle col-12" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-plus-square-o" aria-hidden="true" style="color:#dc3545; margin-right: 8px;"></i><span id="text-listas-adicional">Cadastrar novo cupom</span>
                            </a>
                            <form method='post' enctype='multipart/form-data' action='cupom.php'>
                                <div id="dropdown" class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <div class="col-12" style="margin-bottom:10px;">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th scope="row">Cupom</th>
                                                    <td><input class="cupom-input" type="text" name='cupom' value=""></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Disponíbiliidade</th>
                                                    <td style="text-align: center"><input type='checkbox' name='disponibilidade'></td>
                                                </tr>
                                                <tr style="display: none">
                                                    <th scope="row">Estabelecimento</th>
                                                    <td style="text-align: center"><input type="text" name='estabelecimento' value="<?php echo $globalData['nameUser'] ?>"></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Frete Gratis</th>
                                                    <td style="text-align: center"><input type='checkbox' name='freteGratis'></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Quantidade Permitida</th>
                                                    <td><input type="text" name='quantidadePermitida' value=""></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Uso Repetido</th>
                                                    <td><input type="text" name='usoRepete' value=""></td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Valor Desconto</th>
                                                    <td><input type="text" class='desconto-text' name='valorDesconto' value=""></td>
                                                    <td style='text-align: center'>
                                                        <fieldset id='group1'> <label style='margin-right:3px' for='valorDesconto'>Usar este</label> <input class='uso-desconto' type='radio' name='group1' checked>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Valor Desconto Real</th>
                                                    <td><input type="text" class='desconto-text' name='valorDescontoReal' value="" disabled></td>
                                                    <td style='text-align: center'><label style='margin-right:3px' for='valorDescontoReal'>Usar este</label> <input class='uso-desconto' type='radio' name='group1'></fieldset>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">Valor Minimo Compra</th>
                                                    <td><input type="text" name='valorMinimoCompra' value=""></td>
                                                </tr>
                                                <tr>
                                                    <td><button type="submit" name="cadastrar" class="btn btn-danger">Cadastrar</button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <?php echo $arraySeus; ?>
                    </div>
                    <?php echo $arrayApp; ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    include_once('../footer.php');
    ?>
    <script>
        /*$('.uso-desconto').on('change', function(i) {
            alert($(this).attr('pos'));
        });*/

        $('.uso-desconto').on('change', function() {
            $('.desconto-text').each(function() {
                if ($(this).prop('disabled')) {
                    $(this).prop('disabled', false);
                } else {
                    $(this).prop('disabled', true);
                }
            });
        });
    </script>